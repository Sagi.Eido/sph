#-------------------------------------------------
#
# Project created by QtCreator 2016-04-23T08:22:17
#
#-------------------------------------------------

# Qt 5.13.0 with MSVC2017 64bit

message(== START ==============================================================$$escape_expand(\r\n))

QT += core gui opengl multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET    = SPH
TEMPLATE  = app
CONFIG   += mobility c++17
CONFIG   += resources_big # for bigger heap space for compiler
#MOBILITY  =

LIBS += -luser32 # for keybd_event on Windows

############################################################
# Intel C++ Compiler ICL ICC ICPC ##########################
############################################################

##ICL_VSN = 2018.4.253
#ICL_VSN = 2019.0.120

## add win32-icc at Tools -> Options -> Compilers -> Manual -> Compiler -> "Qt mkspecs"

## add to environmental variable Path:
##   E:\dev\IntelSWTools\compilers_and_libraries_$${ICL_VSN}\windows\bin # for compilervars.bat
##   E:\dev\IntelSWTools\compilers_and_libraries_$${ICL_VSN}\windows\bin\intel64 # for icl.exe
##   E:\dev\Qt\$${QT_VERSION}\msvc2017_64\bin # for qmake.exe

## Go to project directory
## Type: qmake -d -spec win32-icc
##   It will use qmake.conf from E:\dev\Qt\$${QT_VERSION}\msvc2017_64\mkspecs\win32-icc

## compilervars.bat -arch intel64 vs2017 #-platform [linux|android]

#ICL_DIR = E:/dev/IntelSWTools/compilers_and_libraries_$${ICL_VSN}
#ICL_BIN_DIR = $${ICL_DIR}/windows/bin
#ICL_CMP_DIR = $${ICL_BIN_DIR}/intel64
#ICL_CMP = $${ICL_CMP_DIR}/icl.exe

###system($${ICL_BIN_DIR}/compilervars.bat -arch intel64 vs2017)
###system($${ICL_BIN_DIR}/iclvars.bat -arch intel64 vs2017)

##QMAKE_CC = icl
#QMAKE_CC = $${ICL_CMP}
#QMAKE_CXX = $${ICL_CMP}
#QMAKE_LINK = $${ICL_CMP}
###QMAKE_CXXFLAGS += /Qvc14

#win*-icc { message("haha") }
#win*-icpc { message("hihi") }
#win*-icl { message("hoho") }

#ICL_IN_USE = "yes"

############################################################
# Flags ####################################################
############################################################

*-g++:QMAKE_CXXFLAGS_WARN_ON += -Wextra
#win*-msvc*:QMAKE_CXXFLAGS   += /sdl # TODO, it ignores some errors

#CPP_STD = "c++17"
CPP_STD = "c++latest"
CPP_STD_CUDA = c++14

# add /fp:strict & /fp:except for floating point control for NaN detection?
CONFIG(debug, debug|release) {
    CONFIG += console
    *-g++      { QMAKE_CXXFLAGS += -std=$${CPP_STD} -g3 }
    win*-msvc* { QMAKE_CXXFLAGS += /std:$${CPP_STD} /Zi } #/Z7
}
else {
    *-g++      { QMAKE_CXXFLAGS += -std=$${CPP_STD} -O3 -msse4.2 }
    win*-msvc* { QMAKE_CXXFLAGS += /std:$${CPP_STD} /O2 /arch:AVX2 }
}

############################################################
# Defines & Variables ######################################
############################################################

!android   { DEFINES += DESKTOP_BUILD }
android    { DEFINES += ANDROID_BUILD }
*-g++      { DEFINES += COMPILER_GPP }
win*-msvc* { DEFINES += COMPILER_MSVC }

*-g++      { COMPILER = "GPP" }
win*-msvc* { COMPILER = "MSVC" }

*-g++ {
    #MINGW_V = mingw53_32
    MINGW_V = mingw73_64
}

CONFIG(debug, debug|release) {
    DEFINES += DEBUG_BUILD
    BUILD = "DEBUG"
}
else {
    DEFINES += RELEASE_BUILD
    BUILD = "RELEASE"
}

############################################################
# Directories ##############################################
############################################################

DEV_DIR = E:/dev
QT_DIR = $${DEV_DIR}/Qt/$${QT_VERSION}

{
    # Qt Creator seems to have a problem providing qwindows.dll / qwindowsd.dll
    *-g++ {
        Debug:QWINDOWS_DLL_SRC   = $$shell_path($${QT_DIR}/$${MINGW_V}/plugins/platforms/qwindowsd.dll)
        Debug:QWINDOWS_DLL_DST   = $$shell_path({OUT_PWD}/$${MINGW_V}/debug/platforms/)
        Release:QWINDOWS_DLL_SRC = $$shell_path({QT_DIR}/$${MINGW_V}/plugins/platforms/qwindows.dll)
        Release:QWINDOWS_DLL_DST = $$shell_path({OUT_PWD}/release/platforms/)
    }
    win*-msvc* {
        Debug:QWINDOWS_DLL_SRC   = $$shell_path($${QT_DIR}/msvc2017_64/plugins/platforms/qwindowsd.dll)
        Debug:QWINDOWS_DLL_DST   = $$shell_path($${OUT_PWD}/debug/platforms/)
        Release:QWINDOWS_DLL_SRC = $$shell_path($${DEV_DIR}/Qt/$${QT_VERSION}/msvc2017_64/plugins/platforms/qwindows.dll)
        Release:QWINDOWS_DLL_DST = $$shell_path($${OUT_PWD}/release/platforms/)
        #$$shell_path()
        !exists($$shell_quote($${QWINDOWS_DLL_DST})) {
            QMAKE_POST_LINK += \
                #$${QMAKE_MKDIR} $${QWINDOWS_DLL_DST} $$escape_expand(\r\n) \
                # there is no -p flag for mkdir on Windows for some time, any more
                IF NOT EXIST $${QWINDOWS_DLL_DST} MKDIR $${QWINDOWS_DLL_DST} $$escape_expand(\r\n) \
                $${QMAKE_COPY} $${QWINDOWS_DLL_SRC} $${QWINDOWS_DLL_DST} $$escape_expand(\r\n)
            export(QMAKE_POST_LINK)
        }
    }
}

############################################################
# OpenCV ###################################################
############################################################

OPENCV_DIR      = $${DEV_DIR}/opencv/build
OPENCV_MSVC_DIR = $${OPENCV_DIR}/install/x64/vc15

win*-msvc* {
    # Qt Creator seems to have a problem providing qwindows.dll / qwindowsd.dll
    #   Copying DLLs

    !CONFIG(debug, debug|release) {
        OPENCV_BIN_DIR = $${OPENCV_MSVC_DIR}/bin
        OPENCV_DLLS += opencv_core330.dll \
                       opencv_imgcodecs330.dll \
                       opencv_imgproc330.dll \
                       opencv_videoio330.dll
        DLL_DST = $$shell_path($${OUT_PWD}/release/)
        for(DLL, OPENCV_DLLS) : {
            SRC = $$shell_path($${OPENCV_BIN_DIR}/$${DLL})
            QMAKE_POST_LINK += $${QMAKE_COPY} $${SRC} $${DLL_DST}
            QMAKE_POST_LINK += $$escape_expand(\r\n)
        }
        message("Copying DLLs"$$escape_expand(\r\n)$${QMAKE_POST_LINK})
    }
    export(QMAKE_POST_LINK)
}

INCLUDEPATH += $${OPENCV_DIR}/include

*-g++ {
    Debug:LIBS += \
        -L$${OPENCV_DIR}/bin/Debug \
        -lopencv_ffmpeg330d
    Release:LIBS += \
        -L$${OPENCV_DIR}/bin/Release \
        -lopencv_ffmpeg330
}
win*-msvc* {
    LIBS += \
        -L"$${OPENCV_MSVC_DIR}/lib" \ # LIBs
        -L"$${OPENCV_MSVC_DIR}/bin"   # DLLs
}
Debug:LIBS += \
    -lopencv_calib3d330d \
    -lopencv_core330d \
    -lopencv_features2d330d \
    -lopencv_highgui330d \
    -lopencv_imgcodecs330d \
    -lopencv_imgproc330d \
    -lopencv_videoio330d
Release:LIBS += \
    -lopencv_calib3d330 \
    -lopencv_core330 \
    -lopencv_features2d330 \
    -lopencv_highgui330 \
    -lopencv_imgcodecs330 \
    -lopencv_imgproc330 \
    -lopencv_videoio330
message($${COMPILER} $${BUILD} LIBS: $${LIBS}$${escape_expand(\r\n)})

############################################################
# MPI ######################################################
############################################################

INCLUDEPATH += $${DEV_DIR}/ms-mpi/Include # MPI (Microsoft)

contains(DEFINES, COMPILER_GPP) {
    equals(MINGW_V, "mingw53_32") : LIBS += -L$${DEV_DIR}/ms-mpi/Lib/x86 -lmsmpi
    equals(MINGW_V, "mingw73_64") : LIBS += -L$${DEV_DIR}/ms-mpi/Lib/x64 -lmsmpi
}
contains(DEFINES, COMPILER_MSVC) {
    LIBS += -L$${DEV_DIR}/ms-mpi/Lib/x64 -lmsmpi # MPI (Microsoft)
}

############################################################
# OMP ######################################################
############################################################

*-g++:QMAKE_CXXFLAGS      += -pthread -fopenmp
win*-msvc*:QMAKE_CXXFLAGS += /openmp

contains(DEFINES, COMPILER_GPP) {
    DEFINES += OMP_3_PLUS
}
contains(DEFINES, COMPILER_MSVC) {
    DEFINES += OMP_2
}

equals(ICL_IN_USE, "yes") : {
    #/Qopenmp-link:static

    #DEFINES -= OMP_2
    #DEFINES += OMP_3_PLUS
    #win*-msvc*:QMAKE_CXXFLAGS -= /openmp
}

############################################################
# Workarounds ####################################### TODO #
############################################################

#DEFINES += _HAS_CXX17
#DEFINES += "_MSC_VER=1913"
DEFINES += "_HAS_STD_BYTE=0"

############################################################
# CUDA #####################################################
############################################################

win*-msvc* {
    # MSVC++ 14.0  _MSC_VER == 1900 (Visual Studio 2015 version 14.0)
    # MSVC++ 14.13 _MSC_VER == 1913 (Visual Studio 2017 version 15.6)
    # MSVC++ 14.14 _MSC_VER == 1914 (Visual Studio 2017 version 15.7)
    # MSVC++ 14.15 _MSC_VER == 1915 (Visual Studio 2017 version 15.8)

    # CUDA v8.0 is compatible with MSVC++ 14.0
    # CUDA v9.2 is compatible with VS15.6.7 (remove > 19.13 when seeing error)
    # CUDA must be installed AFTER MSVC

    # 1. Install Visual Studio.
    # 2. Install Visual Studio Build Tools.
    # 3. Turn off Qt Creator.
    # 4. Trun it on for Tools -> Options -> Compilers/Debuggers/Kits to refresh.
    # 5. Projects.
    # 6. Change these below.

    VS = "Enterprise" # "BuildTools"
    VS = "C:/Program Files (x86)/Microsoft Visual Studio/2017/$${VS}/VC/Tools/MSVC"
    UCRT = "C:/Program Files (x86)/Windows Kits/10/Source"

    VS = "$${VS}/14.13.26128" # 15.6.7
    #VS = "$${VS}/14.14.26428" # 15.7.6
    #VS = "$${VS}/14.15.26726" # 15.8

    #UCRT = "$${UCRT}/10.0.15063.0"
    UCRT = "$${UCRT}/10.0.16299.0" # 15.6.7 ???
    #UCRT = "$${UCRT}/10.0.17134.0" # 15.7.6

    VS_CMP = $$shell_quote($${VS}/bin/Hostx64/x64)

    # Some other directories:
    #$ VS140COMNTOOLS=C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\
    #$ VS150COMNTOOLS=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\
    #$ VSINSTALLDIR=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\
    #$ WindowsSdkVerBinPath=C:\Program Files (x86)\Windows Kits\10\bin\10.0.16299.0\
    #C:/Program Files (x86)/Microsoft Visual Studio/Shared
}

win*-msvc* {
    CUDA_VRSN = "v9.2"

    CUDA_TOOLKIT  = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA"/$${CUDA_VRSN}
    CUDA_SDK      = "C:/ProgramData/NVIDIA Corporation/CUDA Samples"/$${CUDA_VRSN}
    CUDA_NVCC_CMP = $${CUDA_TOOLKIT}/bin/nvcc.exe

    INCLUDEPATH += $${CUDA_TOOLKIT}/include \
                   $${CUDA_SDK}/common/inc

    ############################

    CUDA_SRCS += physics/cuda.cu

    ############################

    QMAKE_LIBDIR += $${CUDA_TOOLKIT}/lib/x64 \
                    $${CUDA_SDK}/common/lib/x64

    LIBS      += -lcudart -lcuda -lcublas
    CUDA_LIBS += -lcudart -lcuda -lcublas
}

win*-msvc* {
    INCLUDEPATH += $${UCRT}/ucrt \
                   $${UCRT}/ucrt/inc

    LIBS         += -L$${UCRT}/ucrt/x64

    Debug:LIBS   += -lucrtd # -llibucrtd
    Release:LIBS += -lucrt # -llibucrt

    # /MDd dynamic shared DLL, /MTd static, /LD dynamic DLL
    Debug:MSVCRT_LINK_FLAG   = "/MDd"
    Release:MSVCRT_LINK_FLAG = "/MD"

    CUDA_ARCH = arch=compute_61,code=sm_61 # 7th Gen, Pascal Arch, GTX1060
    NVCC_FLAGS = --use_fast_math --expt-relaxed-constexpr

    CUDA_INCL += $${CUDA_TOOLKIT}/include \
                 $${CUDA_SDK}/common/inc \
                 $${VS}/include \
                 $$_PRO_FILE_PWD_

    CUDA_INCL = $$join(CUDA_INCL,'" -I"','-I"','"')
}

############################################################
# Configuration of the Intermediate CUDA Compiler ##########
############################################################

win*-msvc* {
    linux:cudaIntr.clean = cudaIntrObj/*.o
    win*:cudaIntr.clean  = cudaIntrObj/*.obj
}

############################################################
# Configuration of the CUDA Compiler #######################
############################################################

Debug:CUDA_CMP   = cuda_d
Release:CUDA_CMP = cuda

$${CUDA_CMP}.input = CUDA_SRCS

equals(MSVCRT_LINK_FLAG, "/MDd") | equals(MSVCRT_LINK_FLAG, "/MD") {
    linux:$${CUDA_CMP}.output = ${QMAKE_FILE_BASE}_link.o
    win*:$${CUDA_CMP}.output  = ${QMAKE_FILE_BASE}_link.obj
    NVCC_FLAGS += --shared -cudart shared
}
equals(MSVCRT_LINK_FLAG, "/MTd") | equals(MSVCRT_LINK_FLAG, "/MT") {
    $${CUDA_CMP}.output = ${QMAKE_FILE_BASE}_link.a
}
equals(MSVCRT_LINK_FLAG, "/LDd") | equals(MSVCRT_LINK_FLAG, "/LD") {
    $${CUDA_CMP}.output = ${QMAKE_FILE_BASE}_link.dll
}

QMAKE_EXTRA_COMPILERS += $${CUDA_CMP}
$${CUDA_CMP}.dependency_type = TYPE_C

win*-msvc* {
    Debug:$${CUDA_CMP}.commands = \
        $${CUDA_NVCC_CMP} --verbose -g \
            $${CUDA_INCL} $${CUDA_LIBS} $${NVCC_FLAGS} \
            -D_DEBUG -DCOMPILER_MSVC \
            --machine 64 -gencode $${CUDA_ARCH} \
            -ccbin $${VS_CMP} \ # MSVC CL.exe directory
            -Xcompiler $${MSVCRT_LINK_FLAG} \ # dynamic/static release
            -Xcompiler "/openmp" \
            -Xcompiler "/nologo,/EHsc,/wd4819,/W3,/Zi,/Od" \
            -Xcompiler "/RTC1" \ # error checking, incompatible with /O2
            -c -o ${QMAKE_FILE_OUT} \ # output files
            ${QMAKE_FILE_NAME} # input files
    Release:$${CUDA_CMP}.commands = \
        $${CUDA_NVCC_CMP} --verbose \
            $${CUDA_INCL} $${CUDA_LIBS} $${NVCC_FLAGS} \
            -DCOMPILER_MSVC \
            --machine 64 -gencode $${CUDA_ARCH} \
            #--compiler-options "-D _WIN64" \
            #--cl-version=2017 \
            -std=$${CPP_STD_CUDA} \
            -ccbin $${VS_CMP} \ # MSVC CL.exe directory
            -Xcompiler $${MSVCRT_LINK_FLAG} \ # dynamic/static release
            -Xcompiler "/openmp" \
            -Xcompiler "/nologo,/EHsc,/wd4819,/W3,/Zi,/O2" \
            -Xcompiler "/arch:AVX2" \
            -c -o ${QMAKE_FILE_OUT} \ # output files
            #-dc -o ${QMAKE_FILE_OUT} \ # instead of -c for multiple .cu files / separate compilation and linking of CUDA code?
            ${QMAKE_FILE_NAME} # input files
}

#message($${$${CUDA_CMP}.commands}$$escape_expand(\r\n))
Debug:message($${cuda_d.commands}$$escape_expand(\r\n))
Release:message($${cuda.commands}$$escape_expand(\r\n))

############################################################
# Project files ############################################
############################################################

#QMAKE_EXT_H += .cuh

# Be careful with this. Executable will get too big needlessly!
RESOURCES += \
    $$_PRO_FILE_PWD_/graphics/shader \
    $$_PRO_FILE_PWD_/map \
    $$_PRO_FILE_PWD_/resources/sounds \

FORMS += \
    window/main_window.ui

DISTFILES += \
    graphics/shader/fragment_shader.fsh \
    graphics/shader/vertex_shader.vsh \
    map/sim_communicating_vessels.txt \
    map/sim_dam_break.txt \
    map/sim_dam_fall.txt \
    map/sim_droplet.txt \
    map/user_defined_obstacles.txt \
    resources/sounds/bloop.mp3

HEADERS += \
    control/interaction.h \
    graphics/form.h \
    graphics/gui.h \
    graphics/handles.h \
    graphics/matrices.h \
    graphics/paintable.h \
    graphics/shader/shader.h \
    graphics/vertex_array.h \
    object/obstacle.h \
    object/particle.h \
    object/segment.h \
    physics/collider.h \
    physics/computer.h \
    physics/forces.h \
    physics/geometry.h \
    physics/grid.h \
    physics/integrator.h \
    physics/kernelizer.h \
    physics/neighbourhood.h \
    physics/octree.h \
    physics/partitioning.h \
    physics/vector.h \
    shape/arrow.h \
    shape/dot.h \
    shape/line.h \
    shape/mesh.h \
    shape/shape.h \
    shape/sphere.h \
    shape/triangle.h \
    util/debugger.h \
    util/enums.h \
    util/constants.h \
    util/files.h \
    util/logger.h \
    util/macros.h \
    util/map.h \
    util/parser.h \
    util/settings.h \
    util/state.h \
    util/timer.h \
    window/gl_window.h \
    window/main_window.h \
    window/simulation_window.h

SOURCES += \
    control/interaction.cpp \
    graphics/form.cpp \
    graphics/gui.cpp \
    graphics/handles.cpp \
    graphics/matrices.cpp \
    graphics/paintable.cpp \
    graphics/shader/shader.cpp \
    graphics/vertex_array.cpp \
    main.cpp \
    object/obstacle.cpp \
    object/particle.cpp \
    object/segment.cpp \
    physics/collider.cpp \
    physics/computer.cpp \
    physics/forces.cpp \
    physics/geometry.cpp \
    physics/grid.cpp \
    physics/integrator.cpp \
    physics/kernelizer.cpp \
    physics/neighbourhood.cpp \
    physics/octree.cpp \
    physics/partitioning.cpp \
    physics/vector.cpp \
    shape/arrow.cpp \
    shape/dot.cpp \
    shape/line.cpp \
    shape/mesh.cpp \
    shape/shape.cpp \
    shape/sphere.cpp \
    shape/triangle.cpp \
    util/debugger.cpp \
    util/enums.cpp \
    util/files.cpp \
    util/logger.cpp \
    util/map.cpp \
    util/settings.cpp \
    util/state.cpp \
    util/timer.cpp \
    window/gl_window.cpp \
    window/main_window.cpp \
    window/simulation_window.cpp

message(== END ================================================================$$escape_expand(\r\n))
