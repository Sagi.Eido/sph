#pragma once
//#ifndef GRID_H
//#define GRID_H

#include "physics/partitioning.h"

using namespace std;

class Grid : public Partitioning
{
private:
    vector<Particle*>& getCell(Particle *p) override;
    vector<Particle*>& getCell(int i, int j, int k) override;

public:
    //static vector<vector<vector<vector<Particle*>>>> grid;
    vector<vector<Particle*>> grid;

    void updateNeighbours(Particle *p) override;

    void distributeParticles() override;
    void remove(Particle *p);
    void push(Particle &p) override;
    void getVolume() override;

    Grid();
};

//#endif // GRID_H
