#pragma once
//#ifndef OCTREE_H
//#define OCTREE_H

#include "physics/partitioning.h"

#include <cmath>
#include <iostream>

class Octree : public Partitioning
{
private:
    static int maxDepth;
    static int maxDepthX, maxDepthY, maxDepthZ;
    static int nodeCount;

    Octree *parent;
    Octree *corners[8];
    int nodeDepth;
    vector<Particle*> particles;

    Octree(int nodeDepth, Octree *parent, int corner, std::string placement);

    void clear();
    vector<Particle*>& getCell(Particle *p) override;
    vector<Particle*>& getCell(int i, int j, int k) override;

public:
    static Octree *root;

    double center[3];

    Octree();

    vector<Octree*> getLeaves();
    vector<Octree*> getNodes();
    void getLeaves(vector<Octree*> &leaves);
    void getNodes(vector<Octree*> &nodes);

    void updateNeighbours(Particle *p) override;
    void distributeParticles() override;
    void push(Particle &p) override;
    void getVolume() override;
};

//#endif // OCTREE_H
