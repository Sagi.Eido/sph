#include "physics/collider.h"

#include "control/interaction.h"
#include "object/particle.h"
#include "object/segment.h"
#include "physics/computer.h"
#include "physics/forces.h"
#include "physics/geometry.h"
#include "physics/partitioning.h"
#include "physics/vector.h"
#include "util/debugger.h"
#include "util/settings.h"
#include "util/timer.h"

#include <algorithm>
#include <iostream>

#include <omp.h>

#include "util/enums.h"
#include "util/macros.h"

extern void GPUCollide();

using namespace std;

std::vector<bool> Collider::particleCollision;
std::vector<double> Collider::particleCollisionDistance;
unsigned Collider::particleCollisionsPerStep = 0;
Timer Collider::timer = Timer();

bool triangularMatrix = true;

void Collider::initializeCollisions()
{
    // For triangular matrix n by n we need array n*(n+1)/2 length
    //     and transition rule is m[r][c] = a[r*n + c - r*(r+1)/2]

    int count;
    if (triangularMatrix)
        count = Settings::PARTICLE_COUNT * (Settings::PARTICLE_COUNT + 1) / 2;
    else
        count = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT;

    particleCollision = std::vector<bool>(count, false);
    particleCollisionDistance
        = std::vector<double>(count, std::numeric_limits<double>::infinity());
}

void Collider::resetCollisions()
{
    #pragma omp parallel if(Settings::PARALLEL_OMP)
    {
        static double infinity = std::numeric_limits<double>::infinity();
        LOOP_TYPE size;
        if (triangularMatrix)
            size = Settings::PARTICLE_COUNT * (Settings::PARTICLE_COUNT + 1) / 2;
        else
            size = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT;
        #pragma omp for
        for (LOOP_TYPE i = 0; i < size; ++i)
        {
            particleCollision[i] = false;
            particleCollisionDistance[i] = infinity;
        }
    }
}

map<string, variant<bool, double>>
Collider::getCollision(const Particle &p1, const Particle &p2)
{
    static int n = Settings::PARTICLE_COUNT;
    int i = p1.id - 1;
    int j = p2.id - 1;
    if (i > j) {
        std::swap(i, j);
    }
    int x;
    if (triangularMatrix)
        x = i*n + j - i*(i+1)/2;
    else
        x = i*n + j;
    map<string, variant<bool, double>> map;
    map["collision"] = variant<bool, double>(particleCollision[x]);
    map["distance"] = variant<bool, double>(particleCollisionDistance[x]);
    return map;
}

void Collider::setCollision(const Particle &p1, const Particle &p2,
                            bool happened, double distance)
{
    static int n = Settings::PARTICLE_COUNT;
    int i = p1.id - 1;
    int j = p2.id - 1;
    if (i > j) {
        int k = i;
        i = j;
        j = k;
    }
    int x;
    if (triangularMatrix)
        x = i*n + j - i*(i+1)/2;
    else
        x = i*n + j;
    particleCollision[x] = happened;
    particleCollisionDistance[x] = distance;
}

void Collider::applyEvaluatedCollisions()
{
    #pragma omp parallel for if(Settings::PARALLEL_OMP)
    for (LOOP_TYPE i = 0; i < Particle::flows[0].size(); ++i)
    {
        *Particle::flows[0][i]->r += *Particle::flows[0][i]->displacement;
        *Particle::flows[0][i]->v += *Particle::flows[0][i]->tilt;
    }
}

void Collider::naiveCollisionsCPU(bool execute)
{
    for (LOOP_TYPE i = 0; i < Particle::flows.size(); ++i) {
        for (LOOP_TYPE j = 0; j < Particle::flows.size(); ++j) {
            #ifdef OMP_3_PLUS
            #pragma omp parallel for \
                        if(Settings::PARALLEL_OMP) \
                        schedule(guided, 4)// \
                        //collapse(2)
            for (LOOP_TYPE k = 0; k < Particle::flows[i].size(); ++k) {
                for (LOOP_TYPE l = k; l < Particle::flows[j].size(); ++l) {
                    if (! (k == l && i == j)) {
                        collide(*Particle::flows[i][k], *Particle::flows[j][l]);
                    }
                }
            }
            #elif OMP_2
            #pragma omp parallel for \
                        if(Settings::PARALLEL_OMP) \
                        schedule(guided, 4) // TODO
            for (LOOP_TYPE k = 0; k < Particle::flows[i].size(); ++k) {
                for (LOOP_TYPE l = k+1; l < Particle::flows[j].size(); ++l) {
                    collideParticles(*Particle::flows[i][k], *Particle::flows[j][l], execute);
                }
            }
            #endif
        }
    }
}

void Collider::neighbourhoodCollisionsCPU(bool execute)
{
    #ifdef OMP_3_PLUS
    for (LOOP_TYPE i = 0; i < Particle::flows.size(); ++i) {
    //            #pragma omp parallel for collapse(2) schedule(guided) \
    //                                 if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            for (LOOP_TYPE k = 0; k < Particle::flows[i][j]->neighbours->size(); ++k) {
                collide(*Particle::flows[i][j],
                                *Particle::flows[i][j]->neighbours->at(k));
            }
        }
    }
    #elif OMP_2
    for (LOOP_TYPE i = 0; i < Particle::flows.size(); ++i) {
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            for (LOOP_TYPE k = 0; k < Particle::flows[i][j]->neighbours->size(); ++k) {
                collideParticles(
                    *Particle::flows[i][j],
                    *Particle::flows[i][j]->neighbours->at(k),
                    execute);
            }
        }
    }
    #endif
}

void Collider::obstacleCollisions(bool execute)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i)
    {
    #ifdef OMP_3_PLUS
        #pragma omp parallel for collapse(2) if(Settings::PARALLEL_OMP)
        for (unsigned j = 0; j < Particle::flows[i].size(); ++j)
            for (unsigned k = 0; k < Obstacle::obstacles.size(); ++k)
                Obstacle::obstacles[k]->collide(Particle::flows[i][j], true);
    #elif OMP_2
        LOOP_TYPE obstacles_size = static_cast<LOOP_TYPE>(Obstacle::obstacles.size());
        #pragma omp parallel if(Settings::PARALLEL_OMP)
        {
            Particle *p;
            #pragma omp for
            for (LOOP_TYPE n = 0;
                 n < Particle::flows[i].size() * obstacles_size;
                 ++n)
            {
                LOOP_TYPE j = n / (LOOP_TYPE) obstacles_size;
                LOOP_TYPE k = n % obstacles_size;

                p = Particle::flows[i][j];

                // TODO
                // Obstacle::obstacles[k]->collide access breaks
                //     paralellization
                map<string, variant<double, Vector>> collisionResponse
                    = Obstacle::obstacles[k]->collide(p, execute);
                *p->displacement
                    += get<Vector>(collisionResponse["displacement"]);
                *p->tilt
                    += get<Vector>(collisionResponse["tilt"]);

                //Interaction::playBloop();
            }
        }
    #endif
    }
}

void Collider::particleCollisions()
{
    if (! Settings::COLLIDE_NGHBRS_ONLY && ! Settings::PARALLEL_CUDA)
    {
        naiveCollisionsCPU(Settings::COLLIDE_IMMEDIATE);
    }
    else if (Settings::COLLIDE_NGHBRS_ONLY && ! Settings::PARALLEL_CUDA)
    {
        neighbourhoodCollisionsCPU(Settings::COLLIDE_IMMEDIATE);
    }
    #ifdef COMPILER_MSVC
    else if (Settings::PARALLEL_CUDA)
    {
        std::cout << "GPU collisions" << std::endl << std::flush;
        GPUCollide();
    }
    #endif

    std::cout << particleCollisionsPerStep
        << " <- Particle-Particle collisions per step."
        << std::endl << std::flush;
}

void Collider::collideParticles()
{
    particleCollisionsPerStep = 0;
    resetCollisions();
    Computer::forEachParticleParallel([](Particle *p) -> void {
        *p->dt_left = 1. / Settings::REQUESTED_FRAMERATE; // TODO 1. or Settings::dt ???
        p->displacement->zero();
        p->tilt->zero();
    });

    if (Settings::PARTICLES_COLLIDE)
    {
        timer.measureTime(
            "Particle-Particle colliding",
            []() { particleCollisions(); });
    }

    timer.measureTime(
        "Particle-Obstacle colliding",
        [&]() { obstacleCollisions(Settings::COLLIDE_IMMEDIATE); });

    if (! Settings::COLLIDE_IMMEDIATE)
    {
        applyEvaluatedCollisions();
    }

    #pragma omp parallel for if (Settings::PARALLEL_OMP)
    for (LOOP_TYPE i = 0; i < Particle::flows[0].size(); ++i)
    {
        collideParticleWithLimitingPlanes(Particle::flows[0][i]);
    }
}

bool Collider::doCollide(const Particle &p1, const Particle &p2) { // Elastic Collision
    bool doCollide = false;
    double distanceBorder = p1.r->dist(*p2.r) - p1.radius - p2.radius;
    if (distanceBorder < 0.) {
        doCollide = true;
        #pragma omp critical
        {
            ++particleCollisionsPerStep;
        }
        setCollision(p1, p2, true, distanceBorder);
    }
    return doCollide;
}

void Collider::collisionDetect()
{
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
        for (unsigned j = i+1; j < Particle::flows[0].size(); ++j)
            doCollide(*Particle::flows[0][i], *Particle::flows[0][j]);
}

void Collider::collideParticles(const Particle &p1, const Particle &p2, bool execute) {
    if (! get<bool>(getCollision(p1, p2)["collision"]))
    {
        if (doCollide(p1, p2)) {
            //double exactCollisionTime = 0.;

            Vector sphereNormal = (*p1.r - *p2.r).normalized();
            // TODO
            //     guarding situation where normal is (0, 0, 0),
            //     because distance is 0, but that means we're
            //     guarding against performing collision
            //     (which can't be performed) when at the same
            //     time we clearly have a collision, as two particles
            //     taking same position in space sure is a collision,
            //     though it shouldn't have happened in the first place
            //     (particles shouldn't be able to acquire such state)
            //     and should collide earlier, when is still possible
            //     to evaluate a collision, so the way we're guarding
            //     it may hide the actual problem
            if (! sphereNormal.isZero())
            {
                double distanceBorder
                    = get<double>(getCollision(p1, p2)["distance"]);

                if (! p2.isStationary)
                {
                    Vector new_p2_r = Vector();
                    new_p2_r = *p2.r + sphereNormal * distanceBorder;
                    *p2.displacement += new_p2_r - *p2.r;
                    if (execute)
                    {
                        #pragma omp critical
                        {
                            *p2.r = new_p2_r;
                        }
                    }
                }

                // TODO
                //    it shouldn't have come to this check!
                if (*p1.r != *p2.r)
                {
                    Vector new_p1_v;
                    Vector new_p2_v;
                    if (! p1.isStationary)
                    {
                        new_p1_v
                            = (*p1.v -
                                ((*p1.r - *p2.r)
                                 * (*p1.v - *p2.v).dot(*p1.r - *p2.r)
                                 / pow(p1.r->dist(*p2.r), 2)
                                 * 2 * p2.m / (p1.m + p2.m))
                            ) * (1. - Settings::FORCE_DAMPING_WATER);
                        *p1.tilt += new_p1_v - *p1.v;
                    }
                    if (! p2.isStationary)
                    {
                        new_p2_v
                            = (*p2.v -
                                ((*p2.r - *p1.r)
                                 * (*p2.v - *p1.v).dot(*p2.r - *p1.r)
                                 / pow(p2.r->dist(*p1.r), 2)
                                 * 2 * p1.m / (p1.m + p2.m))
                             ) * (1. - Settings::FORCE_DAMPING_WATER);
                        *p2.tilt += new_p2_v - *p2.v;
                    }
                    if (execute)
                    {
                        #pragma omp critical
                        {
                            *p1.v = new_p1_v;
                            *p2.v = new_p2_v;
                        }
                    }
                }
            }
        }
    }
}

std::map<std::string, std::variant<double, Vector>>
Collider::collideParticleWithSegment(Particle *p, Segment *w, bool execute) {
    variant<double, Vector> minDepth = numeric_limits<double>::max();
    variant<double, Vector> displacement = Vector();
    variant<double, Vector> tilt = Vector();

    if (! p->isGhost)
    {
        if (w->lines.size() > 1)
        {
            Debugger::throwWithMessage("133");
        }
        for (unsigned i = 0; i < w->lines.size(); ++i)
        {
            geometry::Line ptclPath = geometry::Line(*p->r_former, *p->r);

            geometry::Line linePath;
            Vector ln;

            // make wall two-sided
            if (pointLiesFrontOfLine(*p->r_former, w->linePaths[i]))
            {
                linePath = w->linePaths[i];
                ln = w->linePathNormals[i];
            }
            else
            {
                linePath = w->linePathsReversed[i];
                ln = w->linePathNormalsReversed[i];
            }

            if (
                ptclPath.p1 != ptclPath.p2
                && ! ptclPath.p1.isZero() // TODO, why required?
            ) {
                // TODO // particles on the back side of the wall (make walls two sided)
                //if ((pointToLine(*p->r, linePath).normalized() - ln).abs()
                //    < Vector(EPS, EPS, EPS))
                //{
                //    ln *= -1;
                //}

                if (! pointLiesFrontOfLine(*p->r - ln * p->radius, linePath))
                { // passed the line in which wall lays
                    double currentCollisionDepth
                        = p->radius - segSegDist(ptclPath, linePath);
                    if (currentCollisionDepth >= 0) // TODO
                    { // passed the wall, not only the line in which it lays
                        if (pointProjectsToSeg(*p->r, linePath)) // TODO
                        { // passed the wall, not it's edge
                            minDepth = min(
                                currentCollisionDepth, get<double>(minDepth));

                            tilt = Vector(
                                - ln * p->v->dot(ln)
                                * (1 + Settings::FORCE_DAMPING_WALL));
                            Vector new_r
                                = pointSegClosestPoint(*p->r, linePath)
                                  + ln * p->radius;
                            displacement = new_r - *p->r;

                            if (execute)
                            {
                                *p->v += get<Vector>(tilt);
                                *p->r = new_r;
                            }

                            // TODO
                            //break; //////////////////////////////////////////
                        }
                    }
                }
            }
        }
    }

    minDepth = get<double>(minDepth) < numeric_limits<double>::max()
                   ? minDepth : -1.;

    std::map<string, std::variant<double, Vector>> map;
    map["depth"] = minDepth;
    map["displacement"] = displacement;
    map["tilt"] = tilt;
    return map;
}

void Collider::collideParticleWithLimitingPlanes(Particle *p)
{
    if (p->cell[0] == 0 || p->cell[1] == 0 || p->cell[2] == 0
     || p->cell[0] == Partitioning::cellCountX - 1
     || p->cell[1] == Partitioning::cellCountY - 1
     || p->cell[2] == Partitioning::cellCountZ - 1)
    {
        static double lim = Settings::ARENA_DIAMETER / 2
                            - Settings::PARTICLE_INIT_DIST
                              * Settings::GHOST_LAYER_GAGE;
        static double limz = Settings::ARENA_DIAMETER_Z / 2
                             - Settings::PARTICLE_INIT_DIST
                               * Settings::GHOST_LAYER_GAGE;

        static geometry::Plane planes[] = {
            geometry::Plane(Vector(-lim + p->radius, 0, 0),  Vector( 1,  0,  0)),
            geometry::Plane(Vector( lim - p->radius, 0, 0),  Vector(-1,  0,  0)),
            geometry::Plane(Vector(0, -lim + p->radius, 0),  Vector( 0,  1,  0)),
            geometry::Plane(Vector(0,  lim - p->radius, 0),  Vector( 0, -1,  0)),
            geometry::Plane(Vector(0, 0, -limz + p->radius), Vector( 0,  0,  1)),
            geometry::Plane(Vector(0, 0,  limz - p->radius), Vector( 0,  0, -1))
        };

        while (true)
        {
            bool particleInBounds = true;
            for (geometry::Plane &plane : planes)
            {
                if (! pointLiesFrontOfPlane(*p->r, plane)) {
                    if      (plane.point.x != 0) p->r->x = plane.point.x;
                    else if (plane.point.y != 0) p->r->y = plane.point.y;
                    else if (plane.point.z != 0) p->r->z = plane.point.z;
                    *p->v -= plane.normal * (p->v->dot(plane.normal))
                             * (2 - Settings::FORCE_DAMPING_WALL);
                    particleInBounds = false;
                }
            }
            if (particleInBounds) break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//// not in use currently //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Collider::collideParticles(const Particle &p1, const std::vector<Particle*> *with) {
    double former_p1_dt_left = *p1.dt_left;
    Particle *p2 = nullptr;
    while (*p1.dt_left > 0. && fabs(former_p1_dt_left - *p1.dt_left) > 0.01)
    {
        double formerECT = 0.; // former exact collision time
        for (unsigned i = 0; i < with->size(); ++i)
        {
            p2 = with->at(i);
            if (p2->id != p1.id)
            {
                double eCT = 0.; // exact collision time
                Vector dist = *p1.r_former - *p2->r_former;
                Vector relative_v = *p1.v - *p2->v;
                double radiuses = p1.radius + p2->radius;
                double a = relative_v.dot(relative_v);
                double b = relative_v.dot(dist);
                double c = dist.dot(dist) - radiuses * radiuses;
                double d = b*b - a*c;

                if (c < 0.) { // already overlap
                    eCT = 0.;
                }
                if (b >= 0.) { // don't move towards each other
                    continue; //////////////////////////////////////////////////
                }
                if (d < 0.) { // no roots, no collision
                    continue; //////////////////////////////////////////////////
                }
                if (b < 0. && d >= 0.) {
                    eCT = (-b - sqrt(d)) / a;
                }

                if (fabs(eCT - formerECT) < 0.01) { // paricle is most likely locked between others
                    continue; //////////////////////////////////////////////////
                }
                if (eCT < 0.01 || eCT > *p1.dt_left || eCT > *p2->dt_left) {
                    continue; //////////////////////////////////////////////////
                }
                formerECT = eCT;

                setCollision(p1, *p2, true, c);

                *p1.dt_left -= eCT;
                *p2->dt_left -= eCT;
                *p1.r = *p1.r_former + *p1.v * eCT;
                *p2->r = *p2->r_former + *p2->v * eCT;

                std::cout << "distance - r's = " << p1.r->dist(*p2->r) - p1.radius - p2->radius << std::endl << std::flush;
                std::cout << "exactCollisionTime: " << eCT << std::endl << std::flush;
                std::cout << "p" << p1.id << " dt_left: " << *p1.dt_left << std::endl << std::flush;

                Vector p2_v = Vector(*p2->v);
                if (! p2->isStationary)
                {
                    #pragma omp critical
                    {
                        *p2->v -=
                            (*p2->r - *p1.r)
                            * (*p2->v - *p1.v).dot(*p2->r - *p1.r)
                            / pow(p2->r->dist(*p1.r), 2)
                            * 2 * p1.m / (p1.m + p2->m);
                        *p2->v *= (1. - Settings::FORCE_DAMPING_WATER);
                    }
                }
                if (! p1.isStationary)
                {
                    #pragma omp critical
                    {
                        *p1.v -=
                            (*p1.r - *p2->r)
                            * (*p1.v - p2_v).dot(*p1.r - *p2->r)
                            / pow(p1.r->dist(*p2->r), 2)
                            * 2 * p2->m / (p1.m + p2->m);
                        *p1.v *= (1. - Settings::FORCE_DAMPING_WATER);
                    }
                }
            }
        }
        former_p1_dt_left = *p1.dt_left;
    }
}
