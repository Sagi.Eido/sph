#include "physics/computer.h"

#include "control/interaction.h"
#include "object/obstacle.h"
#include "object/particle.h"
#include "physics/collider.h"
#include "physics/forces.h"
#include "physics/grid.h"
#include "physics/integrator.h"
#include "physics/kernelizer.h"
#include "physics/neighbourhood.h"
#include "physics/vector.h"
#include "util/logger.h"
#include "util/macros.h"
#include "util/settings.h"
#include "util/timer.h"
#include "window/simulation_window.h" // for current frame number

#include <iostream>
#include <limits>

#include <omp.h>

extern void GPUUpdateNeighbours();

Computer *Computer::currentComputer = new Computer();
Timer Computer::timer = Timer();

Computer::Computer() {}

////////////////////////////////////////////////////////////////////////////////

void Computer::zeroInternalForces()
{
    forEachParticleParallel([](Particle *p, int i, int j) -> void {
        p->F_former = Particle::flows[i][j]->F;
        p->F->zero();
    });
}

void Computer::zeroExternalForces()
{
    forEachParticleParallel([](Particle *p) -> void { p->F_ext->zero(); });
}

void Computer::evaluateSPHForces()
{
    Partitioning::net != nullptr ? timer.measureTime("Distribute particles", std::bind(&Partitioning::distributeParticles, Partitioning::net)) : double();
    Logger::collectStatistics(); // Evaluating max/avg rho/v/F_P/F_visc/F_surf_tens
    zeroInternalForces();

    if (! Settings::PARALLEL_CUDA)
    {
        timer.measureTime("Update neighbours", neighbourhood::updateNeighboursAll);
    }
    #ifdef COMPILER_MSVC
    else
    {
        timer.measureTime("Update neighbours on GPU", GPUUpdateNeighbours);
    }
    #endif

    if (Settings::GRANULAR_OR_LIQUID)
    {
        timer.measureTime("Update rho & p and find boundaries", Computer::updateDensityAndPressure);
    }
    timer.measureTime("Compute p, viscosity & other Fs", Computer::computeForces);
    zeroExternalForces();
}

////////////////////////////////////////////////////////////////////////////////

void Computer::computeVectors(double dt)
{
    Computer::currentComputer->evaluateSPHForces();
    Collider::collideParticles();
    timer.measureTime("Integration", std::bind(Integrator::integrate, dt));
    Partitioning::net != nullptr ? Partitioning::net->getVolume() : void();
}

////////////////////////////////////////////////////////////////////////////////

void Computer::updateDensityAndPressure()
{
    Computer::forEachParticleParallel([](Particle *p) -> void {
        Computer::updateDensity(*p);
        Computer::updatePressure(*p);
    });
}

void Computer::computeForces()
{
    Computer::forEachParticleParallelGuided([](Particle *p) -> void {
        if (Settings::GRANULAR_OR_LIQUID && !p->isSolid) {
            Computer::computePressureForces(*p);
            Computer::computeViscosityForces(*p);
            Computer::computeBuoyancyForces(*p);
            Computer::computeSurfaceTensionForces(*p); // TODO
        }
        Computer::computeOtherForces(*p);
    });
}

////////////////////////////////////////////////////////////////////////////////

Vector Computer::neighbourBasedVelocity(Particle &p)
{
    Vector v = Vector();
    Particle *n = nullptr;
    if (Settings::PARALLEL_CUDA)
    {
        #ifdef COMPILER_MSVC
        int start = (p.id - 1) * Settings::PARTICLE_COUNT;
        for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
        {
            n = p.getParentFlowParticle(i - start);
            v += *n->v * (n->m / n->rho) * Kernelizer::defaultKernel(&p, n);
        }
        #endif
    }
    else
    {
        for (unsigned i = 0; i < p.neighbours->size(); ++i)
        {
            n = (*p.neighbours)[i];
            v += *n->v * (n->m / n->rho) * Kernelizer::defaultKernel(&p, n);
        }
    }
    return v;
}

////////////////////////////////////////////////////////////////////////////////

void Computer::updateDensity(Particle &p)
{
    if (Settings::PARALLEL_CUDA)
    {
        #ifdef COMPILER_MSVC
//        if (p.neighbours == nullptr)
//        {
//            p.neighbours = new std::vector<Particle*>();
//        }
//        else
//        {
//            p.neighbours->clear();
//        }

//        int start = (p.id - 1) * Settings::PARTICLE_COUNT;
//        for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
//        {
//            //std::cout << Particle::neighbours_host[i];
//            if (Particle::neighbours_host[i])
//            {
//                p.neighbours->push_back(p.getParentFlowParticle(i));
//                p.getParentFlowParticle(i)->neighbours->push_back(p.getParentFlowParticle(p.id - 1));
//            }
//            std::cout << std::flush;
//        }
//        std::cout << std::endl << std::flush;

        p.rho = 0.;
        int start = (p.id - 1) * Settings::PARTICLE_COUNT;
        for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
        {
            if (Particle::neighbours_host[i])
            {
                p.rho += p.getParentFlowParticle(i - start)->m
                       * Kernelizer::defaultKernel(&p, p.getParentFlowParticle(i - start));
            }
        }
        #endif
    }
    else
    {
        ///////////// TODO, why keep density if no neighbours?
        if (p.neighbours->size() > 0)
        {
            p.rho = 0.;

            //#pragma omp parallel for reduction(+:p.rho) if(Settings::PARALLEL_OMP)
            for (unsigned i = 0; i < p.neighbours->size(); ++i)
            {
                p.rho += (*p.neighbours)[i]->m
                       * Kernelizer::defaultKernel(&p, (*p.neighbours)[i]);
            }
            //rho = rho_local;
        }
    }
}

void Computer::updatePressure(Particle &p)
{
    static double K_0   = Settings::STIFFNESS_CONSTANT;
    static double rho_0 = Settings::DESIRED_REST_DENSITY;
    static double P_0   = 0;//101325; // 101325 Pa
    static double gamma = 7.15; // for water
    // rho_0 / gamma =~ 300

    // Tait equation (in pressure form) / Murnaghan equation of state
    p.P = (K_0/gamma) * (pow(p.rho/rho_0, gamma) - 1) + P_0;

    // double c_s = Settings::SOUND_SPEED;
    //P = c_s * c_s * (rho - rho_0);
}

void Computer::computePressureForces(Particle &p)
{
    if (Settings::PARALLEL_CUDA || p.neighbours->size() > 0)
    {
        Vector F_P_new = Vector();
        Particle *n = nullptr;

        static unsigned method = 1;

        if (method == 1) // Monaghan
        {
            if (Settings::PARALLEL_CUDA)
            {
                #ifdef COMPILER_MSVC
                int start = (p.id - 1) * Settings::PARTICLE_COUNT;
                for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
                {
                    if (Particle::neighbours_host[i])
                    {
                        n = p.getParentFlowParticle(i - start);
                        //#pragma omp critical
                        F_P_new +=
                            Kernelizer::pressureKernelGradient(&p, n)
                            * ((p.P/pow(p.rho, 2) + n->P/pow(n->rho, 2)) * n->m);
                    }
                }
                F_P_new *= - p.rho;
                #endif
            }
            else
            {
                //#pragma omp parallel for shared(P_grad) if(Settings::PARALLEL_OMP)
                for (unsigned i = 0; i < p.neighbours->size(); ++i)
                {
                    n = (*p.neighbours)[i];
                    //#pragma omp critical
                    F_P_new +=
                        Kernelizer::pressureKernelGradient(&p, n)
                        * ((p.P/pow(p.rho, 2) + n->P/pow(n->rho, 2)) * n->m);
                }
                F_P_new *= - p.rho;
            }
        }

        else if (method == 2) // Mueller
        {
            //#pragma omp parallel for shared(P_grad) if(Settings::PARALLEL_OMP)
            for (unsigned i = 0; i < p.neighbours->size(); ++i)
            {
                n = (*p.neighbours)[i];
                //#pragma omp critical
                F_P_new +=
                    Kernelizer::pressureKernelGradient(&p, n)
                    * ((n->m / n->rho) * (p.P + n->P) / 2);
            }
            F_P_new *= - 1;
        }

        if (Settings::PRESSURE_KERNEL_SLOPE != SPIKY_DEBRUN)
            F_P_new *= -1;

        *p.F_P = F_P_new;
        *p.F += F_P_new;
    }
}

void Computer::computeViscosityForces(Particle &p)
{
    if (Settings::PARALLEL_CUDA || p.neighbours->size() > 0)
    {
        Vector F_vsc_new = Vector();
        Particle *n = nullptr;

        //*v = neighbourBasedVelocity();

        static unsigned method = 1;

        if (Settings::VISCOSITY_KERNEL_SLOPE == VISCOSITY_SPECIFIC_KERNEL)
        {
            method = 2;
        }

        if (method == 1)
        {
            if (Settings::PARALLEL_CUDA)
            {
                #ifdef COMPILER_MSVC
                int start = (p.id - 1) * Settings::PARTICLE_COUNT;
                for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
                {
                    if (Particle::neighbours_host[i])
                    {
                        n = p.getParentFlowParticle(i - start);
                        //#pragma omp critical
                        F_vsc_new +=
                            (*p.v - *n->v)
                            * ((n->m / n->rho)
                               * (*p.r - *n->r).dot(Kernelizer::viscosityKernelGradient(&p, n))
                               / ((*p.r - *n->r).dot(*p.r - *n->r)
                                  + 0.01 * pow(p.smoothingLength, 2)));
                    }
                }
                F_vsc_new *= - 2 * p.viscosity * p.m;
                #endif
            }
            else
            {
                //#pragma omp parallel for shared(v_grad_div) if(Settings::PARALLEL_OMP)
                for (unsigned i = 0; i < p.neighbours->size(); ++i)
                {
                    n = (*p.neighbours)[i];
                    //#pragma omp critical
                    F_vsc_new +=
                        (*p.v - *n->v)
                        * ((n->m / n->rho)
                           * (*p.r - *n->r).dot(Kernelizer::viscosityKernelGradient(&p, n))
                           / ((*p.r - *n->r).dot(*p.r - *n->r)
                              + 0.01 * pow(p.smoothingLength, 2)));
                }
                F_vsc_new *= - 2 * p.viscosity * p.m;
            }
        }
        else if (method == 2)
        {
            for (unsigned i = 0; i < p.neighbours->size(); ++i)
            {
                n = (*p.neighbours)[i];
                F_vsc_new +=
                    (*n->v - *p.v)
                    * ((n->m / n->rho)
                       * Kernelizer::viscosityKernelLaplacian(&p, n));
            }
            F_vsc_new *= Settings::VISCOSITY;
        }

        *p.F_vsc = F_vsc_new;
        *p.F += F_vsc_new;
    }
}

void Computer::computeBuoyancyForces(Particle &p)
{
    Vector F_b_new = Vector();

    F_b_new = Forces::gravityVector()
              * (Settings::BUOYANCY_DIFFUSION_COEF
                 * (p.rho - Settings::DESIRED_REST_DENSITY));

    *p.F_b = F_b_new;
    *p.F += F_b_new;
}

void Computer::computeSurfaceTensionForces(Particle &p)
{
    if (Settings::PARALLEL_CUDA || p.neighbours->size() > 0)
    {
        double color_field       = 0.;
        Vector inward_normal     = Vector();
        Vector inward_normal_div = Vector();

        if (Settings::PARALLEL_CUDA)
        {
            #ifdef COMPILER_MSVC
            Particle *n = nullptr;
            int start = (p.id - 1) * Settings::PARTICLE_COUNT;
            for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
            {
                if (Particle::neighbours_host[i])
                {
                    n = p.getParentFlowParticle(i - start);
                    color_field       += Kernelizer::defaultKernel(&p, n)          * n->m / n->rho;
                    inward_normal     += Kernelizer::defaultKernelGradient(&p, n)  * n->m / n->rho;
                    inward_normal_div += Kernelizer::defaultKernelLaplacian(&p, n) * n->m / n->rho;
                }
            }
            #endif
        }
        else
        {
            Particle *n = nullptr;
            for (unsigned i = 0; i < p.neighbours->size(); ++i)
            {
                n = (*p.neighbours)[i];
                color_field       += Kernelizer::defaultKernel(&p, n)          * n->m / n->rho;
                inward_normal     += Kernelizer::defaultKernelGradient(&p, n)  * n->m / n->rho;
                inward_normal_div += Kernelizer::defaultKernelLaplacian(&p, n) * n->m / n->rho;
            }
        }

        *p.inwardSurfaceNormal = inward_normal;

        //if (p.rho < Settings::DESIRED_REST_DENSITY) {
        //if (inward_normal.norm() > EPS) {
        //if (inward_normal.norm() > 0.2) {
        if (inward_normal.norm() > Settings::SURFACE_INDICATOR) {
            p.boundary = true;
            p.overpressured = false;

            Vector kappa_curvature
                = - inward_normal_div / inward_normal.norm();

            Vector F_tns_new = kappa_curvature // TODO
                               * inward_normal
                               * - Settings::SURFACE_TENSION_COEF;

            // TODO to get rid off
            // the one above should work better but it doesn't
            // it relies on default kernel (Quintic currently) laplacian
            // perhaps its equations aren't proper (Wolfram-evaluated)
            F_tns_new = - inward_normal
                        / inward_normal.norm()
                        * Settings::SMOOTHING_LENGTH
                        * Settings::SURFACE_TENSION_COEF;

            *p.F_tns = F_tns_new;
            *p.F += *p.F_tns;
        }
        else
        {
            p.boundary = false;
            p.overpressured = true;
        }
    }
}

void Computer::computeOtherForces(Particle &p)
{
    if (! Settings::RHO_GRAVITY)
    {
        Forces::gravityEarth(p);
    }
    else
    {
        Forces::rhoGravityEarth(p);
    }

    if (Settings::FORCE_GRAV_UNIVERSAL_X != 0.)
        for (int i = 0; i < p.getParentFlow().size(); ++i)
            Forces::universalGravitation(p, *p.getParentFlowParticle(i));

    *p.F += *p.F_ext;
}

////////////////////////////////////////////////////////////////////////////////

void Computer::evaluateMaximalDensity()
{
    Particle::rho_max = 0;
    Particle::rho_avg = 0;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->rho;
        if (norm > Particle::rho_max) {
            Particle::rho_max = norm;
        }
        Particle::rho_avg += norm;
    }
    Particle::rho_avg /= Particle::flows[0].size();
}

void Computer::evaluateMaximalVelocity()
{
    Particle::v_max_norm = - std::numeric_limits<double>::infinity();
    Particle::v_avg_norm = 0.;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->v->norm();
        if (norm > Particle::v_max_norm) {
            Particle::v_max_norm = norm;
        }
        Particle::v_avg_norm += norm;
    }
    Particle::v_avg_norm /= Particle::flows[0].size();
}
void Computer::evaluateMaximalPressure()
{
    Particle::F_P_max_norm = - std::numeric_limits<double>::infinity();
    Particle::F_P_avg_norm = 0.;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->F_P->norm();
        if (norm > Particle::F_P_max_norm) {
            Particle::F_P_max_norm = norm;
        }
        Particle::F_P_avg_norm += norm;
    }
    Particle::F_P_avg_norm /= Particle::flows[0].size();
}
void Computer::evaluateMaximalViscosity()
{
    Particle::F_vsc_max_norm = - std::numeric_limits<double>::infinity();
    Particle::F_vsc_avg_norm = 0.;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->F_vsc->norm();
        if (norm > Particle::F_vsc_max_norm) {
            Particle::F_vsc_max_norm = norm;
        }
        Particle::F_vsc_avg_norm += norm;
    }
    Particle::F_vsc_avg_norm /= Particle::flows[0].size();
}
void Computer::evaluateMaximalBuoyancy()
{
    Particle::F_b_max_norm = - std::numeric_limits<double>::infinity();
    Particle::F_b_avg_norm = 0.;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->F_b->norm();
        if (norm > Particle::F_b_max_norm) {
            Particle::F_b_max_norm = norm;
        }
        Particle::F_b_avg_norm += norm;
    }
    Particle::F_b_avg_norm /= Particle::flows[0].size();
}
void Computer::evaluateMaximalTension()
{
    Particle::F_tns_max_norm = - std::numeric_limits<double>::infinity();
    Particle::F_tns_avg_norm = 0.;
    double norm = 0.;
    for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
        norm = Particle::flows[0][i]->F_tns->norm();
        if (norm > Particle::F_tns_max_norm) {
            Particle::F_tns_max_norm = norm;
        }
        Particle::F_tns_avg_norm += norm;
    }
    Particle::F_tns_avg_norm /= Particle::flows[0].size();
}

////////////////////////////////////////////////////////////////////////////////

void Computer::forEachParticleNonParallel(std::function<void(Particle*)> f)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j)
            f(Particle::flows[i][j]);
}

void Computer::forEachParticleParallel(std::function<void(Particle*)> f)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i) {
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            f(Particle::flows[i][j]);
        }
    }
}

void Computer::forEachParticleParallelGuided(std::function<void(Particle*)> f)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i) {
        #pragma omp parallel for schedule(guided) if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            f(Particle::flows[i][j]);
        }
    }
}

void Computer::forEachParticleNonParallel(std::function<void(Particle*, int, int)> f)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j)
            f(Particle::flows[i][j], i, j);
}

void Computer::forEachParticleParallel(std::function<void(Particle*, int, int)> f)
{
    for (unsigned i = 0; i < Particle::flows.size(); ++i) {
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            f(Particle::flows[i][j], i, j);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
