#pragma once
//#ifndef PARTITIONING_H
//#define PARTITIONING_H

class Particle;

#include <vector>

using namespace std;

class Partitioning
{
protected:
    std::vector<int> getCellCoordinates(Particle *p);

public:
    static Partitioning *net;
    static int cellCount;
    static int cellCountX, cellCountY, cellCountZ;

    void remove(Particle *p);

    virtual void updateNeighbours(Particle *p);

    virtual void distributeParticles() = 0;
    virtual void push(Particle &p) = 0;
    virtual vector<Particle*>& getCell(Particle *p) = 0;
    virtual vector<Particle*>& getCell(int i, int j, int k) = 0;
    virtual void getVolume() = 0;
};

//#endif // PARTITIONING_H
