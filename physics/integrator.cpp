#include "integrator.h"

#include "object/particle.h"
#include "physics/vector.h"
#include "util/settings.h"
#include "util/timer.h"

#include <iostream>

#include <omp.h>

#include "util/macros.h"

void Integrator::forwardEuler(const Particle &p, double dt)
{
    // v_n+1 = v_n + a_n * dt
    // r_n+1 = r_n + v_n * dt

    *p.v += *p.F_former/p.m * dt;
    *p.r += *p.v_former * dt;
}
void Integrator::symplecticEuler(const Particle &p, double dt)
{
    // v_n+1 = v_n + a_n   * dt
    // r_n+1 = r_n + v_n+1 * dt

    *p.v += *p.F_former/p.m * dt;
    *p.r += *p.v * dt;
}
void Integrator::backwardEuler(const Particle &p, double dt)
{
    // v_n+1 = v_n + a_n+1 * dt
    // r_n+1 = r_n + v_n+1 * dt

    *p.v += *p.F/p.m * dt;
    *p.r += *p.v * dt;
}
void Integrator::midPoint(const Particle &p, double dt)
{
    // v_n+1 = v_n + a_n * dt
    // r_n+1 = r_n + v_n * dt + 0.5 * a_n * dt^2

    // ???

    *p.v += *p.F_former/p.m * dt;
    *p.r += *p.v_former * dt + *p.F_former/p.m/2 * dt * dt;
}
void Integrator::halfStep(const Particle &/*p*/, double /*dt*/)
{
    // v_0.5 = v_0 + 0.5 * a_0 * dt
    // v_n+0.5 = v_n-0.5 + a_n * dt
    // r_n+1 = r_n + v_n+0.5 * dt

    // ???


}
void Integrator::velocityVerlet(const Particle &p, double dt)
{
    // v_n+1 = v_n + (a_n + a_n+1)/2 * dt
    // r_n+1 = r_n + v_n * dt + 0.5 * a_n * dt^2

    *p.v  += (*p.F_former + *p.F)/p.m/2 * dt;
    *p.r  += *p.v_former * dt + *p.F_former/p.m/2 * dt * dt;
}
void Integrator::leapfrog(const Particle &/*p*/, double /*dt*/) {
    // numerically the same as Velocity Verlet
    // some advantages however
    // uses half-steps
}
void Integrator::rk4(const Particle &/*p*/, double /*dt*/)
{
    // v_1 = v_n
    // a_1 = v_n * dt ???
    // v_2 = v_n + a_1 * dt/2
    // a_2 = a_1 + v_1 * dt/2 ???
    // v_3 = v_n + a_2 * dt/2
    // a_3 = a_1 + v_2 * dt/2 ???
    // v_4 = v_n + a_3 * dt
    // a_4 = a_1 + v_3 * dt ???
    // v_n+1 = v_n + (a*_1 + 2*a*_2 + 2*a*_3 + a*_4) * dt/6
    // r_n+1 = r_n + (v*_1 + 2*v*_2 + 2*v*_3 + v*_4) * dt/6
}

void Integrator::integrate(double dt)
{
    Particle *p = nullptr;
    for (unsigned i = 0; i < Particle::flows.size(); ++i) {
#pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
            p = Particle::flows[i][j];
            if (! p->isStationary)
            {
                *p->r_former = *p->r;
                *p->v_former = *p->v;
                switch (Settings::INTEGRATION_SCHEME)
                {
                    // I order
                    case FORWARD_EULER          : forwardEuler(*p, dt);    break; // explicit method
                    case SYMPLECTIC_EULER       : symplecticEuler(*p, dt); break; // semi-iplicit
                    case BACKWARD_EULER         : backwardEuler(*p, dt);   break; // implicit
                    // II order
                    case MIDPOINT               : midPoint(*p, dt);        break;
                    case HALF_STEP              : halfStep(*p, dt);        break;
                    case VELOCITY_VERLET        : velocityVerlet(*p, dt);  break;
                    case LEAPFROG               : leapfrog(*p, dt);        break; // semi-implicit
                    // IV order
                    case IntegrationScheme::RK4 : rk4(*p, dt);             break;
                    default :
                        std::cout << "Switch failure at computeVectors()!" << std::endl << std::flush;
                        exit(EXIT_FAILURE);
                }
            }
        }
    }
}
