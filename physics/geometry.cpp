#include "physics/geometry.h"

#include "graphics/matrices.h"
#include "util/constants.h"
#include "util/settings.h"

#include <iostream>

namespace op
{
    double sgn(double x)
    {
        return ([](double x) -> double { return x < 0 ? -1 : 1; })(x);
    }
    int limit(int x, int lower, int upper)
    {
        return (x < lower) ? lower : ((x > upper) ? upper : x);
    }
}

void divideByW(QVector4D &v)
{
    for (int i = 0; i < 3; ++i)
    {
        v[i] /= v[3];
    }
}

double timePointPointClosest(
    const Vector &r1, const Vector &v1,
    const Vector &r2, const Vector &v2)
{
    Vector dv = v1 - v2;
    double dv2 = v1.dot(v2);

    if (dv2 < EPS) return 0.; //////////////////////////////////////////////////

    Vector w0 = r1 - r2;
    double cpa_time = - w0.dot(dv) / dv2;

    return cpa_time;
}

double distWhenPointPointClosest(
    const Vector &r1, const Vector &v1,
    const Vector &r2, const Vector &v2)
{
    double ctime = timePointPointClosest(r1, v1, r2, v2);
    Vector p1 = r1 + v1 * ctime;
    Vector p2 = r2 + v2 * ctime;
    return p1.dist(p2);
}

geometry::Ray normalized2DPointToRay(float normalizedX, float normalizedY)
{
    // We'll convert these normalized device coordinates into world-space coordinates.
    // We'll pick a source on the near and far planes, and draw a line between them.
    // To do this transform, we need to first multiply by the inverse matrix,
    //   and then we need to undo the perspective divide.
    QVector4D nearPointNdc(normalizedX, normalizedY, -1, 1);
    QVector4D farPointNdc(normalizedX, normalizedY, 1, 1);
    QVector4D nearPointWorld = Matrices::viewProjectionInverted * nearPointNdc;
    QVector4D farPointWorld = Matrices::viewProjectionInverted * farPointNdc;
    divideByW(nearPointWorld);
    divideByW(farPointWorld);

    Vector nearPointRay = Vector(nearPointWorld);
    Vector farPointRay = Vector(farPointWorld);
    Vector ray = farPointRay - nearPointRay;
    return geometry::Ray(nearPointRay, ray);
}

double pointRayDist(const Vector &point, const geometry::Ray &ray)
{
    Vector p1ToPoint = point - ray.source;
    Vector p2ToPoint = point - (ray.source + ray.vector);
    double areaOfTriangleTimesTwo = (p1ToPoint * p2ToPoint).norm();
    double lengthOfBase = ray.vector.norm();
    return areaOfTriangleTimesTwo / lengthOfBase;
}

double pointlineDist(const Vector &p, const geometry::Line &l)
{
    return p.to(pointLineProj(p, l)).norm();
}
Vector pointToLine(const Vector &p, const geometry::Line &l)
{
    return pointLineProj(p, l) - p;
}
Vector pointLineProj(const Vector &p, const geometry::Line &l)
{
    return l.p1 + l.p1.to(p).proj(l.p1.to(l.p2));
}
bool pointLiesFrontOfLine(const Vector &p, const geometry::Line &l)
{
    return (pointToLine(p, l).normalized() + l.normal()).abs()
        < Vector(EPS, EPS, EPS) ? true : false;
}
bool pointLiesOnSeg(const Vector &p, const geometry::Line &l)
{
    return fabs(l.p1.dist(l.p2) - (l.p1.dist(p) + l.p2.dist(p))) < EPS
        ? true : false;
}
bool pointProjectsToSeg(const Vector &p, const geometry::Line &l)
{
    Vector proj_p_l = pointLineProj(p, l);
    return pointLiesOnSeg(proj_p_l, l);
}
Vector pointSegClosestPoint(const Vector &p, const geometry::Line &l)
{
    Vector proj_p_l = pointLineProj(p, l);
    return pointLiesOnSeg(proj_p_l, l)
        ? proj_p_l : (proj_p_l.dist(l.p1) < proj_p_l.dist(l.p2) ? l.p1 : l.p2);
}
double lineLineDist(const geometry::Line &l1, const geometry::Line &l2)
{
    Vector u = l1.p2 - l1.p1;
    Vector v = l2.p2 - l2.p1;
    Vector w = l1.p1 - l2.p1;
    double a = u.dot(u); // always >= 0
    double b = u.dot(v);
    double c = v.dot(v); // always >= 0
    double d = u.dot(w);
    double e = v.dot(w);
    double D = a*c - b*b; // always >= 0
    double sc, tc;

    if (D < EPS) { // lines almost parallel
        sc = 0.;
        tc = (b>c ? d/b : e/c); // largest denominator
    }
    else {
        sc = (b*e - c*d) / D;
        tc = (a*e - b*d) / D;
    }

    // diff of two closest points
    Vector dP = w + u*sc - v*tc; // L1(sc) - L2(tc)
    return dP.norm();
}
bool lineLineIntersect(const geometry::Line &l1, const geometry::Line &l2)
{
    return lineLineDist(l1, l2) < EPS ? true : false;
}
double segSegDist(const geometry::Line &l1, const geometry::Line &l2)
{
    Vector u = l1.p2 - l1.p1;
    Vector v = l2.p2 - l2.p1;
    Vector w = l1.p1 - l2.p1;
    double a = u.dot(u); // always >= 0
    double b = u.dot(v);
    double c = v.dot(v); // always >= 0
    double d = u.dot(w);
    double e = v.dot(w);
    //std::cout << "a" << a << " b" << b << " c" << c << " d" << d << " e" << e << std::endl << std::flush;
    double D = a*c - b*b; // always >= 0
    double sc, sN, sD = D; // sc = sN/sD, default sD = D >= 0
    double tc, tN, tD = D; // tc = tN/tD, default tD = D >= 0

    // compute the line parameters of the two closest points
    if (D < EPS) { // the lines are almost parallel
        sN = 0.; // force using point P0 on segment S1
        sD = 1.; // to prevent possible division by 0.0 later
        tN = e;
        tD = c;
    }
    else { // get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        if (sN < 0.) { // sc < 0 => the s=0 edge is visible
            sN = 0.;
            tN = e;
            tD = c;
        }
        else if (sN > sD) { // sc > 1 => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;
        }
    }

    if (tN < 0.0) { // tc < 0 => the t=0 edge is visible
        tN = 0.0;
        // recompute sc for this edge
        if (-d < 0.0) {
            sN = 0.0;
        }
        else if (-d > a) {
            sN = sD;
        }
        else {
            sN = -d;
            sD = a;
        }
    }
    else if (tN > tD) { // tc > 1 => the t=1 edge is visible
        tN = tD;
        // recompute sc for this edge
        if (b - d < 0.) {
            sN = 0;
        }
        else if (b - d > a) {
            sN = sD;
        }
        else {
            sN = b - d;
            sD = a;
        }
    }
    // finally do the division to get sc and tc
    sc = (fabs(sN) < EPS ? 0. : sN / sD);
    tc = (fabs(tN) < EPS ? 0. : tN / tD);

    // get the difference of the two closest points
    Vector dP = w + u*sc - v*tc; // S1(sc) - S2(tc)

    //std::cout << dP.norm() << std::endl << std::flush;
    //std::cout << std::endl << std::flush;

    return dP.norm(); // return the closest distance
}
bool segSegIntersect(const geometry::Line &l1, const geometry::Line &l2)
{
    return segSegDist(l1, l2) == 0. ? true : false;
}

Vector rayPlaneIntersection(const geometry::Ray &ray, const geometry::Plane &plane)
{
    Vector rayToPlaneVector = plane.point - ray.source;
    double scaleFactor = rayToPlaneVector.dot(plane.normal) / ray.vector.dot(plane.normal);
    Vector intersectionPoint = ray.source + ray.vector * scaleFactor;
    return intersectionPoint;
}

bool sphereRayIntersect(const geometry::Sphere &sphere, const geometry::Ray &ray)
{
    return pointRayDist(sphere.center, ray) < sphere.radius;
}

bool pointLiesFrontOfPlane(const Vector &point, const geometry::Plane &plane)
{
    // plane equation: ax + by + cz + d = 0
    // where:
    // normal n = (a, b, c)
    // point on plane P = (x_0, y_0, z_0)
    // d = - (ax_0 + by_0 + cz_0)

    return plane.normal.x * point.x
        + plane.normal.y * point.y
        + plane.normal.z * point.z
        - plane.normal.x * plane.point.x
        - plane.normal.y * plane.point.y
        - plane.normal.z * plane.point.z >= 0;
}
