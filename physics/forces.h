#pragma once
//#ifndef FORCES_H
//#define FORCES_H

#include "object/particle.h"

#include <cmath>

class Forces {
public:
    static Vector gravityVector();

    static void universalGravitation(const Particle &p1, const Particle &p2);
    static void gravityEarth(const Particle &p);
    static void rhoGravityEarth(const Particle &p);
    static void Coulomb(const Particle &p1, const Particle &p2);
    static void Friction(const Particle &p);
    static void Hooke(
        const Particle &p1, const Particle &p2,
        double ks, double d, double kd);
};

//#endif // FORCES_H
