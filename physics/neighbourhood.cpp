#include "physics/neighbourhood.h"

#include "object/particle.h"
#include "physics/partitioning.h"
#include "physics/vector.h"
#include "util/enums.h"
#include "util/macros.h"
#include "util/settings.h"

#include <algorithm>
#include <iostream>
#include <vector>

namespace neighbourhood
{
    void updateNeighboursAll()
    {
        for (unsigned i = 0; i < Particle::flows.size(); ++i) {
            #pragma omp parallel for if(Settings::PARALLEL_OMP)
            for (LOOP_TYPE j = 0; j < Particle::flows[i].size(); ++j) {
                neighbourhood::updateNeighbours(Particle::flows[i][j]);
            }
        }
    }
    void updateNeighbours(Particle *p)
    {
   //     if (Settings::GRANULAR_OR_LIQUID || Settings::COLLIDE_NGHBRS_ONLY)
        {
        //    if (stationary)
        //        neighbours = new std::vector<Particle*>();
        //        return; ////////////////////////////////////////////////////////

            // naive // incomputable for large particle count
            if (Settings::NEIGHBOUR_CHOICE == ALL) {
                double neighbour_range = std::numeric_limits<double>::infinity();//2 * smoothing_length;

                int particle_count = static_cast<int>(p->getParentFlow().size());
                if (p->neighbours == nullptr)
                {
                    p->neighbours = new std::vector<Particle*>();
                }
                else
                {
                    p->neighbours->clear();
                }

                // TODO to recheck these logics and compare with newly developed CUDA negihbourship logics
                double last_min_distance = 0;
                for (int i = 0; i < particle_count; ++i)
                {
                    double min_distance = std::numeric_limits<double>::infinity();
                    int min_index = std::numeric_limits<int>::max();
                    for (int j = 0; j < particle_count; ++j)
                    {
                        if (p != p->getParentFlowParticle(j))
                        {
                            double j_distance
                                = p->r->dist(*p->getParentFlowParticle(j)->r);
                            if (j_distance < min_distance && j_distance <= neighbour_range)
                            {
                                if (j_distance > 0 && j_distance > last_min_distance)
                                {
                                    min_distance = j_distance;
                                    min_index = j;
                                }
                            }
                        }
                    }
                    if (min_index != std::numeric_limits<int>::max())
                    {
                        p->neighbours->push_back(p->getParentFlowParticle(min_index));
                    }
                    last_min_distance = min_distance;
                }
            }

            else if (Settings::NEIGHBOUR_CHOICE == GRID
                  || Settings::NEIGHBOUR_CHOICE == OCTREE)
            {
                Partitioning::net->updateNeighbours(p);
            }
        }
    }

    void unNeighbour(Particle *p)
    {
        // remove particle from other particles neighbour lists
        Particle *p2 = nullptr;
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE i = 0; i < Particle::flows[0].size(); ++i)
        {
            p2 = Particle::flows[0][i];
            std::vector<Particle*> *neighs = p2->neighbours;
            neighs->erase(std::remove(neighs->begin(), neighs->end(), p), neighs->end());
            //neighs->erase(std::find(neighs->begin(), neighs->end(), p));
        }
        // TODO remove from neighbour array // PS there is no neighbour array? only collide array
    }
};
