#include "physics/octree.h"

#include "object/particle.h"
#include "physics/computer.h"
#include "physics/vector.h"
#include "util/debugger.h"
#include "util/macros.h"
#include "util/settings.h"

#include <algorithm> // for std::max()

#include <omp.h>

Octree *Octree::root = nullptr;
int Octree::maxDepth;
int Octree::maxDepthX;
int Octree::maxDepthY;
int Octree::maxDepthZ;

Octree::Octree()
{
    root = &*this;
    cellCount += 1;
    maxDepth = static_cast<int>(floor(log2(std::max(std::max(cellCountX, cellCountY), cellCountZ))));
    maxDepthX = static_cast<int>(floor(log2(cellCountX)));
    maxDepthY = static_cast<int>(floor(log2(cellCountY)));
    maxDepthZ = static_cast<int>(floor(log2(cellCountZ)));
    parent = nullptr;
    center[0] = 0.;
    center[1] = 0.;
    center[2] = 0.;
    nodeDepth = 0;
    particles = vector<Particle*>();
    for (unsigned i = 0; i < 8; ++i) {
        corners[i] = nullptr;
    }
    for (int i = 0; i < 8; ++i) {
        corners[i] = new Octree(nodeDepth + 1, this, i, std::string("M"));
    }
    std::cout << "Octree maxDepth: "  << maxDepth  << std::endl;
    std::cout << "Octree maxDepthX: " << maxDepthX << std::endl;
    std::cout << "Octree maxDepthY: " << maxDepthY << std::endl;
    std::cout << "Octree maxDepthZ: " << maxDepthZ << std::endl;
    std::cout << "There are " << cellCount << " Octrees." << std::endl;
}

Octree::Octree(int depth, Octree *parent, int corner, std::string placement)
{
    cellCount += 1;
    this->parent = parent;
    center[0] = parent->center[0];
    center[1] = parent->center[1];
    center[2] = parent->center[2];
    nodeDepth = depth;
    for (unsigned i = 0; i < 8; ++i) {
        corners[i] = nullptr;
    }
    particles = vector<Particle*>();

    if (nodeDepth <= maxDepth)
    {
        placement.append("->");
        double cubeDiameter = Settings::ARENA_DIAMETER / pow(2, nodeDepth + 1);
        bool leafs[8] = {true, true, true, true, true, true, true, true};
        if (nodeDepth <= maxDepthX) {
            if (corner < 4) {
                center[0] -= cubeDiameter; // L
                placement.append("L");
            }
            else {
                center[0] += cubeDiameter; // R
                placement.append("R");
            }
        }
        else {
            leafs[4] = leafs[5] = leafs[6] = leafs[7] = false;
        }
        if (nodeDepth <= maxDepthY) {
            if (corner % 4 < 2) {
                center[1] -= cubeDiameter; // D
                placement.append("D");
            }
            else {
                center[1] += cubeDiameter; // U
                placement.append("U");
            }
        }
        else {
            leafs[2] = leafs[3] = leafs[6] = leafs[7] = false;
        }
        if (nodeDepth <= maxDepthZ) {
            if (corner % 2 < 1) {
                center[2] -= cubeDiameter; // B
                placement.append("B");
            }
            else {
                center[2] += cubeDiameter; // F
                placement.append("F");
            }
        }
        else {
            leafs[1] = leafs[3] = leafs[5] = leafs[7] = false;
        }

        for (int i = 0; i < 8; ++i)
            if (leafs[i])
                corners[i] = new Octree(nodeDepth + 1, this, i, placement);

        std::cout << placement << ": " << center[0] << " " << center[1] << " " << center[2] << std::endl;
    }
}

void Octree::distributeParticles()
{
    root->clear();
    Computer::forEachParticleParallel([](Particle *p) -> void { root->push(*p); });
}

void Octree::clear()
{
    particles.clear();
    if (nodeDepth < maxDepth)
        for (unsigned i = 0; i < 8; ++i)
            if (corners[i] != nullptr)
                corners[i]->clear();
}

vector<Particle*>& Octree::getCell(int, int, int)
{
    Debugger::throwWithMessage("Octree.getCell(int, int, int) is not implemented");
//    return std::vector<Particle*>();
}

vector<Particle*>& Octree::getCell(Particle *p)
{
    Octree &node = *root;
    while (true)
    {
        unsigned corner = 0;
        if (p->r->x <= node.center[0]) corner  = 0; // L
        else                           corner  = 4; // R
        if (p->r->y <= node.center[1]) corner += 0; // D
        else                           corner += 2; // U
        if (p->r->z <= node.center[2]) corner += 0; // B
        else                           corner += 1; // F

        if (node.corners[corner] != nullptr) {
            node = *node.corners[corner];
        }
        else {
            break; /////////////////////////////////////////////////////////////
        }
    }
    return node.particles;
}

void Octree::push(Particle &p)
{
    Octree &node = *root;
    while (true)
    {
        unsigned corner = 0;
        if (p.r->x <= node.center[0]) corner  = 0; // L
        else                          corner  = 4; // R
        if (p.r->y <= node.center[1]) corner += 0; // D
        else                          corner += 2; // U
        if (p.r->z <= node.center[2]) corner += 0; // B
        else                          corner += 1; // F
        if (node.corners[corner] != nullptr) {
            node = *node.corners[corner];
        }
        else {
            //p->cube = node;

            #pragma omp critical
            node.particles.push_back(&p);
            break; /////////////////////////////////////////////////////////////
        }
    }
}

vector<Octree*> Octree::getLeaves()
{
    vector<Octree*> leaves = vector<Octree*>();
    root->getLeaves(leaves);
    std::cout << leaves.size() << std::endl;
    return leaves;
}
void Octree::getLeaves(vector<Octree*> &leaves)
{
    if (nodeDepth <= maxDepth)
    {
        if (std::all_of(std::begin(corners), std::end(corners), [](Octree*){ return nullptr; }))
        {
            leaves.push_back(&*this);
        }
        else
        {
            for (unsigned i = 0; i < 8; ++i)
            {
                if (corners[i] != nullptr)
                {
                    corners[i]->getLeaves(leaves);
                }
            }
        }
    }
}

vector<Octree*> Octree::getNodes()
{
    vector<Octree*> nodes = vector<Octree*>();
    root->getNodes(nodes);
    return nodes;
}
void Octree::getNodes(vector<Octree*> &nodes)
{
    nodes.push_back(&*this);
    if (nodeDepth < maxDepth)
        for (unsigned i = 0; i < 8; ++i)
            if (corners[i] != nullptr)
                corners[i]->getNodes(nodes);
}

void Octree::getVolume()
{
    std::cout << "Octree volume measurement not supported" << std::endl;
}

void Octree::updateNeighbours(Particle *p)
{
    Partitioning::updateNeighbours(p);

    for (Particle *n : particles)
        if (n != p && p->r->dist(*n->r) <= Settings::NEIGHBOUR_RANGE)
            p->neighbours->push_back(n);

    if (parent != nullptr)
        for (unsigned i = 0; i < 8; ++i)
            if (parent->corners[i] != nullptr && parent->corners[i] != this)
                for (Particle *n : parent->corners[i]->particles)
                    if (n != p && p->r->dist(*n->r) <= Settings::NEIGHBOUR_RANGE)
                        p->neighbours->push_back(n);
}
