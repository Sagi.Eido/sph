#pragma once
//#ifndef INTEGRATOR_H
//#define INTEGRATOR_H

class Particle;

class Integrator
{
public:
    static void forwardEuler(const Particle &p, double dt);
    static void symplecticEuler(const Particle &p, double dt);
    static void backwardEuler(const Particle &p, double dt);
    static void midPoint(const Particle &p, double dt);
    static void halfStep(const Particle &p, double dt);
    static void velocityVerlet(const Particle &p, double dt);
    static void leapfrog(const Particle &p, double dt);
    static void rk4(const Particle &p, double dt);

    static void integrate(double secondsLater);
};

//#endif // INTEGRATOR_H
