#pragma once
//#ifndef VECTOR_H
//#define VECTOR_H

#include "graphics/paintable.h"
#include "util/constants.h"

#include <cmath>
#include <ostream>

class QVector4D;

class Vector : public Paintable {
public:
    double x, y, z;

    Vector(double x, double y, double z);
    Vector(const Vector &v); // copy constructor
    //Vector(Vector&&); // move constructor
    Vector();
    Vector(double v[3]);
    Vector(QVector4D &v);

    friend std::ostream& operator<<(std::ostream &out, const Vector &v)
    {
        return out << v.x << " " << v.y << " " << v.z;
    }

    virtual bool operator==(const Vector &v) const;
    inline virtual bool operator!=(const Vector &v) const {
        return ! (*this == v);
    }
    inline virtual Vector operator-() const {
        return Vector(-x, -y, -z);
    }
    inline virtual bool operator<(const Vector &v) const {
        return x >= v.x || y >= v.y || z >= v.z ? false : true;
    }
    inline virtual Vector operator+(const Vector &v) const {
        return Vector(x+v.x, y+v.y, z+v.z);
    }
    inline virtual Vector operator-(const Vector &v) const {
        return Vector(x-v.x, y-v.y, z-v.z);
    }
    virtual Vector operator*(const Vector &v) const; // cross
    //virtual double operator/(const Vector& v) const;
    inline virtual Vector operator+(double v) const {
        return Vector(x+v, y+v, z+v);
    }
    inline virtual Vector operator-(double v) const {
        return Vector(x-v, y-v, z-v);
    }
    inline virtual Vector operator*(double v) const {
        return Vector(x*v, y*v, z*v);
    }
    inline virtual Vector operator/(double v) const {
        return Vector(x/v, y/v, z/v);
    }

    virtual Vector& operator+=(const Vector &v);
    virtual Vector& operator-=(const Vector &v);
    virtual Vector& operator*=(const Vector &v);
    virtual Vector& operator+=(double v);
    virtual Vector& operator-=(double v);
    virtual Vector& operator*=(double v);
    virtual Vector& operator/=(double v);

    void zero();
    void assertLimits(double x_max, double y_max, double z_max, std::string msg);
    void limit(double x_max, double y_max, double z_max);
    void limit(double max);
    void cut(double max);

    void setModelMatrix() override;
    void paint() override;
    void createView() override;

    inline bool isZero() const {
        return (x != 0. || y != 0. || z != 0.) ? false : true;
    }
    inline double cosxy(const Vector &v) const {
        return this->dot(v) / (this->norm() * v.norm());
    }
    inline double dist(const Vector &v) const {
        return (*this - v).norm(); // TODO check!!!!! what is *this in norm when called here!!!???
    }
    inline double dot(const Vector &v) const {
        return this->x*v.x + this->y*v.y + this->z*v.z;
    }
    inline double norm() const {
        return sqrt(this->dot(*this));
    }
    inline Vector normalized() const {
        return this->isZero() ? Vector() : *this / this->norm();
    }
    inline Vector perp2d() const {
        return Vector(- this->y, this->x, this->z);
    }
    inline Vector to(const Vector &v) const {
        return v - *this;
    }
    inline double angle(const Vector &v) const {
        return acos(this->cos(v));
    }
    inline double cos(const Vector &v) const {
        return this->dot(v) / (this->norm() * v.norm());
    }
    // scalarProj_a(b) = |a|cos(a, b)
    // scalarProj_a(b) = |a|a.b / (|a||b|) = a.b / |b|
    inline double scalarProj(const Vector &v) const {
        return this->norm() * this->cos(v);
    }
    // proj_a(b) = b/|b| * scalar_proj(a, b)
    // proj_a(b) = b/|b| * a.b/|b|
    inline Vector proj(const Vector &v) const {
        return v.normalized() * this->scalarProj(v);
    }
    inline Vector abs() const
    {
        return Vector(fabs(x), fabs(y), fabs(z));
    }
    inline Vector correct() const {
        return Vector(
            fabs(x) < EPS ? 0. : x,
            fabs(y) < EPS ? 0. : y,
            fabs(z) < EPS ? 0. : z
        );
    }
};

//#endif // VECTOR_H
