//#include "physics/cuda.cuh"

#include "object/particle.h"
//#include "physics/forces.h"
#include "physics/vector.h"
#include "util/settings.h"

#include <vector>
//#ifdef COMPILER_MSVC
#include <algorithm>
//#endif

#include <omp.h>

#include <cuda.h>
#include <cuda_runtime.h> // checkCudaErrors(), cudaMalloc(), cudaMemcpy(), ...
#include <helper_cuda.h> // checkCudaErrors() // helper_cuda.h must go after cuda_runtime.h
#include <builtin_types.h> // __global__ __host__ __device__

#define UNIFIED_MATH_CUDA_H
#include <vector_functions.h>
#include <math_functions.h>

//#include <thrust/device_vector.h>
//#include <thrust/host_vector.h>
//#include <thrust/system/cuda/experimental/pinned_allocator.h>

using namespace std;

#include "util/macros.h"

////////////////////////////////////////////////////////////////////////////////
//// Interface for outside world. //////////////////////////////////////////////
//// Kernel (= global = host + device) functions. //////////////////////////////
////////////////////////////////////////////////////////////////////////////////

__forceinline __device__ double3 operator+(const double3 &v1, const double3 &v2) {
    return make_double3(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);
}

__forceinline __device__ double3 operator-(const double3 &v1, const double3 &v2) {
    return make_double3(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}

__forceinline __device__ double3 operator*(const double3 &v, const double &n) {
    return make_double3(v.x*n, v.y*n, v.z*n);
}

__forceinline __device__ double3 operator/(const double3 &v, const double &n) {
    return make_double3(v.x/n, v.y/n, v.z/n);
}

__forceinline __device__ double dot(const double3 &v1, const double3 &v2) {
    return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

__forceinline __device__ double norm(const double3 &v) {
    //return sqrt(dot(v, v));
    return norm3d(v.x, v.y, v.z);
}

__forceinline __device__ double distance(const double3 &v1, const double3 &v2) {
    double3 v3 = v1 - v2;
    return norm(v3);
}

__forceinline __device__ double3 normal(const double3 &v) {
    return v / norm(v);
}

////////////////////////////////////////////////////////////////////////////////
//// Common ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void GPUFrees()
{
    // pinned host memory
    checkCudaErrors(cudaFreeHost(Particle::rx_host));
    checkCudaErrors(cudaFreeHost(Particle::ry_host));
    checkCudaErrors(cudaFreeHost(Particle::rz_host));
    checkCudaErrors(cudaFreeHost(Particle::vx_host));
    checkCudaErrors(cudaFreeHost(Particle::vy_host));
    checkCudaErrors(cudaFreeHost(Particle::vz_host));
    checkCudaErrors(cudaFreeHost(Particle::m_host));
    checkCudaErrors(cudaFreeHost(Particle::is_stationary_host));

    checkCudaErrors(cudaFree(Particle::rx_device));
    checkCudaErrors(cudaFree(Particle::ry_device));
    checkCudaErrors(cudaFree(Particle::rz_device));
    checkCudaErrors(cudaFree(Particle::vx_device));
    checkCudaErrors(cudaFree(Particle::vy_device));
    checkCudaErrors(cudaFree(Particle::vz_device));
    checkCudaErrors(cudaFree(Particle::m_device));
    checkCudaErrors(cudaFree(Particle::is_stationary_device));

    checkCudaErrors(cudaFreeHost(Particle::neighbours_host));
    checkCudaErrors(cudaFree(Particle::neighbours_device));
}

void GPUMallocs()
{
    static bool wasExecuted = false;
    if (! wasExecuted) {
        wasExecuted = true;

        CUDA_TIME_MEASUREMENT_INIT

        CUDA_TIME_MEASUREMENT_START
        {
            // pinned host memory
            checkCudaErrors(cudaMallocHost((void**)&Particle::rx_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::ry_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::rz_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::vx_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::vy_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::vz_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::m_host, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMallocHost((void**)&Particle::is_stationary_host, Settings::PARTICLE_COUNT * sizeof(bool)));
        }
        CUDA_TIME_MEASUREMENT_STOP
        bytes = Settings::PARTICLE_COUNT * (7 * sizeof(double) + 1 * sizeof(bool));
        printf("Allocating memory for the host: %fGB / %fs = %fGB/s\n",
               bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
        fflush(stdout);

        CUDA_TIME_MEASUREMENT_START
        {
            checkCudaErrors(cudaMalloc((void**)&Particle::rx_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::ry_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::rz_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::vx_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::vy_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::vz_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::m_device, Settings::PARTICLE_COUNT * sizeof(double)));
            checkCudaErrors(cudaMalloc((void**)&Particle::is_stationary_device, Settings::PARTICLE_COUNT * sizeof(bool)));
        }
        CUDA_TIME_MEASUREMENT_STOP
        bytes = Settings::PARTICLE_COUNT * (7 * sizeof(double) + 1 * sizeof(bool));
        printf("Allocating memory for the device: %fGB / %fs = %fGB/s\n",
               bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
        fflush(stdout);

        CUDA_TIME_MEASUREMENT_START
        {
            checkCudaErrors(cudaMallocHost((void**)&Particle::neighbours_host, Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool)));
        }
        CUDA_TIME_MEASUREMENT_STOP
        bytes = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool);
        printf("Allocating memory for the host: %fGB / %fs = %fGB/s\n",
               bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
        fflush(stdout);

        CUDA_TIME_MEASUREMENT_START
        {
            checkCudaErrors(cudaMalloc((void**)&Particle::neighbours_device, Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool)));
        }
        CUDA_TIME_MEASUREMENT_STOP
        bytes = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool);
        printf("Allocating memory for the device: %fGB / %fs = %fGB/s\n",
               bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
        fflush(stdout);

        CUDA_TIME_MEASUREMENT_FIN
    }
}

////////////////////////////////////////////////////////////////////////////////
//// Collisions ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

__forceinline __device__ void collideKernelImplementation(
    double &rx1, double &ry1, double &rz1,
    double &vx1, double &vy1, double &vz1,
    double &rx2, double &ry2, double &rz2,
    double &vx2, double &vy2, double &vz2,
    double &m1, double &m2,
    double &radius,
    bool &is_stationary_p1, bool &is_stationary_p2,
    double &waterDampening)
{
    double3 p1_r = make_double3(rx1, ry1, rz1);
    double3 p2_r = make_double3(rx2, ry2, rz2);
    double3 p1_v = make_double3(vx1, vy1, vz1);
    double3 p2_v = make_double3(vx2, vy2, vz2);
    double3 sphereNormal = normal(p1_r - p2_r);
    double distanceBorder = distance(p1_r, p2_r) - radius - radius;
    if (distanceBorder < 0.)
    {
        double3 p2_r_old = make_double3(p2_r.x, p2_r.y, p2_r.z);
        if (! is_stationary_p2)
        {
            p2_r = p2_r + sphereNormal * distanceBorder;
        }

        double3 p2_v_old = make_double3(p2_v.x, p2_v.y, p2_v.z);
        if (! is_stationary_p2)
        {
            p2_v = p2_v
                   - (p2_r - p1_r)
                     * dot(p2_v - p1_v, p2_r - p1_r)
                     / pow(distance(p2_r, p1_r), 2)
                     * 2 * m1 / (m1 + m2);
            p2_v = p2_v * (1. - waterDampening);
        }
        if (! is_stationary_p1)
        {
            p1_v = p1_v
                   - (p1_r - p2_r_old)
                      * dot(p1_v - p2_v_old, p1_r - p2_r_old)
                      / pow(distance(p1_r, p2_r_old), 2)
                      * 2 * m2 / (m1 + m2);
            p1_v = p1_v * (1. - waterDampening);
        }
    }

//    __syncthreads();
    rx1 = p1_r.x; ry1 = p1_r.y; rz1 = p1_r.z;
    vx1 = p1_v.x; vy1 = p1_v.y; vz1 = p1_v.z;
    rx2 = p2_r.x; ry2 = p2_r.y; rz2 = p2_r.z;
    vx2 = p2_v.x; vy2 = p2_v.y; vz2 = p2_v.z;
//    __syncthreads();
}

__global__ void collideKernel(
    int i, int x, int y, int z,
    double *rx, double *ry, double *rz,
    double *vx, double *vy, double *vz,
    double *m,
    bool *stationary,
    int particleCount,
    double radius,
    double waterDampening)
{
    unsigned j = blockIdx.x * y + threadIdx.x * z;
    if (j > i)
        collideKernelImplementation(
            rx[i], ry[i], rz[i], vx[i], vy[i], vz[i],
            rx[j], ry[j], rz[j], vx[j], vy[j], vz[j],
            m[i], m[j], radius,
            stationary[i], stationary[j],
            waterDampening);
}

void GPUCollide()
{    
    GPUMallocs();

    CUDA_TIME_MEASUREMENT_INIT

    CUDA_TIME_MEASUREMENT_START
    {
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE i = 0; i < (LOOP_TYPE) Settings::PARTICLE_COUNT; ++i) {
            Particle::rx_host[i]            = Particle::flows[0][i]->r->x;
            Particle::ry_host[i]            = Particle::flows[0][i]->r->y;
            Particle::rz_host[i]            = Particle::flows[0][i]->r->z;
            Particle::vx_host[i]            = Particle::flows[0][i]->v->x;
            Particle::vy_host[i]            = Particle::flows[0][i]->v->y;
            Particle::vz_host[i]            = Particle::flows[0][i]->v->z;
            Particle::m_host[i]             = Particle::flows[0][i]->m;
            Particle::is_stationary_host[i] = Particle::flows[0][i]->isStationary;
        }
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * (7 * sizeof(double) + 1 * sizeof(bool));
    printf("Host Vector to Host Vector bandwidth: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        checkCudaErrors(cudaMemcpy(Particle::rx_device, Particle::rx_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::ry_device, Particle::ry_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::rz_device, Particle::rz_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::vx_device, Particle::vx_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::vy_device, Particle::vy_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::vz_device, Particle::vz_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::m_device, Particle::m_host, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(Particle::is_stationary_device, Particle::is_stationary_host, Settings::PARTICLE_COUNT * sizeof(bool), cudaMemcpyHostToDevice));
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * (7 * sizeof(double) + 1 * sizeof(bool));
    printf("Host to CUDA Device bandwidth: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        //    cuDeviceGet...(
        //    1152 cores
        //    1024 threads per block
        //    Compute Capability         6.1
        //    Processor Count            10
        //    Cores per Processor        128
        //    Threads per Multiprocessor 2048
        //    Warp Size                  32 Threads
        //    Block has at least one warp, with at least 32 threads.

        for (LOOP_TYPE i = 0; i < Settings::PARTICLE_COUNT; ++i) {
            dim3 blocksPerGrid(Settings::X_PARTICLE_COUNT); // ? x ? x ?
            dim3 threadsPerBlock(Settings::Y_PARTICLE_COUNT * Settings::Z_PARTICLE_COUNT); // ? x ? x ?
            collideKernel<<<blocksPerGrid, threadsPerBlock>>>(
                i,
                Settings::X_PARTICLE_COUNT,
                Settings::Y_PARTICLE_COUNT,
                Settings::Z_PARTICLE_COUNT,
                Particle::rx_device, Particle::ry_device, Particle::rz_device,
                Particle::vx_device, Particle::vy_device, Particle::vz_device,
                Particle::m_device,
                Particle::is_stationary_device,
                Settings::PARTICLE_COUNT,
                Settings::PARTICLE_RADIUS,
                Settings::FORCE_DAMPING_WATER
            );
        }
        checkCudaErrors(cudaDeviceSynchronize());
    }
    CUDA_TIME_MEASUREMENT_STOP
    printf("CUDA Collision Kernel execution time: %fs\n", time * 1e-3);
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        // pinned host memory
        checkCudaErrors(cudaMemcpy(&Particle::rx_host[0], Particle::rx_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
        checkCudaErrors(cudaMemcpy(&Particle::ry_host[0], Particle::ry_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
        checkCudaErrors(cudaMemcpy(&Particle::rz_host[0], Particle::rz_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
        checkCudaErrors(cudaMemcpy(&Particle::vx_host[0], Particle::vx_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
        checkCudaErrors(cudaMemcpy(&Particle::vy_host[0], Particle::vy_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
        checkCudaErrors(cudaMemcpy(&Particle::vz_host[0], Particle::vz_device, Settings::PARTICLE_COUNT * sizeof(double), cudaMemcpyDeviceToHost));
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * (6 * sizeof(double));
    printf("CUDA Device to Host bandwidth: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE i = 0; i < (LOOP_TYPE) Settings::PARTICLE_COUNT; ++i) {
            Particle::flows[0][i]->r->x = Particle::rx_host[i];
            Particle::flows[0][i]->r->y = Particle::ry_host[i];
            Particle::flows[0][i]->r->z = Particle::rz_host[i];
            Particle::flows[0][i]->v->x = Particle::vx_host[i];
            Particle::flows[0][i]->v->y = Particle::vy_host[i];
            Particle::flows[0][i]->v->z = Particle::vz_host[i];
        }
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * (6 * sizeof(double));
    printf("Host Vector to Host Vector bandwidth: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_FIN
}

////////////////////////////////////////////////////////////////////////////////
//// Meighbourship /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

__forceinline __device__ void updateNeighboursKernelImplementation(
    bool &neighbours_ij,
    bool &neighbours_ji,
    double &neighbourRange,
    double &r1x, double &r1y, double &r1z,
    double &r2x, double &r2y, double &r2z)
{
    double3 r1 = make_double3(r1x, r1y, r1z);
    double3 r2 = make_double3(r2x, r2y, r2z);
    double distanceCenter = distance(r1, r2);
    bool areNeighbours = (distanceCenter <= neighbourRange && distanceCenter > 0);
    neighbours_ij = areNeighbours;
    neighbours_ji = areNeighbours;
}

__global__ void updateNeighboursKernel(
    int i, int x, int y, int z,
    bool *neighbours,
    double neighbourRange,
    double *rx, double *ry, double *rz,
    int particleCount)
{
    int j = blockIdx.x * y + threadIdx.x * z;
    if (j > i)
    {
        updateNeighboursKernelImplementation(
            neighbours[i *particleCount + j],
            neighbours[j *particleCount + i],
            neighbourRange,
            rx[i], ry[i], rz[i],
            rx[j], ry[j], rz[j]);
    }
}

void updateNeighboursKernelCall(int i)
{
    dim3 blocksPerGrid(Settings::X_PARTICLE_COUNT);
    dim3 threadsPerBlock(Settings::Y_PARTICLE_COUNT * Settings::Z_PARTICLE_COUNT); // ? x ? x ?
    double neighbourRange = Settings::NEIGHBOUR_RANGE;
    updateNeighboursKernel<<<blocksPerGrid, threadsPerBlock>>>(
        i,
        Settings::X_PARTICLE_COUNT,
        Settings::Y_PARTICLE_COUNT,
        Settings::Z_PARTICLE_COUNT,
        Particle::neighbours_device,
        neighbourRange,
        Particle::rx_device, Particle::ry_device, Particle::rz_device,
        Settings::PARTICLE_COUNT);
}

void GPUUpdateNeighboursImplementation(int i)
{
    GPUMallocs();

    CUDA_TIME_MEASUREMENT_INIT

    CUDA_TIME_MEASUREMENT_START
    {
        checkCudaErrors(cudaMemcpy(Particle::neighbours_device, Particle::neighbours_host, Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool), cudaMemcpyHostToDevice));
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool);
    printf("Transferring from host to device: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        if (i != -1)
        {
            updateNeighboursKernelCall(i);
        }
        else for (LOOP_TYPE j = 0; j < Settings::PARTICLE_COUNT - 1; ++j)
        {
            updateNeighboursKernelCall(j);
        }
        checkCudaErrors(cudaDeviceSynchronize());
    }
    CUDA_TIME_MEASUREMENT_STOP
    printf("CUDA Neighbourship Kernel execution time: %fs\n", time * 1e-3);
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_START
    {
        checkCudaErrors(cudaMemcpy(&Particle::neighbours_host[0], Particle::neighbours_device, Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool), cudaMemcpyDeviceToHost));
    }
    CUDA_TIME_MEASUREMENT_STOP
    bytes = Settings::PARTICLE_COUNT * Settings::PARTICLE_COUNT * sizeof(bool);
    printf("Transferring from device to host: %fGB / %fs = %fGB/s\n",
           bytes * 1e-9, time * 1e-3, (bytes * 1e-9) / (time * 1e-3));
    fflush(stdout);

    CUDA_TIME_MEASUREMENT_FIN
}

void GPUUpdateNeighbours()
{
    GPUUpdateNeighboursImplementation(-1);
}

void GPUUpdateNeighbour(int i)
{
    GPUUpdateNeighboursImplementation(i);
}

////////////////////////////////////////////////////////////////////////////////
//// Pressure //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

__forceinline __device__ void pressureKernelImplementation()
{

}

__global__ void pressureKernel()
{

}

void pressureKernelCall()
{
    dim3 blocksPerGrid(Settings::X_PARTICLE_COUNT);
    dim3 threadsPerBlock(Settings::Y_PARTICLE_COUNT * Settings::Z_PARTICLE_COUNT); // ? x ? x ?
    double neighbourRange = Settings::NEIGHBOUR_RANGE;
    pressureKernel<<<blocksPerGrid, threadsPerBlock>>>();
}

void GPUComputePressure()
{
    GPUMallocs();

    CUDA_TIME_MEASUREMENT_INIT

    CUDA_TIME_MEASUREMENT_START
    {
        pressureKernelCall();
        checkCudaErrors(cudaDeviceSynchronize());
    }
    CUDA_TIME_MEASUREMENT_STOP

    CUDA_TIME_MEASUREMENT_FIN
}

////////////////////////////////////////////////////////////////////////////////
//// Viscosity /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

__forceinline __device__ void viscosityKernelImplementation()
{

}

__global__ void viscosityKernel()
{

}

void viscosityKernelCall()
{
    dim3 blocksPerGrid(Settings::X_PARTICLE_COUNT);
    dim3 threadsPerBlock(Settings::Y_PARTICLE_COUNT * Settings::Z_PARTICLE_COUNT); // ? x ? x ?
    double neighbourRange = Settings::NEIGHBOUR_RANGE;
    viscosityKernel<<<blocksPerGrid, threadsPerBlock>>>();
}

void GPUComputeViscosity()
{
    GPUMallocs();

    CUDA_TIME_MEASUREMENT_INIT

    CUDA_TIME_MEASUREMENT_START
    {
        viscosityKernelCall();
        checkCudaErrors(cudaDeviceSynchronize());
    }
    CUDA_TIME_MEASUREMENT_STOP

    CUDA_TIME_MEASUREMENT_FIN
}
