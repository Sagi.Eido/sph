#include "physics/vector.h"

#include "graphics/form.h"
#include "graphics/matrices.h"
#include "physics/geometry.h"
#include "shape/arrow.h"
#include "util/debugger.h"
#include "util/settings.h"

#include <iostream>

using namespace std;

Vector::Vector(double x, double y, double z)
    : x(x), y(y), z(z)
{
#ifndef TEST_BUILD
    linkView(ARROW);
#endif
}
Vector::Vector(const Vector &v) // copy constructor
    : Vector::Vector(v.x, v.y, v.z) {} // delegating constructor
//Vector::Vector(Vector&&) {}
Vector::Vector()
    : Vector::Vector(0, 0, 0) {}
Vector::Vector(double v[3])
    : Vector::Vector(v[0], v[1], v[2]) {}
Vector::Vector(QVector4D &v)
{
    x = static_cast<double>(v[0]);
    y = static_cast<double>(v[1]);
    z = static_cast<double>(v[2]);
}

bool Vector::operator==(const Vector &v) const {
    //return (x != v.x || y != v.y || z != v.z) ? false : true;
    return (fabs(x - v.x) < EPS || fabs(y - v.y) < EPS || fabs(z - v.z) < EPS)
            ? false : true;
}
Vector Vector::operator*(const Vector &v) const { // cross
    //return Vector(x*v.x, y*v.y, z*v.z);
    return Vector(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
}
Vector& Vector::operator+=(const Vector &v) {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}
Vector& Vector::operator-=(const Vector &v) {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}
Vector& Vector::operator*=(const Vector &v) {
    *this = Vector(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
    return *this;
}
Vector& Vector::operator+=(double v) {
    x += v;
    y += v;
    z += v;
    return *this;
}

Vector& Vector::operator-=(double v) {
    x -= v;
    y -= v;
    z -= v;
    return *this;
}
Vector& Vector::operator*=(double v) {
    x *= v;
    y *= v;
    z *= v;
    return *this;
}
Vector& Vector::operator/=(double v) {
    x /= v;
    y /= v;
    z /= v;
    return *this;
}

void Vector::zero() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
}
void Vector::assertLimits(
    double x_max, double y_max, double z_max, std::string msg)
{
    if (fabs(x) > x_max || fabs(y) > y_max || fabs(z) > z_max)
    {
        Debugger::throwWithMessage(
            (std::string("Assertion failure at: ") + msg).c_str());
    }
}
void Vector::limit(double x_max, double y_max, double z_max)
{
    if (fabs(x) > x_max) x = op::sgn(x) * x_max;
    if (fabs(y) > y_max) y = op::sgn(y) * y_max;
    if (fabs(z) > z_max) z = op::sgn(z) * z_max;
}
void Vector::limit(double max)
{
    Vector::limit(max, max, max);
}
void Vector::cut(double max) {
    if (this->norm() > max)
    {
        *this = this->normalized() * max;
    }
}

void Vector::setModelMatrix()
{
    double us = norm() * 0.2 * Settings::VECTOR_LEN_MULT;
    Matrices::modelMatrix.scale(QVector3D(static_cast<float>(us),
                                          static_cast<float>(us),
                                          static_cast<float>(us)));
}
void Vector::paint()
{
    if (Settings::PAINT_VECTORS) {
        setModelMatrix();
        Paintable::paint();
    }
}
void Vector::createView()
{
    Arrow arrow;
    this->currentForm = arrow.form;
}
