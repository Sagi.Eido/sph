#include "physics/grid.h"

#include "object/particle.h"
#include "physics/computer.h"
#include "physics/geometry.h"
#include "physics/vector.h"
#include "util/macros.h"
#include "util/settings.h"

#include <omp.h>

#include <iostream>

Grid::Grid()
{
    grid = vector<vector<Particle*>>(
        static_cast<size_t>(cellCountX * cellCountY * cellCountZ),
        vector<Particle*>());
}

void Grid::distributeParticles()
{
//    #pragma omp parallel for \
//                if(Settings::PARALLEL_OMP)// \
//                //collapse(3) // TODO fails in debug build, probably because of static "grid"
//    for (LOOP_TYPE i = 0; i < grid.size(); ++i)
//        for (LOOP_TYPE j = 0; j < grid[i].size(); ++j)
//            for (LOOP_TYPE k = 0; k < grid[i][j].size(); ++k)
//                grid[i][j][k].clear();
    #pragma omp parallel for if(Settings::PARALLEL_OMP)
    for (LOOP_TYPE i = 0; i < grid.size(); ++i)
        grid[i].clear();

    Computer::forEachParticleNonParallel([this](Particle *p) -> void { push(*p); });
}

vector<Particle*>& Grid::getCell(Particle *p)
{
    std::vector<int> ijk = getCellCoordinates(p);
    return getCell(ijk[0], ijk[1], ijk[2]);
}
vector<Particle*>& Grid::getCell(int i, int j, int k)
{
    return grid[i*cellCountY*cellCountZ + j*cellCountZ + k];
}

void Grid::push(Particle &p) {
    std::vector<int> ijk = getCellCoordinates(&p);
    p.cell[0] = ijk[0];
    p.cell[1] = ijk[1];
    p.cell[2] = ijk[2];
//#pragma omp critical
    grid[ijk[0]*cellCountY*cellCountZ + ijk[1]*cellCountZ + ijk[2]].push_back(&p);
}

void Grid::getVolume()
{
    int volume = 0;

    #ifdef COMPILER_GPP
    #pragma omp parallel for \
                if(Settings::PARALLEL_OMP) \
                collapse(3) \
                reduction(+:volume)
    for (LOOP_TYPE i = 0; i < Net::cellCountX; ++i)
        for (LOOP_TYPE j = 0; j < Net::cellCountY; ++j)
            for (LOOP_TYPE k = 0; k < Net::cellCountZ; ++k)
                if (! Partitioning::net->getCell(i, j, k).empty())
                    ++volume;
    #elif COMPILER_MSVC
    #pragma omp parallel for if(Settings::PARALLEL_OMP)
    for (LOOP_TYPE n = 0;
         n < static_cast<LOOP_TYPE>(Partitioning::cellCountX * Partitioning::cellCountY * Partitioning::cellCountZ);
         ++n)
    {
        int i =  n / (Partitioning::cellCountY * Partitioning::cellCountZ);
        int j = (n % (Partitioning::cellCountY * Partitioning::cellCountZ)) / Partitioning::cellCountZ;
        int k = (n % (Partitioning::cellCountY * Partitioning::cellCountZ)) % Partitioning::cellCountZ;
        if (! Partitioning::net->getCell(i, j, k).empty()) {
            #pragma omp atomic
            ++volume;
        }
    }
    #endif

    std::cout << volume << "cbs <- Volume." << std::endl << std::flush;
    // TODO
//    std::cout << volume << "cbs <- Volume. ("
//              << volume / (0.7584/0.907 * Settings::PARTICLE_COUNT_2D)
//              << "x_hex_pack)"
//              << std::endl << std::flush;
}

void Grid::updateNeighbours(Particle *p)
{
    Partitioning::updateNeighbours(p);

    // TODO
    // shouldn't all particles be checked, not all grid cells?
    // if cells are checked first, then the more there are cells
    // (the bigger the arena is), the more checks there are
    // (of mostly empty cells what's more),
    // but if particles were checked first, to find in which
    // cell they are, and then find neighbhours among
    // particles of this cell, than it would be much fast
    // (? having brought no disadvantages at the same time ?)
    int nlc = Settings::NEIGHBOUR_RANGE_CELLS;
    Particle *n = nullptr;
#pragma omp parallel for if(Settings::PARALLEL_OMP) \
//    collapse(3) \
//    firstprivate(p) // TODO
    for (int i = std::max<int>(p->cell[0] - nlc, 0);
         i <= std::min<int>(p->cell[0] + nlc, Partitioning::cellCountX - 1);
         ++i) {
        for (int j = std::max<int>(p->cell[1] - nlc, 0);
             j <= std::min<int>(p->cell[1] + nlc, Partitioning::cellCountY - 1);
             ++j) {
            for (int k = std::max<int>(p->cell[2] - nlc, 0);
                 k <= std::min<int>(p->cell[2] + nlc, Partitioning::cellCountZ - 1);
                 ++k) {
                for (int l = 0; l < Partitioning::net->getCell(i, j, k).size(); ++l)
                {
                    n = Partitioning::net->getCell(i, j, k)[l];
                    if (p != n)
                    {
                        if (p->r->dist(*n->r) > Settings::NEIGHBOUR_RANGE)
                        {
                            continue; /////////////////////////////////////////////////////////////////////////////
                        }
//                            if (id == 20) {
//                                float f[3] = {1.0f, 0, 0};
//                                recolor(f);
//                            }
//#pragma omp critical
                        p->neighbours->push_back(n);
                    }
                }
            }
        }
    }
}
