#include "physics/partitioning.h"

#include "object/particle.h"
#include "physics/geometry.h"
#include "physics/vector.h"
#include "util/debugger.h"
#include "util/enums.h"
#include "util/settings.h"

#include <iostream>

Partitioning *Partitioning::net = nullptr;
int Partitioning::cellCount = 0;
int Partitioning::cellCountX;
int Partitioning::cellCountY;
int Partitioning::cellCountZ;

std::vector<int> Partitioning::getCellCoordinates(Particle *p) {
    //  TODO
    //   it shouldn't be done here on call of Grid::update() particles should
    //   already be inside the premises, in fact - they should always be
    // problem may be in: Walls::collide, >>Computer::collide<<, ...
    // Computer::Collide collides particles which may get out of bounds
    //   (and then get collided again and get former position out of bounds
    //    as well), before checking Wall collisions!!!
    double r = Settings::ARENA_DIAMETER / 2;
    double rz = Settings::ARENA_DIAMETER_Z / 2;

    // with this: flows out, without: crashes
    p->r->limit(r, r, rz);

    p->r->assertLimits(r, r, rz, "Partitioning::getCellCoordinates");

    int i = static_cast<int>(floor((p->r->x + r) / Settings::MESH_CELL_DIAMETER));
    int j = static_cast<int>(floor((p->r->y + r) / Settings::MESH_CELL_DIAMETER));
    int k = static_cast<int>(floor((p->r->z + rz) / Settings::MESH_CELL_DIAMETER));

    return std::vector<int>{
        op::limit(i, 0, Partitioning::cellCountX - 1),
        op::limit(j, 0, Partitioning::cellCountY - 1),
        op::limit(k, 0, Partitioning::cellCountZ - 1),
    };
}

void Partitioning::remove(Particle *p)
{
    vector<Particle*> &cell = getCell(p);
    cell.erase(std::remove(cell.begin(), cell.end(), p), cell.end());
    push(*p);
}

void Partitioning::updateNeighbours(Particle *p)
{
    if (!Settings::PARALLEL_CUDA)
    {
        if (p->neighbours == nullptr)
        {
            p->neighbours = new std::vector<Particle*>();
        }
        else
        {
            p->neighbours->clear();
        }
    }
}
