#pragma once
//#ifndef COMPUTER_H
//#define COMPUTER_H

class Particle;
class Timer;
class Vector;

#include <functional>

class Computer
{
public:
    static Computer *currentComputer;
    static Timer timer;

    Computer();

    static void zeroInternalForces();
    static void zeroExternalForces();
    static void evaluateSPHForces();

    void computeVectors(double dt);

    static void updateDensityAndPressure();
    static void computeForces();

    static void updateDensity(Particle &p);
    static void updatePressure(Particle &p);
    static void computePressureForces(Particle &p);
    static void computeViscosityForces(Particle &p);
    static void computeBuoyancyForces(Particle &p);
    static void computeSurfaceTensionForces(Particle &p);
    static void computeOtherForces(Particle &p);

    static Vector neighbourBasedVelocity(Particle &p);

    static void evaluateMaximalDensity();
    static void evaluateMaximalVelocity();
    static void evaluateMaximalPressure();
    static void evaluateMaximalViscosity();
    static void evaluateMaximalBuoyancy();
    static void evaluateMaximalTension();

    static void forEachParticleNonParallel(std::function<void(Particle*)> f);
    static void forEachParticleParallel(std::function<void(Particle*)> f);
    static void forEachParticleParallelGuided(std::function<void(Particle *)> f);
    static void forEachParticleNonParallel(std::function<void(Particle*, int i, int j)> f);
    static void forEachParticleParallel(std::function<void(Particle*, int i, int j)> f);
};

//#endif // COMPUTER_H
