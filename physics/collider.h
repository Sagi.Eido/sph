#pragma once
//#ifndef COLLIDER_H
//#define COLLIDER_H

#include <map>
#include <variant>
#include <vector>

class Particle;
class Vector;
class Segment;

class Timer;

class Collider
{
private:
    static Timer timer;

    static void preCollisions();
    static void applyEvaluatedCollisions();
    static void postCollisions();
    static void naiveCollisionsCPU(bool execute);
    static void neighbourhoodCollisionsCPU(bool execute);
    static void obstacleCollisions(bool execute);
    static void particleCollisions();

public:    
    static std::vector<bool> particleCollision;
    static std::vector<double> particleCollisionDistance;
    static unsigned particleCollisionsPerStep;

    static void initializeCollisions();

    static void resetCollisions();
    static std::map<std::string, std::variant<bool, double>> getCollision(
        const Particle &p1, const Particle &p2);
    static void setCollision(
        const Particle &p1, const Particle &p2,
        bool happened, double distance);

    static void collideParticles();
    static bool doCollide(const Particle &p1, const Particle &p2);
    static void collisionDetect();
    static void collideParticles(const Particle &p1, const Particle &p2, bool execute);
    static std::map<std::string, std::variant<double, Vector>>
        collideParticleWithSegment(Particle *p, Segment *w, bool execute);

    static void collideParticleWithLimitingPlanes(Particle *p);

////////////////////////////////////////////////////////////////////////////////

    static void collideParticles(const Particle &p1, const std::vector<Particle*>* with);
};

//#endif // COLLIDER_H
