#pragma once
//#ifndef GEOMETRY_H
//#define GEOMETRY_H

#include "physics/geometry.h"
#include "physics/vector.h"

#include <QVector4D>

namespace geometry {
    class Ray;
    class Sphere;
    class Plane;
    class Line;
}

namespace op {
    double sgn(double x);
    int limit(int x, int lower, int upper);
}

void divideByW(QVector4D &v);
double timePointPointClosest(
    const Vector &r1, const Vector &v1,
    const Vector &r2, const Vector &v2);
double distWhenPointPointClosest(
    const Vector &r1, const Vector &v1,
    const Vector &r2, const Vector &v2);
geometry::Ray normalized2DPointToRay(float normalizedX, float normalizedY);
double         pointRayDist(const Vector &point, const geometry::Ray &ray);
double        pointlineDist(const Vector &p, const geometry::Line &l);
Vector          pointToLine(const Vector &p, const geometry::Line &l);
Vector        pointLineProj(const Vector &p, const geometry::Line &l);
bool   pointLiesFrontOfLine(const Vector &p, const geometry::Line &l);
bool         pointLiesOnSeg(const Vector &p, const geometry::Line &l);
bool     pointProjectsToSeg(const Vector &p, const geometry::Line &l);
Vector pointSegClosestPoint(const Vector &p, const geometry::Line &l);
double         lineLineDist(const geometry::Line &l1, const geometry::Line &l2);
bool      lineLineIntersect(const geometry::Line &l1, const geometry::Line &l2);
double           segSegDist(const geometry::Line &l1, const geometry::Line &l2);
bool        segSegIntersect(const geometry::Line &l1, const geometry::Line &l2);
Vector rayPlaneIntersection(const geometry::Ray &ray, const geometry::Plane &plane);
bool     sphereRayIntersect(const geometry::Sphere &sphere, const geometry::Ray &ray);
bool  pointLiesFrontOfPlane(const Vector &point, const geometry::Plane &plane);

class geometry::Ray {
public:
    Vector source, vector;
    Ray(const Vector &source, const Vector &vector) {
        this->source = source;
        this->vector = vector;
    }
};

class geometry::Sphere {
public:
    Vector center;
    double radius;
    Sphere(const Vector &center, double radius) {
        this->center = center;
        this->radius = radius;
    }
};

class geometry::Plane {
public:
    mutable Vector point, normal;
    Plane(const Vector &point, const Vector &normal) {
        this->point  = point;
        this->normal = normal;
    }
};

class geometry::Line {
public:
    Vector p1, p2;
    Line() {}
    Line(const Vector &p1, const Vector &p2) {
        this->p1 = p1;
        this->p2 = p2;
    }

    Vector normal() const
    {
        return (p2 - p1).normalized().perp2d();
    }
};

//#endif // GEOMETRY_H
