#pragma once
//#ifndef KERNELIZER_H
//#define KERNELIZER_H

class Particle;
class Vector;

#include "util/settings.h"

/**
    Let's have:
        Scalar Field - phi - p
        Vector Field - f
        Nabla Operator - v
        Laplacian - ^

    Gradient (scalar field -> vector field):
        grad(p) = vp

    Divergence (vector field -> scalar field):
        div(f) = v.f

    Laplacian (scalar field -> scalar field):
        ^ = v.v = v2
        ^(p) = div(grad(p))

    Vector Laplacian (vector field -> vector field):
        ^(f) = v2f = v(v.f) - v*(v*f)
*/

class Kernelizer
{
private:
    static double kernelConstant(
        KernelSlope kernel, unsigned dim, double smoothingLength);
    static double kernelGradientConstant(
        KernelSlope kernel, unsigned dim, double smoothingLength);
    static double kernelLaplacianConstant(
        KernelSlope kernel, unsigned dim, double smoothingLength);

    static double kernel(
        const Particle *p1, const Particle *p2,
        KernelSlope kernel = Settings::DEFAULT_KERNEL_SLOPE,
        unsigned dim = Settings::KERNEL_DIM
    );
    static Vector kernelGradient(
        const Particle *p1, const Particle *p2,
        KernelSlope kernel = Settings::DEFAULT_KERNEL_SLOPE,
        unsigned dim = Settings::KERNEL_DIM
    );
    // Laplacian = Gradient Divergence = div(grad)
    static double kernelLaplacian(
        const Particle *p1, const Particle *p2,
        KernelSlope kernel = Settings::DEFAULT_KERNEL_SLOPE,
        unsigned dim = Settings::KERNEL_DIM
    );

    static double kernelDivergence(
        const Particle *p1/*,
        KernelSlope kernel = Settings::DEFAULT_KERNEL_SLOPE*/
    );

public:
    static double defaultKernel(const Particle *p1, const Particle *p2);
    static double pressureKernel(const Particle *p1, const Particle *p2);
    static double viscosityKernel(const Particle *p1, const Particle *p2);

    static Vector defaultKernelGradient(const Particle *p1, const Particle *p2);
    static Vector pressureKernelGradient(const Particle *p1, const Particle *p2);
    static Vector viscosityKernelGradient(const Particle *p1, const Particle *p2);

    static double defaultKernelLaplacian(const Particle *p1, const Particle *p2);
    static double pressureKernelLaplacian(const Particle *p1, const Particle *p2);
    static double viscosityKernelLaplacian(const Particle *p1, const Particle *p2);
};

//#endif // KERNELIZER_H
