#pragma once
//#ifndef NEIGHBOURHOOD_H
//#define NEIGHBOURHOOD_H

class Particle;

namespace neighbourhood {
    void updateNeighboursAll();
    void updateNeighbours(Particle *p);
    void unNeighbour(Particle *p);
};

//#endif // NEIGHBOURHOOD_H
