#include "physics/forces.h"

#include "physics/geometry.h"
#include "physics/vector.h"
#include "util/constants.h"
#include "util/settings.h"

#include <limits>

Vector Forces::gravityVector()
{
    return Vector(0, Settings::FORCE_GRAV_SURFACE, 0);
}

void Forces::universalGravitation(const Particle &p1, const Particle &p2)
{ // F1->2 = -G * m1 * m2 * r / |r|^2
    if (&p1 != &p2 && ! p1.isStationary)
    {
        Vector r21 = *p1.r - *p2.r;
        double d = r21.norm();
        double M = p1.m * p2.m;
        *p1.F -= r21.normalized() * (G_const * M/(d*d)) \
                 * Settings::FORCE_GRAV_UNIVERSAL_X; // TODO
    }
}

void Forces::gravityEarth(const Particle &p)
{
    if (! p.isStationary)
    {
        *p.F += gravityVector() * p.m;
    }
}

void Forces::rhoGravityEarth(const Particle &p)
{
    if (! p.isStationary)
    {
        *p.F += gravityVector() * p.rho;
    }
}

void Forces::Coulomb(const Particle &p1, const Particle &p2)
{
    if (&p1 != &p2 && ! p1.isStationary)
    {
        double Q = p1.charge * p2.charge;
        Vector r21 = *p1.r - *p2.r;
        double d = r21.norm();
        r21 = r21.normalized();
        *p1.F -= r21 * (Coulomb_const * Q/(d*d));
    }
}

void Forces::Friction(const Particle &p)
{
    if (! p.isStationary)
    {
        *p.F -= Vector(*p.F).normalized()
            * (-1 * Settings::FORCE_FRICTION * p.F->y);
    }
}

void Forces::Hooke(const Particle &p1, const Particle &p2,
                   double ks, double d, double kd)
{ // F-> = -ks . x-> // d = targetSpringDistance
    if (! p1.isStationary || ! p2.isStationary)
    {
        Vector r12 = *p1.r - *p2.r;
        Vector v12 = *p1.v - *p2.v;
        double fs = ks * fabs(r12.norm() - d);
        double fd = kd * v12.dot(r12) / r12.norm();
        Vector fH = r12.normalized() * (fs + fd);
        if (! p1.isStationary) *p1.F -= fH;
        if (! p2.isStationary) *p2.F += fH;
    }
}
