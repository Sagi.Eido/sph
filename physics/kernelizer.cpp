#include "physics/kernelizer.h"

#include "object/particle.h"
#include "physics/vector.h"
#include "util/constants.h"
#include "util/debugger.h"
#include "util/enums.h"
#include "util/macros.h"
#include "util/settings.h"

#include <iostream>

//#define EVALUATE_C_H_ONLY_ONCE
#define EVALUATE_C_H_ON_EVERY_KERNEL_FUNCTION_CALL

#ifdef EVALUATE_C_H_ONLY_ONCE
    #define OPTIONALLY_ASSERT_THAT_CALLED_ONLY_ONCE ASSERT_CALLED_ONLY_ONCE
    #define OPTIONAL_STATIC_ON_C_H static
#endif

#ifdef EVALUATE_C_H_ON_EVERY_KERNEL_FUNCTION_CALL
    #define OPTIONALLY_ASSERT_THAT_CALLED_ONLY_ONCE ;
    #define OPTIONAL_STATIC_ON_C_H ;
#endif

double Kernelizer::kernelConstant(
    KernelSlope kernel, unsigned dim, double smoothingLength)
{
    OPTIONALLY_ASSERT_THAT_CALLED_ONLY_ONCE

    double h = smoothingLength;
    double C_h = std::numeric_limits<double>::quiet_NaN();

    if (dim < 1 || dim > 3) Debugger::throwWithMessage("Wrong dimension: " + dim); /////////////////////////////////

    switch (kernel) {
        case GAUSSIAN :
            switch (dim) {
                case 1: C_h = 1. / (pow(M_PI, 0.5) * h);         break;
                case 2: C_h = 1. / (    M_PI       * pow(h, 2)); break;
                case 3: C_h = 1. / (pow(M_PI, 1.5) * pow(h, 3)); break;
            }
        break;
        case SUPER_GAUSS :
            Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_NOT_IMPLEMENTED");
        break;
        case CUBIC_MONAGHAN :
            switch (dim) { // Cubic Spline
                case 1: C_h = 1.  / ( 6        * h);         break;
                case 2: C_h = 15. / (14 * M_PI * pow(h, 2)); break;
                case 3: C_h = 1.  / ( 4 * M_PI * pow(h, 3)); break;
            }
        break;
        case QUINTIC_LIU :
            switch (dim) { // Quintic Spline SPH kernel: [Liu2010]
                case 1: C_h = 1. / (120 * h);                break;
                case 2: C_h = 7. / (478 * M_PI * pow(h, 2)); break;
                case 3: C_h = 3. / (359 * M_PI * pow(h, 3)); break;
            }
        break;
        case POLY_6 :
            switch (dim) {
                case 1: Debugger::throwWithMessage("POLY_6_1D_KERNEL_NOT_IMPLEMENTED"); break;
                case 2: C_h = 4.   / (     M_PI * pow(h, 8)); break;
                case 3: C_h = 315. / (64 * M_PI * pow(h, 9)); break;
            }
        break;
        case SPIKY_DEBRUN :
            switch (dim) {
                case 1: Debugger::throwWithMessage("SPIKY_1D_KERNEL_NOT_IMPLEMENTED"); break;
                case 2: C_h = 10. / (M_PI * pow(h, 5)); break;
                case 3: C_h = 15. / (M_PI * pow(h, 6)); break;
            }
        break;
        case VISCOSITY_SPECIFIC_KERNEL :
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_VISCOSITY_KERNEL_NOT_IMPLEMENTED"); break;
                case 2: C_h = 10. / (3 * M_PI * pow(h, 2)); break;
                case 3: C_h = 15. / (2 * M_PI * pow(h, 3)); break;
            }
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    return C_h;
}

double Kernelizer::kernel(
    const Particle *p1, const Particle *p2,
    KernelSlope kernel,
    unsigned dim)
{
    double h = p1->smoothingLength;
    double d = p1->r->dist(*p2->r);
    double q = d / h;
    OPTIONAL_STATIC_ON_C_H double C_h = kernelConstant(kernel, dim, h);
    double W = 0.;

    switch (kernel) {
        case GAUSSIAN :
            if      (q >= 0. && q <= 3.) W = pow(M_E, -pow(q, 2));
            else if (q >  3)             W = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case SUPER_GAUSS :
            Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_NOT_IMPLEMENTED");
        break;
        case CUBIC_MONAGHAN :
            if      (q >= 0. && q < 1.) W = pow(2-q, 3) - 4*pow(1-q, 3);
            else if (q >= 1. && q < 2.) W = pow(2-q, 3);
            else if (q >= 2.)           W = 0.;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case QUINTIC_LIU : // Horner Form
            if      (q >= 0. && q < 1.) W = ((30-10*q)*q*q-60)*q*q+66; // W = pow(3-q, 5) - 6*pow(2-q, 5) + 15*pow(1-q, 5);
            else if (q >= 1. && q < 2.) W = q*(q*(q*(q*(5*q - 45)+150)-210)+75)+51; // W = pow(3-q, 5) - 6*pow(2-q, 5);
            else if (q >= 2. && q < 3.) W = q*(q*(q*((15-q)*q-90)+270)-405)+243; // W = pow(3-q, 5);
            else if (q >= 3.)           W = 0;
            else Debugger::throwWithMessage("QUINTIC_KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case POLY_6 :
            if      (d >= 0. && d <= h) W = pow(pow(h, 2) - pow(d, 2), 3);
            else if (d > h)             W = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case SPIKY_DEBRUN :
            if      (d >= 0. && d <= h) W = pow(h-d, 3);
            else if (d > h)             W = 0;
            else Debugger::throwWithMessage("SPIKY_KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case VISCOSITY_SPECIFIC_KERNEL :
            //d = 0 => W = inf
            if      (d >= 0. && d <= h) W = - pow(d, 3)/(2*pow(h, 3)) + pow(d, 2)/pow(h, 2) + h/(2*d) - 1;
            else if (d > h)             W = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    W *= C_h;

    return W;
}

double Kernelizer::kernelGradientConstant(
    KernelSlope kernel, unsigned dim, double smoothingLength)
{
    OPTIONALLY_ASSERT_THAT_CALLED_ONLY_ONCE

    double h = smoothingLength;
    double C_h = std::numeric_limits<double>::quiet_NaN();

    if (dim < 1 || dim > 3) Debugger::throwWithMessage("Wrong dimension: " + dim); /////////////////////////////////

    switch (kernel) {
        case GAUSSIAN :
            switch (dim) {
                case 1: C_h = 1. / (    sqrt(M_PI)     * h);         break;
                case 2: C_h = 1. / (pow(sqrt(M_PI), 2) * pow(h, 2)); break;
                case 3: C_h = 1. / (pow(sqrt(M_PI), 3) * pow(h, 3)); break;
            }
        break;
        case SUPER_GAUSS :
            Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_GRAD_NOT_IMPLEMENTED");
        break;
        case CUBIC_MONAGHAN : // Cubic Spline
            switch (dim) {
                case 1: C_h =  -1. / (6        * pow(h, 2)); break;
                case 2: C_h = -15. / (7 * M_PI * pow(h, 3)); break;
                case 3: C_h =  -3. / (4 * M_PI * pow(h, 4)); break;
            }
        break;
        case QUINTIC_LIU : // Quintic Spline SPH kernel: [Liu2010]
            switch (dim) {
                case 1: C_h = -1. / (120        * pow(h, 2)); break;
                case 2: C_h = -7. / (239 * M_PI * pow(h, 3)); break;
                case 3: C_h = -9. / (359 * M_PI * pow(h, 4)); break;
            }
        break;
        case POLY_6 :
            switch (dim) {
                case 1: Debugger::throwWithMessage("POLY_6_1D_KERNEL_GRAD_NOT_IMPLEMENTED"); break;
                case 2: C_h = -24.  / (     M_PI * pow(h, 8)); break;
                case 3: C_h = -945. / (32 * M_PI * pow(h, 9)); break;
            }
        break;
        case SPIKY_DEBRUN :
            switch (dim) {
                case 1: Debugger::throwWithMessage("SPIKY_1D_KERNEL_GRAD_NOT_IMPLEMENTED"); break;
                case 2: C_h = -30. / (M_PI * pow(h, 5)); break;
                case 3: C_h = -45. / (M_PI * pow(h, 6)); break;
            }
        break;
        case VISCOSITY_SPECIFIC_KERNEL :
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_VISCOSITY_KERNEL_GRAD_NOT_IMPLEMENTED"); break;
                case 2: C_h = -10. / (    M_PI * pow(h, 2)); break;
                case 3: C_h = -15. / (2 * M_PI * pow(h, 3)); break;
            }
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    return C_h;
}

Vector Kernelizer::kernelGradient(
    const Particle *p1, const Particle *p2,
    KernelSlope kernel,
    unsigned dim
) {
    double h = p1->smoothingLength;
    double d = p1->r->dist(*p2->r);
    double q = d / h;
    OPTIONAL_STATIC_ON_C_H double C_h = kernelGradientConstant(kernel, dim, h);
    double dW = 0;

    switch (kernel) {
        case GAUSSIAN :
            if      (q <  3.) dW = std::exp(-q*q);
            else if (q >= 3.) dW = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case SUPER_GAUSS :
            Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_GRAD_NOT_IMPLEMENTED");
        break;
        case CUBIC_MONAGHAN : // Cubic Spline
            if      (q >= 0. && q < 1.) dW = q * (9*q - 4);
            else if (q >= 1. && q < 2.) dW = q * (4 - 3*q) - 4;
            else if (q >= 2.)           dW = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case QUINTIC_LIU : // Quintic Spline SPH kernel: [Liu2010] // Horner Form
            if      (q >= 0. && q < 1.) dW = q*((120-50*q)*q*q-120); // dW = -10*q * (5*pow(q, 3) - 12*pow(q, 2) + 12);
            else if (q >= 1. && q < 2.) dW = q*(q*(q*(25*q-180)+450)-420)+75; // dW = 30*pow(2-q, 4) - 5*pow(3-q, 4);
            else if (q >= 2. && q < 3.) dW = q*(q*((60-5*q)*q-270)+540)-405; // dW = -5*pow(3-q, 4);
            else if (q >= 3.)           dW = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case POLY_6 :
            if      (d >= 0. && d <= h) dW = pow(pow(h, 2) - pow(d, 2), 2);
            else if (d > h)             dW = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case SPIKY_DEBRUN :
            if      (d >= 0. && d <= h) dW = pow(h-d, 2);
            else if (d > h)             dW = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case VISCOSITY_SPECIFIC_KERNEL : // d -> 0- => W = +inf, d -> 0+ => W = -inf
            if      (d >= 0. && d <= h) dW = - 3*d/(2*pow(h, 3)) + 2./pow(h, 2) - h/(2*pow(d, 3));
            else if (d > h)             dW = 0;
            // Wolfram below:
            //if      (d >= 0. && d <= h) dW = (1. / (6 * pow(h, 3) * pow(d, 2))) * (pow(h, 4) - 4*h*pow(d, 3) + 3*pow(d, 4));
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    dW *= C_h;

    // TODO can we remove this normalization?
    Vector grad_W = d != 0. ? (*p1->r - *p2->r) / d * dW : Vector(); // TODO zero gradient
    //Vector grad_W = d != 0. ? (*p1->r - *p2->r) * dW : Vector(); // TODO zero gradient

    return grad_W;
}

double Kernelizer::kernelLaplacianConstant(
    KernelSlope kernel, unsigned dim, double smoothingLength)
{
    OPTIONALLY_ASSERT_THAT_CALLED_ONLY_ONCE

    double h = smoothingLength;
    double C_h = std::numeric_limits<double>::quiet_NaN();

    if (dim < 1 || dim > 3) Debugger::throwWithMessage("Wrong dimension: " + dim); /////////////////////////////////

    switch (kernel) {
        case GAUSSIAN       : Debugger::throwWithMessage("GAUSSIAN_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case SUPER_GAUSS    : Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case CUBIC_MONAGHAN : Debugger::throwWithMessage("CUBIC_MONAGHAN_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case QUINTIC_LIU :
        // Wofram Alpha; input for 0 <= q < 1 :
        // C_h for 2D = (7/(478*pi*h^2)
        // C_h for 3D = (3/(359*pi*h^3)
        // (C_h * ((3-r/h)^5 - 6*(2-r/h)^5 + 15*(1-r/h)^5))'
        // grad(C_h * ((3-r/h)^5 - 6*(2-r/h)^5 + 15*(1-r/h)^5))
        // (C_h * ((3-r/h)^5 - 6*(2-r/h)^5 + 15*(1-r/h)^5))''
        // laplacian(C_h * ((3-r/h)^5 - 6*(2-r/h)^5 + 15*(1-r/h)^5))
        // need to use r instead of d and r/h instead of q for Wolfram Alpha
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_QUINTIC_LAPLACIAN_NOT_IMPLEMENTED"); break;
                case 2: C_h = -35. / (478 * M_PI * pow(h, 7)); break;
                case 3: C_h = -15. / (359 * M_PI * pow(h, 8)); break;
            }
        break;
        case POLY_6 :
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_POLY_6_LAPLACIAN_NOT_IMPLEMENTED"); break;
                case 2: C_h = -24.  / (     M_PI * pow(h, 8)); break;
                case 3: C_h = -945. / (32 * M_PI * pow(h, 9)); break;
            }
        break;
        case SPIKY_DEBRUN :
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_SPIKY_LAPLACIAN_NOT_IMPLEMENTED"); break;
                case 2: C_h = -60. / (M_PI * pow(h, 5)); break;
                case 3: C_h = -90. / (M_PI * pow(h, 6)); break;
            }
        break;
        case VISCOSITY_SPECIFIC_KERNEL :
            switch (dim) {
                case 1: Debugger::throwWithMessage("1D_VISC_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
                case 2: C_h = -20. / (    M_PI * pow(h, 5)); break;
                case 3: C_h = -45. / (2 * M_PI * pow(h, 6)); break;
            }
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    return C_h;
}

double Kernelizer::kernelLaplacian(
    const Particle *p1, const Particle *p2,
    KernelSlope kernel,
    unsigned dim
) {
    double h = p1->smoothingLength;
    double d = p1->r->dist(*p2->r);
    double q = d / h;
    OPTIONAL_STATIC_ON_C_H double C_h = kernelLaplacianConstant(kernel, dim, h);
    double L = 0;

    switch (kernel) {
        case GAUSSIAN       : Debugger::throwWithMessage("GAUSSIAN_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case SUPER_GAUSS    : Debugger::throwWithMessage("SUPER_GAUSS_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case CUBIC_MONAGHAN : Debugger::throwWithMessage("CUBIC_MONAGHAN_KERNEL_LAPLACIAN_NOT_IMPLEMENTED"); break;
        case QUINTIC_LIU :
            // Horner Form:
            if      (q >= 0. && q < 1.) L = d*d*(50*d-96*h)+48*pow(h,3); // L = 2 * (24*pow(h, 3) - 48*h*pow(d, 2) + 25*pow(d,3));
            else if (q >= 1. && q < 2.) L = (d*(d*(d*(144*h-25*d)-270*h*h)+168*pow(h,3))-15*pow(h,4))/d; // L = (-1./d) * (15*pow(h,4) - d*168*pow(h,3) + 270*pow(h,2)*pow(d,2) - 144*h*pow(d,3) + 25*pow(d,4));
            else if (q >= 2. && q < 3.) L = (d*(d*(d*(5*d-48*h)+162*h*h)-216*pow(h,3))+81*pow(h,4))/d; // L = ( 1./d) * (3*h - 5*d) * pow(3*h - d, 3);
            else if (q >= 3.)           L = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case POLY_6 :
            if      (d >= 0. && d <= h) L = (pow(h, 2) - pow(d, 2)) * (3*pow(h, 2) - 7*pow(d, 2));
            // Wolfram below
            //if      (d >= 0. && d <= h) L = 2 * (pow(h, 4) - 4*pow(h, 2) * pow(d, 2) + 3*pow(d, 4));
            else if (d > h)             L = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case SPIKY_DEBRUN :
            if      (d >= 0. && d <= h) L = (h-d) * (h-2*d);
            else if (d > h)             L = 0;
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        case VISCOSITY_SPECIFIC_KERNEL :
            if      (d >= 0. && d <= h) L = (h-d);
            else if (d > h)             L = 0;
            // Wolfram below:
            //if      (d >= 0. && d <= h) L = (-1. / (12 * pow(d, 3))) * (pow(h, 4) + 8*h*pow(d, 3) - 9*pow(d, 4));
            else Debugger::throwWithMessage("KERNEL_UNDEFINED_DOMAIN_REACHED");
        break;
        default: Debugger::throwWithMessage("No Kernel provided!");
    }

    L *= C_h;

    return L;
}

////////////////////////////////////////////////////////////////////////////////

double Kernelizer::kernelDivergence(const Particle *p1/*, KernelSlope kernel*/)
{
    double divergence = 0.;
    Particle *n = nullptr;
    if (Settings::PARALLEL_CUDA)
    {
        #ifdef COMPILER_MSVC
        int start = (p1->id - 1) * Settings::PARTICLE_COUNT;
        for (int i = start; i < start + Settings::PARTICLE_COUNT; ++i)
        {
            if (Particle::neighbours_host[i])
            {
                n = p1->getParentFlowParticle(i - start);
                divergence += n->v->dot(kernelGradient(p1, n)) * n->m;
            }
        }
        #endif
    }
    else
    {
        for (unsigned i = 0; i < p1->neighbours->size(); ++i)
        {
            n = (*p1->neighbours)[i];
            divergence += n->v->dot(kernelGradient(p1, n)) * n->m;
        }
    }
    divergence *= -1. / p1->rho;
    return divergence;
}

////////////////////////////////////////////////////////////////////////////////

double Kernelizer::defaultKernel(const Particle *p1, const Particle *p2) {
    return kernel(p1, p2);
}
double Kernelizer::pressureKernel(const Particle *p1, const Particle *p2) {
    return kernel(p1, p2, Settings::PRESSURE_KERNEL_SLOPE);
}
double Kernelizer::viscosityKernel(const Particle *p1, const Particle *p2) {
    return kernel(p1, p2, Settings::VISCOSITY_KERNEL_SLOPE);
}

Vector Kernelizer::defaultKernelGradient(const Particle *p1, const Particle *p2) {
    return kernelGradient(p1, p2);
}
Vector Kernelizer::pressureKernelGradient(const Particle *p1, const Particle *p2) {
    return kernelGradient(p1, p2, Settings::PRESSURE_KERNEL_SLOPE);
}
Vector Kernelizer::viscosityKernelGradient(const Particle *p1, const Particle *p2) {
    return kernelGradient(p1, p2, Settings::VISCOSITY_KERNEL_SLOPE);
}

double Kernelizer::defaultKernelLaplacian(const Particle *p1, const Particle *p2) {
    return kernelLaplacian(p1, p2);
}
double Kernelizer::pressureKernelLaplacian(const Particle *p1, const Particle *p2) {
    return kernelLaplacian(p1, p2, Settings::PRESSURE_KERNEL_SLOPE);
}
double Kernelizer::viscosityKernelLaplacian(const Particle *p1, const Particle *p2) {
    return kernelLaplacian(p1, p2, Settings::VISCOSITY_KERNEL_SLOPE);
}

////////////////////////////////////////////////////////////////////////////////
