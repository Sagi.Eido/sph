#include "dot.h"

#include "graphics/form.h"

Dot::Dot() : color {0, 0, 0} {}

Dot::Dot(float color[])
{
    if (color == nullptr) vertices = std::vector<float> {0, 0, 0, 0, 0};
    else                  vertices = std::vector<float> {0, 0, color[0], color[1], color[2]};
}

void Dot::makeForm() {
    form = new Form(
        vertices, 2, GL_POINTS,
        std::vector<float>(), 3,
        std::vector<float>(), 0, 0,
        DOT
    );
}
