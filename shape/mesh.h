#pragma once
//#ifndef MESH_H
//#define MESH_H

#include "graphics/paintable.h"
#include "shape/shape.h"

namespace shapeSpace {
    class Mesh;
}

class shapeSpace::Mesh : public Shape, public Paintable {

public:
    Mesh();

    virtual void makeForm() override;

    void move(
        float x1, float y1,
        float x2, float y2,
        float x3, float y3,
        float x4, float y4,
        float x5, float y5
    );

    void createView() override;
    void setModelMatrix() override;
    void paint() override;

};

//#endif // MESH_H
