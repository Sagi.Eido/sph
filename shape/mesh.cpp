#include "shape/mesh.h"

#include "graphics/form.h"
#include "graphics/matrices.h"
#include "object/segment.h"
#include "physics/grid.h"
#include "physics/octree.h"
#include "shape/line.h"
#include "util/debugger.h"
#include "util/settings.h"

#include <cmath>

using namespace std;
using namespace shapeSpace;

Mesh::Mesh()
{
    float clr_1[3] = {0.f, 0.1f, 0.f};
    float clr_2[3] = {0.f, 0.5f, 0.f};

    vector<float> clr_1_v = {clr_1[0], clr_1[1], clr_1[2]};
    vector<float> clr_2_v = {clr_2[0], clr_2[1], clr_2[2]};

    float lim = float(Settings::ARENA_DIAMETER / 2
                      - Settings::PARTICLE_INIT_DIST
                        * Settings::GHOST_LAYER_GAGE);
    float lim_z = float(Settings::ARENA_DIAMETER_Z / 2
                        - Settings::PARTICLE_INIT_DIST
                          * Settings::GHOST_LAYER_GAGE);

    switch (Settings::NEIGHBOUR_CHOICE)
    {
        case ALL : break;
        case GRID :
        {
            if (Settings::PAINT_GRID) {
                for (int j = 0; j <= Partitioning::cellCountX; ++j)
                {
                    vector<float> v1 = {
                        -lim,
                        -lim + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                        -lim_z,
                         lim,
                        -lim  + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                        -lim_z
                    };
                    vector<float> v2 = {
                        -lim + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                        -lim,
                        -lim_z,
                        -lim + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                         lim,
                        -lim_z
                    };

                    Line l1 = Line(v1, clr_1_v);
                    Line l2 = Line(v2, clr_1_v);
                    vertices.insert(vertices.end(), l1.vertices.begin(), l1.vertices.end());
                    vertices.insert(vertices.end(), l2.vertices.begin(), l2.vertices.end());
                }

                for (int j = 0; j <= Partitioning::cellCountZ; ++j)
                {
                    vector<float> v1 = {
                        -lim,
                        -lim,
                        -lim_z + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                         lim,
                        -lim,
                        -lim_z + static_cast<float>(Settings::MESH_CELL_DIAMETER * j)
                    };
                    Line l1 = Line(v1, clr_1_v);
                    vertices.insert(vertices.end(), l1.vertices.begin(), l1.vertices.end());
                }
                for (int j = 0; j <= Partitioning::cellCountX; ++j)
                {
                    vector<float> v2 = {
                        -lim + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                        -lim,
                        -lim_z,
                        -lim + static_cast<float>(Settings::MESH_CELL_DIAMETER * j),
                        -lim,
                         lim_z
                    };
                    Line l2 = Line(v2, clr_1_v);
                    vertices.insert(vertices.end(), l2.vertices.begin(), l2.vertices.end());
                }
            }
            break;
        }
        case OCTREE :
        {
            if (Settings::PAINT_GRID) {
                vector<Octree*> nodes;// = Octree::root->getLeaves();
                for (unsigned i = 0; i < nodes.size(); ++i)
                {
                    vector<float> v1 = {
                        -lim,
                         static_cast<float>(nodes[i]->center[1]),
                         static_cast<float>(nodes[i]->center[2]),
                         lim,
                         static_cast<float>(nodes[i]->center[1]),
                         static_cast<float>(nodes[i]->center[2])
                    };
                    vector<float> v2 = {
                         static_cast<float>(nodes[i]->center[0]),
                        -lim,
                         static_cast<float>(nodes[i]->center[2]),
                         static_cast<float>(nodes[i]->center[0]),
                         lim,
                         static_cast<float>(nodes[i]->center[2])
                    };
    //                vector<float> v3 = {
    //                     static_cast<float>(nodes[i]->center[0]),
    //                     static_cast<float>(nodes[i]->center[1]),
    //                    -lim,
    //                     static_cast<float>(nodes[i]->center[0]),
    //                     static_cast<float>(nodes[i]->center[1]),
    //                     lim,
    //                };
                    Line l1 = Line(v1, clr_1_v);
                    Line l2 = Line(v2, clr_1_v);
    //                Line l3 = Line(v3, clr_1_v);
                    vertices.insert(vertices.end(), l1.vertices.begin(), l1.vertices.end());
                    vertices.insert(vertices.end(), l2.vertices.begin(), l2.vertices.end());
    //                vertices.insert(vertices.end(), l3.vertices.begin(), l3.vertices.end());
                }
            }
            break;
        }
        default :
            Debugger::throwWithMessage("Switch failure at neighbourhood method choice.");
        break;
    }

    makeForm();
    linkView(MESH);
}

void Mesh::makeForm()
{
    posCoordsPerVertex = 3;
    clrCoordsPerVertex = 3;
    form = new Form(
        vertices, posCoordsPerVertex, GL_LINES,//GL_TRIANGLE_FAN
        vector<float>(), clrCoordsPerVertex,
        vector<float>(), 0, 0,
        MESH
    );
}

void Mesh::move(
    float x1, float y1,
    float x2, float y2,
    float x3, float y3,
    float x4, float y4,
    float x5, float y5
) {
    form->posCoords[0]  = x1;
    form->posCoords[1]  = y1;
    form->posCoords[6]  = x2;
    form->posCoords[7]  = y2;
    form->posCoords[12] = x3;
    form->posCoords[13] = y3;
    form->posCoords[18] = x4;
    form->posCoords[19] = y4;
    form->posCoords[24] = x5;
    form->posCoords[25] = y5;
    form->posCoords[30] = x2;
    form->posCoords[31] = y2;
    form->move(); // initialize vertex byte buffer for shape coordinates
}

void Mesh::createView()
{
    this->currentForm = form;
}

void Mesh::setModelMatrix()
{
    Matrices::modelMatrix.setToIdentity();
}

void Mesh::paint()
{
    setModelMatrix();
    Paintable::paint();
}
