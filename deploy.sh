#!/usr/bin/env bash

# Currently deploys only MSVC64 Release builds (at two destinations).

base_dir=E:/Dropbox/well/WFAIS/prog/Qt
qt_dir=E:/dev/Qt
qt_version_dotted=`ls -1a "$qt_dir" | grep -E ^[0-9]+\.[0-9]+\.[0-9]+$`
qt_version_underscored=`sed s/\\\./_/g <<< ${qt_version_dotted}`
windeployqt_for_msvc64_dir=${qt_dir}/${qt_version_dotted}/msvc2017_64/bin
exe_name=SPH.exe

#export VCINSTALLDIR="C:/Program Files (x86)/Microsoft Visual Studio/Shared/14.0/VC/" # for windeployqt.exe
export VCINSTALLDIR="C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/VC" # for windeployqt.exe

deploy_dst_1="${base_dir}/build-SPH-Desktop_Qt_${qt_version_underscored}_MSVC2017_64bit-Release/release"
deploy_dst_2="${base_dir}/DLLs/msvc64"
cp "${deploy_dst_1}/${exe_name}" "${deploy_dst_2}/."
declare -a deploy_dsts=("$deploy_dst_1" "$deploy_dst_2")
for deploy_dst in "${deploy_dsts[@]}"
do
    mkdir -p $deploy_dst
    rm -rf "${deploy_dst}/*"

    ${windeployqt_for_msvc64_dir}/windeployqt "${deploy_dst}/${exe_name}"

    # MS MPI
    cp "C:\Windows\System32\msmpi.dll" $deploy_dst

	# OpenCV
    opencv_dir=E:/dev/opencv/build/install/x64/vc15/bin
	cp "${opencv_dir}/opencv_core330.dll" $deploy_dst
	cp "${opencv_dir}/opencv_imgcodecs330.dll" $deploy_dst
	cp "${opencv_dir}/opencv_imgproc330.dll" $deploy_dst
	cp "${opencv_dir}/opencv_videoio330.dll" $deploy_dst

	# CUDA
    cuda_dir="C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/"
    cuda_version_dotted=`ls -1a "${cuda_dir}" | grep -E v[0-9]+\.[0-9]`
    cuda_version_simple=`sed s/[v\\\.]//g <<< ${cuda_version_dotted}`
	cp "${cuda_dir}${cuda_version_dotted}/bin/cudart64_${cuda_version_simple}.dll" $deploy_dst
	cp "C:/Windows/System32/nvcuda.dll" $deploy_dst
	cp "C:/Windows/System32/nvfatbinaryLoader.dll" $deploy_dst
	cp "C:/Windows/System32/ucrtbased.dll" $deploy_dst

    cp "${base_dir}/SPH/README.md" $deploy_dst

	# MSVC 2015 (in case vcredist_x64.exe fails, because of lacking dependencies, like updates on Window 8.1)
	# but... it seems it still doesn't work with those below if vcredist_x64.exe has not been installed...
	# cp "C:/Windows/System32/concrt140.dll" $deploy_dst
	# cp "C:/Windows/System32/msvcp140.dll" $deploy_dst
	# cp "C:/Windows/System32/vcomp140.dll" $deploy_dst
	# cp "C:/Windows/System32/vcruntime140.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-convert-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-environment-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-filesystem-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-heap-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-locale-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-math-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-multibyte-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-runtime-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-stdio-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-string-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-time-l1-1-0.dll" $deploy_dst
	# cp "C:/Program Files (x86)/Microsoft Visual Studio/2017/Enterprise/Common7/IDE/Remote Debugger/x64/api-ms-win-crt-utility-l1-1-0.dll" $deploy_dst

    cygstart $deploy_dst
done
