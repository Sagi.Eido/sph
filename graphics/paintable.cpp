#include "graphics/paintable.h"

#include "graphics/form.h"
#include "graphics/matrices.h"
#include "util/enums.h"

Paintable::Paintable() : currentForm(nullptr) {}

void Paintable::linkView(ShapeType formShapeType)
{
    if (Form::forms.size() == 0)
    {
        createView();
    }
    else
    {
        for (unsigned i = 0; i < Form::forms.size(); ++i)
        {
            if (Form::forms[i]->type == formShapeType)
            {
                currentForm = Form::forms[i];
                break; ///////////////////////////////////////////////////
            }
            else if (i == Form::forms.size() - 1)
            {
                createView();
                break;
                // break; isn't be needed here, because at createView() Form::forms
                // is appended and at next loop run we'll hit first break
            }
        }
    }
}

void Paintable::paint()
{
    if (currentForm != nullptr)
    {
        if (Form::lastBoundFormType != currentForm->type)
        {
           currentForm->bindVAO();
        }

        Matrices::modelViewMatrix.setToIdentity();
        Matrices::mvpMatrix.setToIdentity();
        Matrices::modelViewMatrix = Matrices::viewMatrix * Matrices::modelMatrix;
        Matrices::mvpMatrix = Matrices::projectionMatrix * Matrices::modelViewMatrix;

        currentForm->draw();
    }
}

void Paintable::recolor(float color[])
{
    currentForm->recolor(color);
}
