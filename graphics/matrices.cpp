#include "graphics/matrices.h"

#include "util/settings.h"
#include "window/simulation_window.h"

QMatrix4x4 Matrices::modelMatrix            = QMatrix4x4();
QMatrix4x4 Matrices::viewMatrix             = QMatrix4x4();
QMatrix4x4 Matrices::modelViewMatrix        = QMatrix4x4();
QMatrix4x4 Matrices::projectionMatrix       = QMatrix4x4();
QMatrix4x4 Matrices::viewProjectionMatrix   = QMatrix4x4();
QMatrix4x4 Matrices::viewProjectionInverted = QMatrix4x4();
QMatrix4x4 Matrices::mvpMatrix              = QMatrix4x4();
float Matrices::camRX = 0.f;
float Matrices::camRY = 0.f;
float Matrices::camTX = 0.f;
float Matrices::camTY = 0.f;
float Matrices::camTZ = 0.f;

void Matrices::resetCamera()
{
    Matrices::camRX = 0.f;
    Matrices::camRY = 0.f;
    Matrices::camTX = 0.f;
    Matrices::camTY = 0.f;
    Matrices::camTZ = 0.f;

    Matrices::viewMatrix.setToIdentity();
    Matrices::viewMatrix.lookAt(
        QVector3D(0, 0, static_cast<float>(Settings::ARENA_DIAMETER)),
        QVector3D(0, 0, 0),
        QVector3D(0, 1, 0));
    Matrices::setViewMatrix();

    Matrices::camTZ = static_cast<float>(Settings::ARENA_DIAMETER * 0.6);
    Matrices::projectionMatrix.setToIdentity();
    Matrices::projectionMatrix.perspective(
        90.f,
        SimulationWindow::simWindow->width()/SimulationWindow::simWindow->height(),
        0.1f,
        Matrices::camTZ * 10);
}

void Matrices::setViewMatrix()
{
    viewMatrix.rotate(camRX, QVector3D(1, 0, 0));
    viewMatrix.rotate(camRY, QVector3D(0, 1, 0));
    viewMatrix.translate(QVector3D(-camTX, -camTY, -camTZ));
}
