#include "graphics/shader/shader.h"

#include "graphics/handles.h"
#include "graphics/matrices.h"
#include "util/parser.h"

Shader *Shader::currentShader = nullptr;

Shader::~Shader()
{
    delete vertexShaderSource;
    delete fragmentShaderSource;
    delete program;
}

Shader::Shader() {
    program = new QOpenGLShaderProgram(nullptr);
    program->create(); // worked without it, created at object creation?

    if (! program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderSource)
     || ! program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderSource))
    {
        exit(EXIT_FAILURE);
    }

    program->link();

    Handles::aPos = program->attributeLocation("aPos");
    Handles::aCol = program->attributeLocation("aCol");
    Handles::uMatrix = program->uniformLocation("uMatrix");
}

void Shader::setUniforms() {
    Shader::currentShader->program->setUniformValue(Handles::uMatrix, Matrices::mvpMatrix);
}
