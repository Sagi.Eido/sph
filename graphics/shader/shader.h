#pragma once
//#ifndef SHADER_H
//#define SHADER_H

#include "util/files.h"

#include <QOpenGLShaderProgram>

class Shader {
public:
    static Shader *currentShader;

    const char *vertexShaderSource   = Files::VERTEX_SHADER_FILE;
    const char *fragmentShaderSource = Files::FRAGMENT_SHADER_FILE;
    QOpenGLShaderProgram *program;

    ~Shader();
    Shader();

    void bind();
    void release();
    void setUniforms();
};

//#endif // SHADER_H
