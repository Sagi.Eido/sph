#pragma once
//#ifndef PAINTABLE_H
//#define PAINTABLE_H

class Form;

enum ShapeType : unsigned char;

class Paintable
{
public:
    Form *currentForm;

    Paintable();
    virtual ~Paintable() {}

    void linkView(ShapeType formShapeType);
    void recolor(float color[]);

    virtual void paint();

    virtual void setModelMatrix() {}
    virtual void createView() {}

};

//#endif // PAINTABLE_H
