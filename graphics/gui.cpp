#include "gui.h"

#include "object/particle.h"
#include "util/debugger.h"
#include "util/logger.h"
#include "util/settings.h"
#include "util/state.h"
#include "window/simulation_window.h"

#include <QApplication>
#include <QBitmap>
#include <QDesktopWidget>
#include <QPainter>
#include <QPushButton>
#include <QScreen>
#include <QSlider>
#include <QTimer>
#include <QHBoxLayout>
#include <QVBoxLayout>

const char *GUI::mainMenuGameButtonText = "Game Mode";
const char *GUI::mainMenuSimulationButtonText = "Simulation Mode";
const char *GUI::simulationMenuStartButtonText = "Start simulation";

QFont *GUI::font = new QFont;
QLabel *GUI::stateLabel = nullptr;
QLabel *GUI::helpLabel = nullptr;
QLabel *GUI::gameHelpLabel = nullptr;
QLabel *GUI::obstacleLabel = nullptr;
QLayout *GUI::menuLayout = nullptr;
QLayout *GUI::simulationLayout = nullptr;
QPushButton *GUI::mainMenuGameButton = nullptr;
QPushButton *GUI::mainMenuSimulationButton = nullptr;
QPushButton *GUI::simulationMenuStartButton = nullptr;
DoubleLabeledSlider *GUI::gravitySlider = nullptr;
IntegerLabeledSlider *GUI::xParticleSlider = nullptr;
IntegerLabeledSlider *GUI::yParticleSlider = nullptr;
IntegerLabeledSlider *GUI::zParticleSlider = nullptr;
std::vector<QPushButton*> *GUI::mainMenuButtons = new std::vector<QPushButton*>;
std::vector<QLabel*> *GUI::simulationLabels = new std::vector<QLabel*>;
std::vector<LabeledSlider*> *GUI::simulationMenuLabeledSliders = new std::vector<LabeledSlider*>;
std::vector<QPushButton*> *GUI::gameMenuButtons = new std::vector<QPushButton*>;
std::vector<QLabel*> *GUI::gameLabels = new std::vector<QLabel*>;

class LabeledSlider {
private:
    int minimum;
    int maximum;

protected:
    std::string name;
    void *attendedVariable;
    QSlider *qSlider;
    QLabel *qSliderLabel;
    QLayout *destinationLayout;

public:
    LabeledSlider(
        std::string name,
        void *attendedVariable,
        int minimum, int maximum,
        QLayout *destinationLayout,
        std::vector<LabeledSlider*> *labeledSlidersContainer)
    {
        this->name = name;
        this->attendedVariable = attendedVariable;
        this->minimum = minimum;
        this->maximum = maximum;
        this->destinationLayout = destinationLayout;
        labeledSlidersContainer->push_back(this);

        qSlider = new QSlider(Qt::Horizontal);
        qSlider->setMaximumWidth(250);
        qSlider->setTickInterval(1);
        qSlider->setMinimum(minimum);
        qSlider->setMaximum(maximum);
        qSlider->setStyleSheet(
            QString("QSlider::handle:horizontal {background-color: #00ff00;}"));
    }

    friend bool operator==(const LabeledSlider &slider, const std::string name)
    {
        return slider.name == name;
    }

    virtual void initialize() = 0;
    virtual void setValue() = 0;
    virtual void displayValue(int value) = 0;
    virtual QString getNumberString() = 0;

    void hide()
    {
        qSlider->hide();
        qSliderLabel->hide();
    }

    void show()
    {
        qSlider->show();
        qSliderLabel->show();
    }

    QSlider* getQSlider()
    {
        return qSlider;
    }
};

class IntegerLabeledSlider : public LabeledSlider
{
public:
    IntegerLabeledSlider(
        std::string name,
        int *attendedVariable,
        int minimum, int maximum,
        QLayout *destinationLayout,
        std::vector<LabeledSlider*> *labeledSlidersContainer)
    : LabeledSlider(
        name, attendedVariable, minimum, maximum,
        destinationLayout, labeledSlidersContainer) {};
    void initialize() override
    {
        setValue();

        qSliderLabel = GUI::addAndGetLabel(SimulationWindow::simWidget, destinationLayout);
        qSliderLabel->setText(
            QString::fromStdString(name) + QString(": ") + getNumberString());

        SimulationWindow::simWidget->connect(
            qSlider,
            &QSlider::valueChanged,
            SimulationWindow::simWidget,
            [=] (int value) {
                displayValue(value);
                qSliderLabel->setText(
                    QString::fromStdString(name) + QString(": ")
                    + getNumberString());
            });

        destinationLayout->addWidget(qSliderLabel);
        destinationLayout->addWidget(qSlider);
    }
protected:
    void setValue() override
    {
        qSlider->setValue(*((int*) attendedVariable));
    }
    void displayValue(int value) override
    {
        *((int*) attendedVariable) = value;
    }
    QString getNumberString() override
    {
        return QString::number(*((int*) attendedVariable));
    }
};

class DoubleLabeledSlider : public LabeledSlider
{
public:
    DoubleLabeledSlider(
        std::string name,
        double *attendedVariable,
        int minimum, int maximum,
        QLayout *destinationLayout,
        std::vector<LabeledSlider*> *labeledSlidersContainer)
    : LabeledSlider(
        name, attendedVariable, minimum, maximum,
        destinationLayout, labeledSlidersContainer) {};
    void initialize() override
    {
        setValue();

        qSliderLabel = GUI::addAndGetLabel(SimulationWindow::simWidget, destinationLayout);
        qSliderLabel->setText(
            QString::fromStdString(name) + QString(": ") + getNumberString());

        SimulationWindow::simWidget->connect(
            qSlider,
            &QSlider::valueChanged,
            SimulationWindow::simWidget,
            [=] (int value) {
                displayValue(value);
                qSliderLabel->setText(
                    QString::fromStdString(name) + QString(": ")
                    + getNumberString());
            });

        destinationLayout->addWidget(qSliderLabel);
        destinationLayout->addWidget(qSlider);
    }
protected:
    void setValue() override
    {
        qSlider->setValue(*((double*) attendedVariable) * 10);
    }
    void displayValue(int value) override
    {
        *((double*) attendedVariable) = 0.1 * value;
    }
    QString getNumberString() override
    {
        return QString::number(*((double*) attendedVariable));
    }
};

void GUI::showHideLabel(QLabel *label)
{
    label->isVisible() ? label->hide() : label->show();
}

void GUI::changeLayout(QWidget *widget, QLayout *newLayout)
{
    QLayout *oldLayout = widget->layout();
    oldLayout->removeWidget(widget);
    delete oldLayout;
    widget->setLayout(newLayout);
}

QPushButton* GUI::addAndGetButton(const char *text, QLayout *destinationLayout)
{
    QPushButton *button = new QPushButton;
    button->setFont(*font);
    button->setText(text);
    button->setFixedWidth(Settings::WINDOW_WIDTH / 5);
    button->setFixedHeight(Settings::WINDOW_HEIGHT / 10);
    button->setStyleSheet("border: 1px solid #00ff00;");
    destinationLayout->addWidget(button);
    return button;
}

QLabel* GUI::addAndGetLabel(QWidget *parent, QLayout *destinationLayout)
{
    QLabel *label = new QLabel(parent);
    label->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    label->setAttribute(Qt::WA_NativeWindow);
    label->setFont(*font);

    destinationLayout->addWidget(label);
    return label;
}

void GUI::prepareGUI()
{
    prepareWindowAndWidget();

    prepareMenuLayout();
    SimulationWindow::simWidget->setLayout(menuLayout);
    SimulationWindow::simWidget->setFocus();

    prepareSimulationLayout();
    SimulationWindow::simWindow->hide();

    addMainMenu();

    Interaction::centerCursor();
}

void GUI::prepareSimulationLayout()
{
    simulationLayout = new QHBoxLayout;
    simulationLayout->setAlignment(Qt::AlignRight | Qt::AlignTop);
    simulationLayout->setMargin(30);
}

void GUI::prepareMenuLayout()
{
    menuLayout = new QVBoxLayout;
    menuLayout->setAlignment(Qt::AlignCenter);
    menuLayout->setMargin(30);
}

void GUI::prepareWindowAndWidget()
{
    Settings::WINDOW_HEIGHT
        = QGuiApplication::screenAt(QCursor::pos())->geometry().height() * 4/5;
    Settings::WINDOW_WIDTH = Settings::WINDOW_HEIGHT * 4/3;
    QSize defaultSize = QSize(Settings::WINDOW_WIDTH, Settings::WINDOW_HEIGHT);
    SimulationWindow::simWidget
        = QWidget::createWindowContainer(SimulationWindow::simWindow);
    SimulationWindow::simWidget->setFixedSize(
        Settings::WINDOW_WIDTH, Settings::WINDOW_HEIGHT);

    setFullScreen(Settings::FULLSCREEN);

    const char *styleSheetString =
        "* { background-color: rgba(0, 0, 0, 255); color: green; } \
         QLabel { padding: 5px; }";
    SimulationWindow::simWidget->setStyleSheet(styleSheetString);

    font->setPointSize(Settings::WINDOW_HEIGHT / 70);
}

void GUI::addMainMenu()
{
    mainMenuGameButton = addAndGetButton(mainMenuGameButtonText, menuLayout);
    mainMenuSimulationButton = addAndGetButton(mainMenuSimulationButtonText, menuLayout);

    SimulationWindow::simWidget->connect(
        mainMenuSimulationButton,
        &QPushButton::released,
        SimulationWindow::simWidget,
        [&] {
            mainMenuGameButton->hide();
            mainMenuSimulationButton->hide();
            addSimulationMenu();
        });

    SimulationWindow::simWidget->connect(
        mainMenuGameButton,
        &QPushButton::released,
        SimulationWindow::simWidget,
        [&] {
            mainMenuGameButton->hide();
            mainMenuSimulationButton->hide();
            addGameMenu();
        });
}

void GUI::addGameMenu()
{
    addGameMenuButtons();

    Interaction::centerCursor();
    SimulationWindow::simWidget->setFocus();
}

void GUI::addSimulationMenu()
{
    addSimulationMenuLabels();
    addSimulationMenuSliders();
    addSimulationMenuStartButton();

    Interaction::centerCursor();
    SimulationWindow::simWidget->setFocus();
}

void GUI::addGameMenuButtons()
{
    QPushButton *level_1 = addAndGetButton("Level 1\n\nObjective of the game is to place the obstacles in such a way, so that it becomes possible to get the water to the pointed destination.\nThough you can push it a little, there's mostly no returning once the water spoils!", menuLayout);
    level_1->setMaximumWidth(1000);
//    QPushButton *level_2 = addAndGetButton("Level 2", menuLayout);
//    QPushButton *level_3 = addAndGetButton("Level 3", menuLayout);
//    QPushButton *level_4 = addAndGetButton("Level 4", menuLayout);
//    QPushButton *level_5 = addAndGetButton("Level 5", menuLayout);
    gameMenuButtons->push_back(level_1);
//    gameMenuButtons->push_back(level_2);
//    gameMenuButtons->push_back(level_3);
//    gameMenuButtons->push_back(level_4);
//    gameMenuButtons->push_back(level_5);

    SimulationWindow::simWidget->connect(
        level_1,
        &QPushButton::released,
        SimulationWindow::simWidget,
        [&] {
            gameStart();
        });

//    SimulationWindow::simWidget->connect(
//        level_2,
//        &QPushButton::released,
//        SimulationWindow::simWidget,
//        [&] {
//            gameStart();
//        });

//    SimulationWindow::simWidget->connect(
//        level_3,
//        &QPushButton::released,
//        SimulationWindow::simWidget,
//        [&] {
//            gameStart();
//        });

//    SimulationWindow::simWidget->connect(
//        level_4,
//        &QPushButton::released,
//        SimulationWindow::simWidget,
//        [&] {
//            gameStart();
//        });

//    SimulationWindow::simWidget->connect(
//        level_5,
//        &QPushButton::released,
//        SimulationWindow::simWidget,
//        [&] {
//            gameStart();
//        });
}

void GUI::gameStart()
{
    for_each(
        gameMenuButtons->begin(), gameMenuButtons->end(),
        [](QPushButton *button) { button->hide(); });
    changeLayout(SimulationWindow::simWidget, simulationLayout);
    SimulationWindow::simWindow->show();
    addSimulationMenuLabels();
    stateLabel->hide();

    Settings::NO_PRINTOUT = true;
    Logger::enableOutput(false);

    Settings::MAP_SETUP = MAP_CODED_2D_GAME;
    State::OBSTACLE_VERTICES_LEFT_TO_BUILD = 8;
    State::USER_INSIDE_GAME_BUILDING_OBSTACLES = true;
    State::USER_INSIDE_MENU = false;
    State::USER_INSIDE_GAME = true;
    State::CAMERA_ROTATION_LOCKED = true;
    State::CAMERA_POSITION_LOCKED = true;

    addGameHelpLabel();
    showHideLabel(gameHelpLabel);
    helpLabel = gameHelpLabel;

    obstacleLabel = addAndGetLabel(SimulationWindow::simWidget, simulationLayout);
    updateLabels();

    Interaction::centerCursor();
    SimulationWindow::simWidget->setFocus();
    SimulationWindow::simulationTimer->start(1000 / Settings::REQUESTED_FRAMERATE);

    Interaction::playBloop();

    // void GLWindow::render(const std::function <void()>& render)
    // SimulationWindow::simWindow->prepareScene();
    // scene not prepared here yet
    // that's why R key button will cause a crash?
//    QKeyEvent *e1 = new QKeyEvent(QEvent::KeyPress, Qt::Key_R, Qt::NoModifier, "r");
//    QKeyEvent *e2 = new QKeyEvent(QEvent::KeyRelease, Qt::Key_R, Qt::NoModifier, "r");
//    qApp->postEvent(SimulationWindow::simWidget, e1);
//    qApp->postEvent(SimulationWindow::simWidget, e2);
//    QCoreApplication::processEvents(QEventLoop::AllEvents);
//    keybd_event(0x52, 0, 0, 0);
//    keybd_event(0x52, 0, KEYEVENTF_KEYUP, 0);
}

void GUI::addSimulationMenuStartButton()
{
    simulationMenuStartButton = addAndGetButton(simulationMenuStartButtonText, menuLayout);
    SimulationWindow::simWidget->connect(
        simulationMenuStartButton,
        &QPushButton::released,
        SimulationWindow::simWidget,
        [&] {
            simulationMenuStartButton->hide();
            for_each(
                simulationMenuLabeledSliders->begin(), simulationMenuLabeledSliders->end(),
                [](LabeledSlider *slider) { slider->hide(); });

            changeLayout(SimulationWindow::simWidget, simulationLayout);
            SimulationWindow::simWindow->show();
            for_each(
                simulationLabels->begin(), simulationLabels->end(),
                [](QLabel *label) { label->show(); });

            State::USER_INSIDE_MENU = false;

            Interaction::centerCursor();
            SimulationWindow::simWidget->setFocus();
            SimulationWindow::simulationTimer->start(1000 / Settings::REQUESTED_FRAMERATE);
        });
}

void GUI::addSimulationMenuLabels()
{
    addHelpLabel();

    stateLabel = addAndGetLabel(SimulationWindow::simWidget, simulationLayout);
    updateLabels();
    simulationLabels->push_back(stateLabel);
}

void GUI::addHelpLabel()
{
    helpLabel = addAndGetLabel(SimulationWindow::simWidget, simulationLayout);

    QString labelText;
    labelText += "WSAD / arrows\tChange camera position";
    labelText += "\nMouse cursor\tChange camera rotation";
    labelText += "\nSpace\t\t(Re)start/pause simulation";
    labelText += "\nLeft CTRL\tHold to lock camera";
    labelText += "\nLeft Mouse Button\tWhilst holding Left CTRL, press, hold and drag to push particles around";
    labelText += "\nT\t\tTurn on/off console printout";
    labelText += "\nR\t\tProgress by one frame when paused";
    labelText += "\nP\t\tTurn off/on CPU parallelization";
    labelText += "\nV\t\tReset camera to initial position and rotation";
    labelText += "\nEscape\t\tQuit";
    labelText += "\nH\t\tHide/show this help";
//    labelText += "\nF\t\tTurn fullscreen display on/off";
//    labelText += "\nC\tchange control mode";
//    labelText += "\nBackspace\treverse time";
//    labelText += "\nShift\tchange camera moving speed";

    helpLabel->setText(labelText);
    simulationLabels->push_back(helpLabel);
    helpLabel->hide();
}

void GUI::addGameHelpLabel()
{
    gameHelpLabel = addAndGetLabel(SimulationWindow::simWidget, simulationLayout);

    QString labelText;
    labelText += "Left Mouse Button\t\tClick to place obstacles by pointing the new line vertices";
    labelText += "\nSpace\t\t\t(Re)start/pause the flow";
    labelText += "\nLeft Mouse Button\t\tPress and move to push particles around. Works after obstacles are set.";
    labelText += "\nEscape\t\t\tQuit";
    labelText += "\nH\t\t\tHide/show this help";

    gameHelpLabel->setText(labelText);
    gameHelpLabel->hide();
}

void GUI::setFullScreen(bool on)
{
    on ? SimulationWindow::simWidget->showFullScreen()
       : SimulationWindow::simWidget->showNormal();
}

void GUI::showHideHelpLabel()
{
    showHideLabel(helpLabel);
}

void GUI::showHideObstacleLabel()
{
    showHideLabel(obstacleLabel);
}

void GUI::showHideStateLabel()
{
    showHideLabel(stateLabel);
}

void GUI::addSimulationMenuSliders()
{
    gravitySlider = new DoubleLabeledSlider(
        "Gravity",      &Settings::FORCE_GRAV_SURFACE, -300, 100, menuLayout, simulationMenuLabeledSliders);
    xParticleSlider = new IntegerLabeledSlider(
        "X Particles", &Settings::X_PARTICLE_COUNT, 1, 200, menuLayout, simulationMenuLabeledSliders);
    yParticleSlider = new IntegerLabeledSlider(
        "Y Particles", &Settings::Y_PARTICLE_COUNT, 1, 200, menuLayout, simulationMenuLabeledSliders);
    zParticleSlider = new IntegerLabeledSlider(
        "Z Particles", &Settings::Z_PARTICLE_COUNT, 1, 200, menuLayout, simulationMenuLabeledSliders);
    gravitySlider->initialize();
    xParticleSlider->initialize();
    yParticleSlider->initialize();
    zParticleSlider->initialize();
}

void GUI::updateLabels()
{
    if (Settings::DISPLAY_STATE_LABEL)
    {
        QString labelText;
        labelText += "Framerate: " + QString::number(SimulationWindow::simWindow->latestFPS);
        labelText += "\nParticle count: " + QString::number(Particle::count);
        labelText += "\nGravitational F: " + QString::number(Settings::FORCE_GRAV_SURFACE);
        labelText += "\nMPI parallelization: " + QString((Settings::PARALLEL_OMP ? "ON" : "OFF"));
        labelText += "\nAvg. neighbours: " + QString::number(Logger::averageNeighbourCount);
        labelText += "\nAvg. density: " + QString::number(Particle::rho_avg);
        labelText += "\nAvg. velocity: " + QString::number(Particle::v_avg_norm);
        labelText += "\nAvg. pressure F: " + QString::number(Particle::F_P_avg_norm);
        labelText += "\nAvg. viscous F: " + QString::number(Particle::F_vsc_avg_norm);
        labelText += "\nAvg. buoyant F: " + QString::number(Particle::F_b_avg_norm);
        labelText += "\nAvg. surf. tens. F: " + QString::number(Particle::F_tns_avg_norm);

        if ((! Interaction::pause && SimulationWindow::frameNumber % 30 == 0)
            || Interaction::key[Qt::Key_R]
            || SimulationWindow::frameNumber == 0)
        {
            stateLabel->setText(labelText);
        }
    }
    else if (stateLabel->isVisible()) stateLabel->hide();

    if (State::USER_INSIDE_GAME)
    {
        obstacleLabel->setText(
            QString("Obstacle line vertices left to place: ")
            + QString::number(State::OBSTACLE_VERTICES_LEFT_TO_BUILD));
    }
}

//    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//    QSurfaceFormat format;
//    format.setSamples(16);
//    format.setSwapInterval(Settings::REQUESTED_FRAMERATE);
//    format.setSwapInterval(0);
//    SimulationWindow::simWindow->setFormat(format);
//    SimulationWindow::simWidget->setAutoFillBackground(false);
//    SimulationWindow::simWidget->setWindowOpacity(0.9);
//    label->setAttribute(Qt::WA_TranslucentBackground);
//    label->setWindowOpacity(0.9);
//    label->setAutoFillBackground(false);
