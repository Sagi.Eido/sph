#pragma once
//#ifndef GUI_H
//#define GUI_H

#include <QWindow>

class QLabel;
class QLayout;
class QPushButton;
class QSlider;
class QWidget;

class LabeledSlider;
class IntegerLabeledSlider;
class DoubleLabeledSlider;

class GUI
{
private:
    static const char *mainMenuGameButtonText;
    static const char *mainMenuSimulationButtonText;
    static const char *simulationMenuStartButtonText;

    static QFont *font;
    static QLabel *stateLabel;
    static QLabel *helpLabel;
    static QLabel *gameHelpLabel;
    static QLabel *obstacleLabel;
    static QLayout *menuLayout;
    static QLayout *simulationLayout;
    static QPushButton *mainMenuGameButton;
    static QPushButton *mainMenuSimulationButton;
    static QPushButton *simulationMenuStartButton;
    static DoubleLabeledSlider *gravitySlider;
    static IntegerLabeledSlider *xParticleSlider;
    static IntegerLabeledSlider *yParticleSlider;
    static IntegerLabeledSlider *zParticleSlider;
    static std::vector<QPushButton*> *mainMenuButtons;
    static std::vector<QLabel*> *simulationLabels;
    static std::vector<LabeledSlider*> *simulationMenuLabeledSliders;
    static std::vector<QPushButton*> *gameMenuButtons;
    static std::vector<QLabel*> *gameLabels;

    static void showHideLabel(QLabel *label);
    static void changeLayout(QWidget *widget, QLayout *newLayout);

public:
    static void prepareGUI();
    static void prepareSimulationLayout();
    static void prepareMenuLayout();
    static void prepareWindowAndWidget();
    static void addGameMenu();
    static void addGameMenuButtons();
    static void addMainMenu();
    static void addSimulationMenu();
    static void addSimulationMenuStartButton();
    static void addSimulationMenuSliders();
    static void addSimulationMenuLabels();
    static void addHelpLabel();
    static void addGameHelpLabel();
    static void gameStart();
    static void setFullScreen(bool on);
    static void showHideHelpLabel();
    static void showHideObstacleLabel();
    static void showHideStateLabel();
    static void updateLabels();
    static QPushButton* addAndGetButton(const char *text, QLayout *destinationLayout);
    static QLabel* addAndGetLabel(QWidget *parent, QLayout *destinationLayout);
};

//#endif // GUI_H
