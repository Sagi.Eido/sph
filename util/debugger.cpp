#include "debugger.h"

#include "object/particle.h"
#include "util/logger.h"
#include "util/settings.h"

#include <iostream>

void Debugger::showText(const char *text)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(QString("Debug Helper"));
    msgBox.setText(QString("Debug Helper: ") + QString(text));
    //msgBox.show();
    msgBox.exec();
}

template void Debugger::showVariable<int>(const char*, int);
template void Debugger::showVariable<float>(const char*, float);
template void Debugger::showVariable<double>(const char*, double);

template<typename T>
void Debugger::showVariable(const char *title, T value)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(QString(title));
    msgBox.setText(QString("%1").arg(value));
    msgBox.exec();
}

void Debugger::verifyInvariants()
{
    if (Particle::count > Settings::PARTICLE_COUNT)
    {
        throwWithMessage("Particle count failure");
    }
}

void Debugger::throwWithMessage(const char *msg)
{
    //qFatal(msg); // would prevent other threads' messages from being printed

    if (Settings::NO_PRINTOUT)
    {
        Logger::enableOutput(true);
    }
    std::cout << msg << std::endl << std::flush;
    throw std::runtime_error(msg);
}

void Debugger::throwWithMessage(QString msg)
{
    throwWithMessage(msg.toStdString().c_str());
}

void Debugger::throwWithMessage(int integer)
{
    throwWithMessage(QString::number(integer));
}

void Debugger::throwWithMessage(double doublePrecisionFloat)
{
    throwWithMessage(QString::number(doublePrecisionFloat));
}
