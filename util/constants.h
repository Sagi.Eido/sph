#pragma once
//#ifndef CONSTANTS_H
//#define CONSTANTS_H

#include <cmath>
#ifdef COMPILER_MSVC
static const double M_PI = 3.14159265358979323846264338327950288;
static const double M_E  = 2.71828182845904523536028747135266249;
#endif
static const float M_PI_F = 3.14159265358979323846264338327950288f;
static const double EPS = 1e-6;

static double degToRad  = 2 * M_PI / 360;
static float  degToRadF = 2 * M_PI_F / 360;
static double radToDeg  = 360.0 / (2 * M_PI);

static double G_const = 6.673 * pow(10, -11);
// electric permittivity of free space (vacuum) [F/m] [farads/m]
static double sigma_0 = 8.8541878176 * pow(10, -12);
// Coulomb force constant [ N * m^2/C^2 ]
static double Coulomb_const = 8.987 * pow(10, 9); //1 / (4 * PI * sigma_0);

//#endif // CONSTANTS_H
