#pragma once
//#ifndef LOGGER_H
//#define LOGGER_H

#include <QImage>

#include <iostream>
#include <vector>

class SimulationWindow;

class Logger
{
private:
    static const QString DIR_DESKTOP;
    static const QString DIR_LOGS;
    static const QString DIR_FRAMES;

    static QString IMG_FILE_TYPE;
    static std::vector<QImage> screens;
    static std::vector<std::string> dumps;

    static QString framePath(unsigned i);

public:
    static std::streambuf *basicStream;
    static double averageNeighbourCount;

    static void init();

    static void takeScreenshot(SimulationWindow *window);
    static void saveVideo();
    static void showLogs();

    static void enableOutput(bool enable);
    static void collectStatistics();
    static void fileDump(std::string, qint64);
    static void fileDump(bool overwrite);
};

//#endif // LOGGER_H
