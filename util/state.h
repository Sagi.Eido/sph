#ifndef STATE_H
#define STATE_H

class State
{
public:
    static bool CAMERA_POSITION_LOCKED;
    static bool CAMERA_ROTATION_LOCKED;
    static bool USER_INSIDE_MENU;
    static bool USER_INSIDE_GAME;
    static bool USER_INSIDE_SIMULATION;
    static bool USER_INSIDE_GAME_BUILDING_OBSTACLES;
    static int OBSTACLE_VERTICES_LEFT_TO_BUILD;
};

#endif // STATE_H
