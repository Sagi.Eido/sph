#include "util/settings.h"

#include <cmath>
#include <iostream>
#include <limits>

#include <QApplication>
#include <QDebug>

#include <omp.h>
#include "mpi.h"

#include "control/interaction.h"
#include "util/enums.h"
#include "util/map.h"

// required - anywhere in the code - to get any printout at cmd.exe, when running on Windows
#pragma comment(linker, "/SUBSYSTEM:CONSOLE")

using namespace std;

////////////////////////////////////////////////////////////////////////////////
//// ENVIRONMENT ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int      Settings::WINDOW_HEIGHT       = 0;
int      Settings::WINDOW_WIDTH        = 0;
bool     Settings::FULLSCREEN          = false;
bool     Settings::NO_PRINTOUT         = false;
bool     Settings::NO_GRAPHICS         = false;
bool     Settings::NO_SCREENS_NO_VIDEO = true; // eliminates inconsistent framerate, when enabled
string   Settings::IMG_FILE_TYPE       = ".png";
unsigned Settings::VIDEO_FILE_FPS      = 120;

////////////////////////////////////////////////////////////////////////////////
//// GRAPHICS //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool     Settings::DISPLAY_STATE_LABEL  = true;
bool     Settings::DOT_OR_SPHERE        = true;
int      Settings::SPHERE_DETAIL        = 23;
bool     Settings::PAINT_VECTORS        = false;
bool     Settings::PAINT_GRID           = true;
uchar    Settings::COLOR_BY             = ColorBy::BOUNDARIES;
double   Settings::VECTOR_LEN_MULT      = 0.1; // 30.

////////////////////////////////////////////////////////////////////////////////
//// CONTROLS //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

uchar  Settings::CONTROL_MODE           = FORCE_DRAG;
double Settings::FORCE_DRAG_COEFFICIENT = DESIRED_REST_DENSITY * 600.;
double Settings::WORLD_ROTATION         = 0.;
float  Settings::MOUSE_SPEED            = 0.3f;
float  Settings::MOVE_SPEED             = 1e-1f;

////////////////////////////////////////////////////////////////////////////////
//// PHYSICS ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

double      Settings::PARTICLE_RADIUS            = 0.3; // 0.3 // 0.15
double      Settings::PARTICLE_INIT_DIST         = 4. * PARTICLE_RADIUS; // 4 // 2.1;
double      Settings::MESH_CELL_DIAMETER         = 2. * PARTICLE_INIT_DIST;
bool        Settings::VARIABLE_SMOOTHING_LENGTH  = false;
double      Settings::SMOOTHING_LENGTH           = 8. * PARTICLE_RADIUS; // smoothing length / kernel support
uchar       Settings::NEIGHBOUR_CHOICE           = GRID;
double      Settings::NEIGHBOUR_RANGE            = SMOOTHING_LENGTH; // std::numeric_limits<double>::infinity();
int         Settings::NEIGHBOUR_RANGE_CELLS      = int(ceil(NEIGHBOUR_RANGE / MESH_CELL_DIAMETER));

KernelSlope Settings::DEFAULT_KERNEL_SLOPE       = QUINTIC_LIU;
KernelSlope Settings::PRESSURE_KERNEL_SLOPE      = SPIKY_DEBRUN;
KernelSlope Settings::VISCOSITY_KERNEL_SLOPE     = DEFAULT_KERNEL_SLOPE;
unsigned    Settings::KERNEL_DIM                 = 0; // TODO

bool        Settings::GRANULAR_OR_LIQUID         = true; // when granular, move one particle manually to eliminate equilibrial state
bool        Settings::RHO_GRAVITY                = false; // TODO

double      Settings::DESIRED_REST_DENSITY       = 1000.;
double      Settings::PARTICLE_MASS              = DESIRED_REST_DENSITY
                                                   * pow(2./3 * SMOOTHING_LENGTH, 3); // pow(1 * ...) // 10 // 0.01
double      Settings::SOUND_SPEED                = 340.; // 1500 m/s in water
double      Settings::STIFFNESS_CONSTANT         = 2150.; // 2150. // 2.15 GPa
double      Settings::VISCOSITY                  = 7.; // 7. // .01 // 10^-6 but large values preffered for simulation
double      Settings::BUOYANCY_DIFFUSION_COEF    = 0.0; // TODO
double      Settings::SURFACE_TENSION_COEF       = 10000; //100000.; // TODO
double      Settings::SURFACE_INDICATOR          = 0.2;

double      Settings::FORCE_GRAV_SURFACE      = -9.81;
double      Settings::FORCE_GRAV_UNIVERSAL_X  = 0; //1000000/0.;
double      Settings::FORCE_DAMPING_WALL      = .0; // [0, 1]
double      Settings::FORCE_DAMPING_WATER     = GRANULAR_OR_LIQUID ? .1 : .1; // [0, 1]

bool        Settings::PARTICLES_COLLIDE       = true;
bool        Settings::COLLIDE_NGHBRS_ONLY     = false; // on CPU only
bool        Settings::COLLIDE_IMMEDIATE       = true;

////////////////////////////////////////////////////////////////////////////////
//// SIMULATION ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool     Settings::VARIABLE_TIME_STEP   = false;
// BACKWARD_EULER serves as a good default,
// SYMPLECTIC_EULER much better for variable time step than BACKWARD_EULER,
//     which will explode at high energy regions,
// VELOCITY_VERLET provides faster simulation than BACKWARD_EULER,
//     but causes explosive instability at high energy regions
//     even without variable time step,
// FORWARD_EULER useless for variable time step.
uchar    Settings::INTEGRATION_SCHEME   = SYMPLECTIC_EULER;
int      Settings::REQUESTED_FRAMERATE  = 50;
double   Settings::TIME_ARROW           = 1.;
//unsigned Settings::ITERATIONS_PER_FRAME = 1;

uchar    Settings::MAP_SETUP            = MAP_DEFINED_BY_USER_AT_MENU;
int      Settings::GHOST_LAYER_GAGE     = 0; // TODO
int      Settings::X_PARTICLE_COUNT     = 20;
int      Settings::Y_PARTICLE_COUNT     = 50;
int      Settings::Z_PARTICLE_COUNT     = 1;
int      Settings::PARTICLE_COUNT       = NULL;
double   Settings::ARENA_DIAMETER       = NULL;
double   Settings::ARENA_DIAMETER_Z     = NULL;

bool     Settings::PARALLEL_CUDA        = true;
bool     Settings::PARALLEL_OMP         = true;
int      Settings::PARALLEL_OMP_THREADS = omp_get_num_procs();

////////////////////////////////////////////////////////////////////////////////
//// FUTURE ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool Settings::PARALLEL_MPI = false;

////////////////////////////////////////////////////////////////////////////////
//// FLOAT EXCEPTIONS //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Generate signal and break on first NaN
#ifdef COMPILER_GPP
    #include <fenv.h>
    // also give linker the "-lm" flag
#elif COMPILER_MSVC
    #include <cfenv>
    #include <float.h>
    #pragma float_control(precise, on)
    #pragma fenv_access(on)
    #pragma float_control(except, on)
#endif

void Settings::enableFloatExceptions()
{
    #ifdef DEBUG_BUILD
        // Generate signal and break on first NaN
        #ifdef COMPILER_GPP
            //feenableexcept(FE_INVALID | FE_OVERFLOW);
            feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);
        #elif COMPILER_MSVC
            #pragma omp parallel if(Settings::PARALLEL_OMP)
            {
                _clearfp();
                unsigned int currentControlWord;
                errno_t error_1 = _controlfp_s(&currentControlWord, 0, 0);
                unsigned int newControlWord = currentControlWord;
                newControlWord &= ~(EM_INVALID | _EM_ZERODIVIDE | _EM_OVERFLOW);
                errno_t error_2
                    = _controlfp_s(&currentControlWord, newControlWord, _MCW_EM);
                if (error_1 != 0 || error_2 != 0) {
                    printf_s("The function _controlfp_s failed!\n");
                }
                //feclearexcept(FE_ALL_EXCEPT);
                //fetestexcept(FE_ALL_EXCEPT);
            }
        #endif
    #endif
}

////////////////////////////////////////////////////////////////////////////////
//// CUDA INITIALIZATION ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef COMPILER_MSVC
    #include <cuda.h>
    #include <cuda_runtime.h> // checkCudaErrors()
    #include <helper_cuda.h> // checkCudaErrors() // helper_cuda.h must go after cuda_runtime.h

    template<typename T>
    unsigned checkWithoutExit(T result, char const *const func,
                              const char *const file, int const line) {
        if (result) {
            fprintf(
                stderr,
                "CUDA error at %s:%d code=%d(%s) \"%s\" \n",
                file,
                line,
                static_cast<unsigned int>(result),
                _cudaGetErrorEnum(result),
                func);
            DEVICE_RESET
            //exit(EXIT_FAILURE);
            return static_cast<unsigned>(result);
        }
        return 0;
    }
    #define checkCudaErrorsWithoutExit(val) \
        checkWithoutExit((val), #val, __FILE__, __LINE__)
#endif

bool Settings::cudaInitialized = false;

void Settings::initializeCUDA()
{
    #ifdef COMPILER_MSVC
        if (PARALLEL_CUDA)
        {
            int deviceCount = 0;
            int cudaDevice = 0;
            char cudaDeviceName[100];
            if (checkCudaErrorsWithoutExit(cuInit(0)) == 0) {
                checkCudaErrors(cuDeviceGetCount(&deviceCount));
                checkCudaErrors(cuDeviceGet(&cudaDevice, 0));
                checkCudaErrors(cuDeviceGetName(cudaDeviceName, 100, cudaDevice));
                qDebug() << "Number of devices: " << deviceCount;
                qDebug() << "Device name:" << cudaDeviceName;
                cudaInitialized = true;
            }
            else
            {
                std::cout << "You probably don't possess proper CUDA-enabled nVidia GPU. Moving on..."
                    << std::endl << std::flush;
            }
        }
    #endif
}

////////////////////////////////////////////////////////////////////////////////
//// MPI INITIALIZATION ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Settings::initializeMPI()
{
    if (PARALLEL_MPI)
    {
        MPI_Init(nullptr, nullptr);
        MPI_Finalize();
    }
}

////////////////////////////////////////////////////////////////////////////////
//// OMP INITIALIZATION ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Settings::initializeOMP()
{
    omp_set_num_threads(Settings::PARALLEL_OMP_THREADS);
}

////////////////////////////////////////////////////////////////////////////////
//// LEGACY ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool   Settings::FORCE_COULOMB        = false;
double Settings::FORCE_FRICTION       = 0.00;
