#include "object/particle.h"
#include "object/segment.h"
#include "physics/grid.h"
#include "physics/vector.h"
#include "util/debugger.h"
#include "util/files.h"
#include "util/map.h"
#include "util/settings.h"
#include "window/simulation_window.h"

#include <QFile>
#include <QTextStream>

#include "util/macros.h"
#include "util/files.h"

std::vector<std::vector<char>> Map::map;
unsigned Map::mapWidth = 0;
unsigned Map::mapHeight = 0;
unsigned Map::obstacleCount = 0;
int Map::particleCount = 0;
int Map::ghostCount = 0;

void Map::reset()
{
    map.clear();
    mapWidth = 0;
    mapHeight = 0;
    obstacleCount = 0;
    particleCount = 0;
    ghostCount = 0;
}

void Map::importUserDefinedObstaclesFromFile()
{
    QString obstaclesFileName = Files::OBSTACLES_FILE;
    QFile wallFile(obstaclesFileName);
    if (wallFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&wallFile);
        while (! in.atEnd())
        {
            QString line = in.readLine();
            if (! line.isEmpty() && ! line.startsWith('#') && ! line.startsWith("//"))
            {
                line.remove(" ");
                QStringList expressions = line.split(',');

                double path[6] = {0};
                for (int j = 0; j < expressions.size(); ++j)
                {
                    QString expression = expressions.at(j);
                    if (expression.contains(QString("AD"))) // ARENA_DIAMETER
                    {
                        QStringList equation = expressions.at(j).split('*');
                        path[j] = Settings::ARENA_DIAMETER
                                * equation.at(1).toDouble();
                    }
                    else if (expression.contains(QString("ADZ"))) // ARENA_DIAMETER_Z
                    {
                        QStringList equation = expressions.at(j).split('*');
                        path[j] = Settings::ARENA_DIAMETER_Z
                                * equation.at(1).toDouble();
                    }
                    else if (expression.contains(QString("PID"))) // PARTICLE_INIT_DIST
                    {
                        QStringList equation = expressions.at(j).split('*');
                        path[j] = Settings::PARTICLE_INIT_DIST
                                * equation.at(1).toDouble();
                    }
                    else
                    {
                        path[j] = expressions.at(j).toDouble();
                    }
                }

                Segment *segment = new Segment(
                    path[0], path[1], path[2], path[3], path[4], path[5]);
                Obstacle::obstacles.push_back(segment);
            }
        }
        wallFile.close();
    }
    else Debugger::throwWithMessage("Could not open Map .txt file");
}

void Map::importMapFromFile(unsigned char mapSetup)
{
    QString mapFileName;
    switch (mapSetup) {
        case MAP_2D_DAM_BREAK : mapFileName = Files::MAP_FILE_2D_DAM_BREAK; break;
        case MAP_2D_DROPLET   : mapFileName = Files::MAP_FILE_2D_DROPLET;   break;
        case MAP_2D_VESSELS   : mapFileName = Files::MAP_FILE_2D_VESSELS;   break;
        case MAP_2D_DAM_FALL  : mapFileName = Files::MAP_FILE_2D_DAM_FALL;  break;
        default : return;
    }

    QFile mapFile(mapFileName);
    if (mapFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&mapFile);
        unsigned i = 0;
        while (! in.atEnd())
        {
            QString line = in.readLine();
            if (static_cast<unsigned>(line.length()) > mapWidth)
            {
                mapWidth = static_cast<unsigned>(line.length());
            }
            ++i;
        }
        mapHeight = i;
        mapFile.close();
    }

    map = std::vector<std::vector<char>>(mapHeight);
    for (unsigned i = 0; i < mapHeight; ++i)
    {
        map[i] = std::vector<char>(mapWidth);
    }

    if (mapFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&mapFile);
        char c;
        unsigned i = 0;
        while (! in.atEnd())
        {
            QString line = in.readLine();
            for (int j = 0; j < line.length(); ++j)
            {
                c = line.at(j).toLatin1();
                if      (c == 'O') ++particleCount;
                else if (c == '|') ++obstacleCount;
                else if (c == 'G') ++ghostCount;
                map[i][static_cast<unsigned>(j)] = c;
            }
            ++i;
        }
        mapFile.close();
    }
}

void Map::generate()
{
    reset();
    importMapFromFile(Settings::MAP_SETUP);

    Settings::ARENA_DIAMETER = 1.1 * Settings::PARTICLE_INIT_DIST
        * (std::max(Settings::X_PARTICLE_COUNT, Settings::Y_PARTICLE_COUNT)
           + Settings::GHOST_LAYER_GAGE);
    if (0 != fmod(Settings::ARENA_DIAMETER, Settings::MESH_CELL_DIAMETER))
    {
        Settings::ARENA_DIAMETER = Settings::MESH_CELL_DIAMETER
            * (1 + floor(Settings::ARENA_DIAMETER / Settings::MESH_CELL_DIAMETER));
    }
    Settings::ARENA_DIAMETER_Z = std::max(
        Settings::Z_PARTICLE_COUNT * Settings::PARTICLE_INIT_DIST,
        Settings::MESH_CELL_DIAMETER);
    if (0 != fmod(Settings::ARENA_DIAMETER_Z, Settings::MESH_CELL_DIAMETER))
    {
        Settings::ARENA_DIAMETER_Z = Settings::MESH_CELL_DIAMETER
            * (1 + floor(Settings::ARENA_DIAMETER_Z / Settings::MESH_CELL_DIAMETER));
    }

    switch (Settings::MAP_SETUP)
    {
        case MAP_DEFINED_BY_USER_AT_MENU :
            Settings::KERNEL_DIM = 0;
            if (Settings::X_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            if (Settings::Y_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            if (Settings::Z_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            Settings::PARTICLE_COUNT
                = Settings::X_PARTICLE_COUNT
                * Settings::Y_PARTICLE_COUNT
                * Settings::Z_PARTICLE_COUNT;
            break;
        case MAP_CODED_2D_GAME :
            Settings::KERNEL_DIM = 2;
            Settings::X_PARTICLE_COUNT = 15;
            Settings::Y_PARTICLE_COUNT = 30;
            Settings::PARTICLE_COUNT
                = Settings::X_PARTICLE_COUNT
                * Settings::Y_PARTICLE_COUNT
                * Settings::Z_PARTICLE_COUNT;
            break;
        case MAP_2D_DROPLET     : [[fallthrough]];
        case MAP_2D_VESSELS     : [[fallthrough]];
        case MAP_2D_DAM_FALL :
            Settings::KERNEL_DIM = 2;
            Settings::PARTICLE_COUNT
                = Settings::X_PARTICLE_COUNT
                * Settings::Y_PARTICLE_COUNT;
            break;
        case MAP_CODED_3D_RAINY_PANE :
            Settings::KERNEL_DIM = 3;
            Settings::PARTICLE_COUNT = 2
                * Settings::X_PARTICLE_COUNT
                * Settings::Y_PARTICLE_COUNT;
            break;

        default:
            Settings::KERNEL_DIM = 0;
            if (Settings::X_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            if (Settings::Y_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            if (Settings::Z_PARTICLE_COUNT > 1) Settings::KERNEL_DIM += 1;
            Settings::PARTICLE_COUNT
                = Settings::X_PARTICLE_COUNT
                * Settings::Y_PARTICLE_COUNT
                * Settings::Z_PARTICLE_COUNT;
            break;
    }

    switch (Settings::MAP_SETUP)
    {
        case MAP_DEFINED_BY_USER_AT_MENU :
            Particle::flows.push_back(std::vector<Particle*>(Settings::PARTICLE_COUNT));
            for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
            {
                Particle::flows[0][i]
                    = new Particle(static_cast<int>(Particle::flows.size()) - 1);
                Particle *p = Particle::flows[0][i];
                p->r = new Vector(
                    - Settings::ARENA_DIAMETER/2
                        + Settings::PARTICLE_INIT_DIST/2
                        + fmod(p->id-1, Settings::X_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST,
                    - Settings::ARENA_DIAMETER/2
                        + Settings::PARTICLE_INIT_DIST/2
                        + fmod((p->id-1)/Settings::X_PARTICLE_COUNT, Settings::Y_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST,
                    -Settings::ARENA_DIAMETER_Z/2
                        + Settings::PARTICLE_INIT_DIST/2
                        + (p->id-1)/(Settings::X_PARTICLE_COUNT * Settings::Y_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST
                );
            }
        break;

        case MAP_CODED_2D_GAME :
            Particle::flows.push_back(std::vector<Particle*>(Settings::PARTICLE_COUNT));
            for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
            {
                Particle::flows[0][i]
                    = new Particle(static_cast<int>(Particle::flows.size()) - 1);
                Particle *p = Particle::flows[0][i];
                p->r = new Vector(
                    - Settings::ARENA_DIAMETER/2
                        + Settings::PARTICLE_INIT_DIST/2
                        + fmod(p->id-1, Settings::X_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST,
                    Settings::ARENA_DIAMETER/2
                        - Settings::PARTICLE_INIT_DIST/2
                        - fmod((p->id-1)/Settings::X_PARTICLE_COUNT, Settings::Y_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST,
                    -Settings::ARENA_DIAMETER_Z/2
                        + Settings::PARTICLE_INIT_DIST/2
                        + (p->id-1)/(Settings::X_PARTICLE_COUNT * Settings::Y_PARTICLE_COUNT)
                        * Settings::PARTICLE_INIT_DIST
                );
            }
        break;


        case MAP_CODED_2D_RANDOM :
            Particle::flows.push_back(std::vector<Particle*>(Settings::PARTICLE_COUNT));
            for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
            {
                Particle::flows[0][i]
                    = new Particle(static_cast<int>(Particle::flows.size()) - 1);
            }

            for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
                Particle *p = Particle::flows[0][i];
                p->r = new Vector(
                    (-Settings::ARENA_DIAMETER/2
                        + Settings::PARTICLE_INIT_DIST/2)
                        + (static_cast<double>(rand()) / RAND_MAX)
                        * Settings::ARENA_DIAMETER,
                    (-Settings::ARENA_DIAMETER/2
                        + Settings::PARTICLE_INIT_DIST/2)
                        + (static_cast<double>(rand()) / RAND_MAX)
                        * Settings::ARENA_DIAMETER,
                    0
                );
            }
        break;

        case MAP_CODED_3D_RAINY_PANE :
            Particle::flows.push_back(std::vector<Particle*>(Settings::PARTICLE_COUNT));
            for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
            {
                Particle::flows[0][i]
                    = new Particle(static_cast<int>(Particle::flows.size()) - 1);
            }

            for (unsigned i = 0; i < Particle::flows[0].size(); ++i) {
                Particle *p = Particle::flows[0][i];
                if (i < static_cast<unsigned>(Settings::PARTICLE_COUNT / 2)) {
                    p->r = new Vector(
                        -Settings::ARENA_DIAMETER/2
                            + Settings::PARTICLE_INIT_DIST/2
                            + fmod(p->id-1, 20) * Settings::PARTICLE_INIT_DIST,
                        -Settings::ARENA_DIAMETER/2
                            + Settings::PARTICLE_INIT_DIST/2
                            + (p->id-1)/20 * Settings::PARTICLE_INIT_DIST,
                        0
                    );
                }
                else {
                    p->r = new Vector(
                        (-Settings::ARENA_DIAMETER/2 + Settings::PARTICLE_INIT_DIST/2)
                            + fmod(p->id-1-1000, 20) * Settings::PARTICLE_INIT_DIST,
                        (-Settings::ARENA_DIAMETER/2 + Settings::PARTICLE_INIT_DIST/2)
                            + (p->id-1-1000)/20 * Settings::PARTICLE_INIT_DIST,
                        Settings::PARTICLE_INIT_DIST
                    );
                }
            }

            Settings::ARENA_DIAMETER_Z = 10 * Settings::Z_PARTICLE_COUNT
                                         * Settings::PARTICLE_INIT_DIST;
        break;

        default : // maps
            Settings::PARTICLE_COUNT = particleCount;

            Settings::ARENA_DIAMETER = std::max(mapWidth, mapHeight) * Settings::PARTICLE_INIT_DIST;

            Particle::flows.push_back(std::vector<Particle*>(particleCount));
            for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
            {
                Particle::flows[0][i]
                    = new Particle(static_cast<int>(Particle::flows.size()) - 1);
            }

            unsigned k = 0;
            // for some reason map.size() is 2x mapHeight // TODO
            for (unsigned i = static_cast<unsigned>(map.size()) - 1; i != 0xFFFFFFFF; --i)
            //for (unsigned i = 0; i < map.size(); ++i)
            {
                for (unsigned j = 0; j < map[i].size(); ++j)
                {
                    if (map[i][j] == 'O')
                    {
                        *Particle::flows[0][k++]->r = Vector(
                            -Settings::ARENA_DIAMETER/2
                                + Settings::PARTICLE_INIT_DIST/2
                                + j * Settings::PARTICLE_INIT_DIST,
                            -Settings::ARENA_DIAMETER/2
                                + Settings::PARTICLE_INIT_DIST/2
                                + (map.size() - 1 - i)
                                * Settings::PARTICLE_INIT_DIST,
                            0);
                    }
                }
            }
        break;
    }

    Partitioning::cellCountX = static_cast<int>(Settings::ARENA_DIAMETER / Settings::MESH_CELL_DIAMETER);
    Partitioning::cellCountY = Partitioning::cellCountX;
    Partitioning::cellCountZ = static_cast<int>(Settings::ARENA_DIAMETER_Z / Settings::MESH_CELL_DIAMETER);

    if (Settings::GHOST_LAYER_GAGE > 0)
    {
        if (Settings::KERNEL_DIM != 2)
            return; ////////////////////////////////////////////////////////////

        #pragma omp parallel for if(Settings::PARALLEL_OMP)
        for (LOOP_TYPE i = 0; i < Particle::flows[0].size(); ++i)
        {
            Particle::flows[0][i]->r->x += Settings::GHOST_LAYER_GAGE
                                         * Settings::PARTICLE_INIT_DIST;
            Particle::flows[0][i]->r->y += Settings::GHOST_LAYER_GAGE
                                         * Settings::PARTICLE_INIT_DIST;
        }

        Particle *ghostParticle;
        for (int i = 0; i < Grid::cellCountX; ++i)
        {
//#pragma omp parallel for if(Settings::PARALLEL_OMP)
            for (int j = 0; j < Grid::cellCountY; ++j)
            {
                if (i < Settings::GHOST_LAYER_GAGE
                 || j < Settings::GHOST_LAYER_GAGE
                 || i >= Grid::cellCountX - Settings::GHOST_LAYER_GAGE
                 || j >= Grid::cellCountY - Settings::GHOST_LAYER_GAGE)
                {
                    ghostParticle = new Particle(0);
                    ghostParticle->isStationary = true;
                    ghostParticle->isGhost = true;
                    ghostParticle->r->x = - Settings::ARENA_DIAMETER/2
                                          + Settings::PARTICLE_INIT_DIST/2
                                          + i * Settings::PARTICLE_INIT_DIST;
                    ghostParticle->r->y = - Settings::ARENA_DIAMETER/2
                                          + Settings::PARTICLE_INIT_DIST/2
                                          + j * Settings::PARTICLE_INIT_DIST;
                    ghostParticle->r->z = 0;
//#pragma omp atomic // critical
                    Particle::flows[0].push_back(ghostParticle);
                    Settings::PARTICLE_COUNT += 1;
                }
            }
        }
        //Settings::ARENA_DIAMETER_Z = 0; // TODO // WHY????????
    }

    if (Settings::CONTROL_MODE == BALL_DRAG) {
        Settings::PARTICLE_COUNT += 1;
        Particle *controlBallParticle;
        controlBallParticle
            = new Particle(static_cast<int>(Particle::flows.size()));
        controlBallParticle->r->z = Settings::PARTICLE_INIT_DIST / 2;
        controlBallParticle->isSolid = true;
        controlBallParticle->radius *= 10;
        //controlBallParticle->m *= 50;
        Particle::flows[0].push_back(controlBallParticle);
    }

    for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
    {
        *Particle::flows[0][i]->r_former = *Particle::flows[0][i]->r;
    }
}
