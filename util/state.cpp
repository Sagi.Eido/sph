#include "state.h"

bool State::CAMERA_POSITION_LOCKED = false;
bool State::CAMERA_ROTATION_LOCKED = false;
bool State::USER_INSIDE_MENU = true;
bool State::USER_INSIDE_GAME = false;
bool State::USER_INSIDE_SIMULATION = false;
bool State::USER_INSIDE_GAME_BUILDING_OBSTACLES = false;
int State::OBSTACLE_VERTICES_LEFT_TO_BUILD = -1;
