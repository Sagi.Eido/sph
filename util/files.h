#ifndef FILES_H
#define FILES_H

class Files
{
public:
    static const char *VERTEX_SHADER_FILE;
    static const char *FRAGMENT_SHADER_FILE;

    static const char *MAP_FILE_2D_DAM_BREAK;
    static const char *MAP_FILE_2D_DROPLET;
    static const char *MAP_FILE_2D_VESSELS;
    static const char *MAP_FILE_2D_DAM_FALL;

    static const char *OBSTACLES_FILE;

    static const char *SOUND_BLOOP_FILE;
};

#endif // FILES_H
