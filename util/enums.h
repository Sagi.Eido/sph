#pragma once
//#ifndef ENUMS_H
//#define ENUMS_H

class Enums
{
public:
    static const char *shapeName[];
};

// update shapeName[] in .cpp file according to changes below!
enum ShapeType : unsigned char {
    NOTHING,
    DOT,
    ARROW,
    TRIANGLE,
    MESH,
    SPHERE_YELLOW, SPHERE_BLUE, SPHERE_RED, SPHERE_GREEN,
    LINE
};

enum IntegrationScheme : unsigned char {
    // I order
    FORWARD_EULER, // explicit method
    SYMPLECTIC_EULER, // semi-implicit
    BACKWARD_EULER, // implicit
    // II order
    MIDPOINT, // RK2
    HALF_STEP,
    VELOCITY_VERLET,
    LEAPFROG, // semi-implicit
    // IV order
    RK4 // Runge-Kutta
};

enum MapSetup : unsigned char {
    // user choice
    MAP_DEFINED_BY_USER_AT_MENU,
    // coded 2D
    MAP_CODED_2D_RANDOM,
    MAP_CODED_2D_GAME,
    // text file map 2D
    MAP_2D_DAM_BREAK,
    MAP_2D_DROPLET,
    MAP_2D_VESSELS,
    MAP_2D_DAM_FALL,
    // coded 3D
    MAP_CODED_3D_RAINY_PANE
};

enum DragMode : unsigned char {
    ONE_DRAG, LOCAL_DRAG, FORCE_DRAG, BALL_DRAG
};

enum ColorBy : unsigned char {
    LAYERS,
    BOUNDARIES,
    DENSITY,
    VELOCITY,
    PRESSURE,
    VISCOSITY,
    TENSION
};

enum KernelSlope : unsigned char {
    // density kernels,
    // also used as default kernels, for other operations such as:
    // evaluating color field
    GAUSSIAN, SUPER_GAUSS, CUBIC_MONAGHAN, QUINTIC_LIU, POLY_6,
    // pressure kernels
    SPIKY_DEBRUN,
    // viscosity kernels
    VISCOSITY_SPECIFIC_KERNEL
};

enum NeighbourChoice : unsigned char {
    ALL, GRID, OCTREE
};

//#endif // ENUMS_H
