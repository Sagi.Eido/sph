#include "util/files.h"

const char* Files::VERTEX_SHADER_FILE   = ":/graphics/shader/vertex_shader.vsh";
const char* Files::FRAGMENT_SHADER_FILE = ":/graphics/shader/fragment_shader.fsh";

const char* Files::MAP_FILE_2D_DAM_BREAK = ":/map/sim_dam_break.txt";
const char* Files::MAP_FILE_2D_DROPLET   = ":/map/sim_droplet.txt";
const char* Files::MAP_FILE_2D_VESSELS   = ":/map/sim_communicating_vessels.txt";
const char* Files::MAP_FILE_2D_DAM_FALL  = ":/map/sim_dam_fall.txt";

const char* Files::OBSTACLES_FILE = ":/map/user_defined_obstacles.txt";

const char* Files::SOUND_BLOOP_FILE = ":/resources/sounds/bloop.mp3";
