#pragma once
//#ifndef SETTINGS_H
//#define SETTINGS_H

#include <string>

enum KernelSlope : unsigned char;

class Settings {
public:

////////////////////////////////////////////////////////////////////////////////
//// ENVIRONMENT ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static int         WINDOW_HEIGHT;
    static int         WINDOW_WIDTH;
    static bool        FULLSCREEN;
    static bool        NO_PRINTOUT;
    static bool        NO_GRAPHICS;
    static bool        NO_SCREENS_NO_VIDEO;
    static std::string IMG_FILE_TYPE;
    static unsigned    VIDEO_FILE_FPS;

////////////////////////////////////////////////////////////////////////////////
//// GRAPHICS //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static bool          DISPLAY_STATE_LABEL;
    static bool          DOT_OR_SPHERE;
    static int           SPHERE_DETAIL;
    static bool          PAINT_VECTORS;
    static bool          PAINT_GRID;
    static unsigned char COLOR_BY;
    static double        VECTOR_LEN_MULT;

////////////////////////////////////////////////////////////////////////////////
//// CONTROLS //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static unsigned char CONTROL_MODE;
    static double        FORCE_DRAG_COEFFICIENT;
    static double        WORLD_ROTATION;
    static float         MOUSE_SPEED;
    static float         MOVE_SPEED;

////////////////////////////////////////////////////////////////////////////////
//// PHYSICS ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static double        PARTICLE_RADIUS;
    static double        PARTICLE_INIT_DIST;
    static double        MESH_CELL_DIAMETER;
    static bool          VARIABLE_SMOOTHING_LENGTH;
    static double        SMOOTHING_LENGTH;
    static unsigned char NEIGHBOUR_CHOICE;
    static double        NEIGHBOUR_RANGE;
    static int           NEIGHBOUR_RANGE_CELLS;

    static KernelSlope   DEFAULT_KERNEL_SLOPE;
    static KernelSlope   PRESSURE_KERNEL_SLOPE;
    static KernelSlope   VISCOSITY_KERNEL_SLOPE;
    static unsigned      KERNEL_DIM;

    static bool          GRANULAR_OR_LIQUID;
    static bool          RHO_GRAVITY;

    static double        DESIRED_REST_DENSITY;
    static double        PARTICLE_MASS;
    static double        SOUND_SPEED;
    static double        STIFFNESS_CONSTANT;
    static double        VISCOSITY;
    static double        BUOYANCY_DIFFUSION_COEF;
    static double        SURFACE_TENSION_COEF;
    static double        SURFACE_INDICATOR;

    static double        FORCE_GRAV_SURFACE;
    static double        FORCE_GRAV_UNIVERSAL_X;
    static double        FORCE_DAMPING_WALL; // 1.0 - ELASTICITY
    static double        FORCE_DAMPING_WATER;

    static bool          PARTICLES_COLLIDE;
    static bool          COLLIDE_NGHBRS_ONLY;
    static bool          COLLIDE_IMMEDIATE;

////////////////////////////////////////////////////////////////////////////////
//// SIMULATION ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static bool          VARIABLE_TIME_STEP;
    static unsigned char INTEGRATION_SCHEME;
    static int           REQUESTED_FRAMERATE;
    static double        TIME_ARROW;

    static unsigned char MAP_SETUP;
    static int           GHOST_LAYER_GAGE;
    static int           X_PARTICLE_COUNT;
    static int           Y_PARTICLE_COUNT;
    static int           Z_PARTICLE_COUNT;
    static int           PARTICLE_COUNT;
    static double        ARENA_DIAMETER;
    static double        ARENA_DIAMETER_Z;

    static bool          PARALLEL_CUDA;
    static bool          PARALLEL_OMP;
    static int           PARALLEL_OMP_THREADS;

////////////////////////////////////////////////////////////////////////////////
//// FUTURE ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static bool PARALLEL_MPI;

////////////////////////////////////////////////////////////////////////////////
//// FLOAT EXCEPTIONS //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static void enableFloatExceptions();

////////////////////////////////////////////////////////////////////////////////
//// CUDA, MPI & OMP INITIALIZATION ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static bool cudaInitialized;
    static void initializeCUDA();
    static void initializeMPI();
    static void initializeOMP();

////////////////////////////////////////////////////////////////////////////////
//// LEGACY ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

    static bool   FORCE_COULOMB;
    static double FORCE_FRICTION;
};

//#endif // SETTINGS_H
