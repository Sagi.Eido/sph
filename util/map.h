#pragma once
//#ifndef MAP_H
//#define MAP_H

#include <vector>

class Map
{
private:
    static std::vector<std::vector<char>> map;
    static unsigned mapWidth, mapHeight;
    static unsigned obstacleCount;
    static int particleCount, ghostCount;

    static void reset();
    static void importMapFromFile(unsigned char mapSetup);

public:
    static void generate();
    static void importUserDefinedObstaclesFromFile();
};

//#endif // MAP_H
