#include "util/timer.h"

#include <cmath>

#include "control/interaction.h"
#include "util/enums.h"
#include "util/macros.h"
#include "util/settings.h"

Timer::Timer()
{
    reset();
}

void Timer::reset()
{
    start = high_resolution_clock::now();
}
INT_64 Timer::now()
{
    return timePointToInt64(high_resolution_clock::now());
}
INT_64 Timer::timePointToInt64(time_point<high_resolution_clock> time)
{
    return duration_cast<nanoseconds>(time - start).count();
}

INT_64 Timer::diff()
{
    return diff(timePointToInt64(start));
}
INT_64 Timer::diff(INT_64 start)
{
    return diff(start, now());
}
INT_64 Timer::diff(INT_64 start, INT_64 stop)
{
    return stop - start;
}

double Timer::diff_ms()
{
    return diff_ms(timePointToInt64(start));
}
double Timer::diff_ms(INT_64 start)
{
    return diff(start)/1000000;
    //return int(round(diff(start)/1000000.));
}
double Timer::diff_ms(INT_64 start, INT_64 stop)
{
    return diff(start, stop)/1000000;
}

double Timer::diff_s()
{
    return diff_s(timePointToInt64(start));
}
double Timer::diff_s(INT_64 start)
{
    return diff_ms(start)/1000;
}

int Timer::FPS(INT_64 start)
{
    return 1000000000LL / diff(start);
}
int Timer::FPS(INT_64 start, INT_64 end)
{
    return 1000000000LL / diff(start, end);
}

double Timer::measureTime(std::string msg, const std::function<void()> &f)
{
    long long int startTime = diff();

    f();

    double elapsedTime = diff_ms(startTime);

    IF_SIMULATION_ONGOING
    std::cout << elapsedTime << "ms <- " << msg << " time." << std::endl;

    return elapsedTime;
}
