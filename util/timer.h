#pragma once
//#ifndef TIMER_H
//#define TIMER_H

#include <chrono>
#ifdef ANDROID_BUILD
#include <cinttypes>
#endif

#include <functional>
#include <iostream>

#ifdef DESKTOP_BUILD
    #define INT_64 __int64
#elif ANDROID_BUILD
    #define INT_64 __int64_t
#endif

using namespace std::chrono;

class Timer
{
private:
    time_point<high_resolution_clock> start;

public:
    Timer();

    void reset();
    double measureTime(std::string msg, const std::function<void()> &f);

    INT_64 timePointToInt64(time_point<high_resolution_clock> time);
    INT_64 now();
    INT_64 diff();
    INT_64 diff(INT_64 start);
    INT_64 diff(INT_64 start, INT_64 stop);
    double diff_ms();
    double diff_ms(INT_64 start);
    double diff_ms(INT_64 start, INT_64 stop);
    double diff_s();
    double diff_s(INT_64 start);
    int FPS(INT_64 start);
    int FPS(INT_64 start, INT_64 end);
};

//#endif // TIMER_H
