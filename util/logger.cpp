#include "util/logger.h"

#include "control/interaction.h"
#include "object/particle.h"
#include "physics/computer.h"
#include "physics/vector.h"
#include "util/macros.h"
#include "util/settings.h"
#include "util/timer.h"
#include "window/simulation_window.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp> // does it include all opencv2/core/*.hpp ?
//#include <opencv2/core/core.hpp>
//#include <opencv2/core/mat.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/videoio.hpp>

#include <QDesktopServices>
#include <QDir>
#include <QPixmap>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QTextStream>

#include <iostream>

std::vector<QImage> Logger::screens;
std::vector<std::string> Logger::dumps;
std::streambuf *Logger::basicStream = nullptr;
double Logger::averageNeighbourCount = -1;

const QString Logger::DIR_DESKTOP = QStandardPaths::locate(
    QStandardPaths::DesktopLocation, "",
    QStandardPaths::LocateDirectory);
const QString Logger::DIR_LOGS = DIR_DESKTOP + "SPH Logs/";
const QString Logger::DIR_FRAMES = DIR_LOGS + "frames/";

QString Logger::IMG_FILE_TYPE;

Timer timer;

void Logger::init()
{
    IMG_FILE_TYPE = QString::fromStdString(Settings::IMG_FILE_TYPE);

    if (! Settings::NO_SCREENS_NO_VIDEO)
    {
        QDir(DIR_FRAMES).removeRecursively();
        QDir().mkpath(DIR_FRAMES);
    }

    if (Settings::NO_PRINTOUT)
    {
        enableOutput(false);
    }
}

void Logger::takeScreenshot(SimulationWindow *w)
{
    if (! Settings::NO_SCREENS_NO_VIDEO && ! Interaction::pause)
    {
        timer.measureTime("Screen taking", [w]() -> void {
            QPixmap pixMap = w->screen()->grabWindow(
                SimulationWindow::simWindow->winId(),
                w->x(), w->y(), w->width(), w->height());
            QImage img = pixMap.toImage();
            img.save(framePath(w->frameNumber));
        });
    }
}

void Logger::saveVideo()
{
    QString path = DIR_FRAMES;
    QDir dir = QDir(path);
    dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
    unsigned fileCount = dir.count();
    for (unsigned i = 0; i < fileCount; ++i)
    {
        screens.push_back(QImage(framePath(i)));
    }

    std::vector<cv::Mat> mats;
    for (unsigned i = 0; i < screens.size(); ++i) {
        // QImage to OpenCV cv::Mat
        cv::Mat tmpMat(
            screens[i].height(), screens[i].width(),
            CV_8UC3,
            screens[i].bits(),
            static_cast<size_t>(screens[i].bytesPerLine()));
        cv::Mat finMat;
        cv::cvtColor(tmpMat, finMat, CV_BGR2RGB);
        finMat = cv::imread(framePath(i).toStdString(), CV_LOAD_IMAGE_COLOR);
        mats.push_back(finMat);
    }

    cv::VideoWriter video = cv::VideoWriter(
        (DIR_LOGS + QString("sphVideo.avi")).toStdString(),
        CV_FOURCC('M', 'J', 'P', 'G'),
        Settings::VIDEO_FILE_FPS,
        cv::Size(screens[0].width(), screens[0].height()),
        true);
    {
        // Eliminates console output on OpenCV cv::error() calls after
        // failed OpenCV assertions.
        cv::redirectError(
            [] (int /*status*/, const char * /*func_name*/, const char *err_msg,
                const char * /*file_name*/, int /*line*/, void * /*userdata*/)
            -> int {
                std::cout << "Failed writing video frame." << std::endl
                    << err_msg << std::endl << std::flush;
                return 0;
            }
        );
        {
            unsigned failedFrames = 0;
            for (unsigned i = 0; i < mats.size(); ++i)
            {
                try
                {
                    video.write(mats[i]);
                }
                catch (...) { ++failedFrames; continue; }
            }
            std::cout << failedFrames << " <- failed frames count." << std::endl << std::flush;
        }
        cv::redirectError(nullptr);
    }
    video.release();
}

void Logger::enableOutput(bool enable)
{
    if (enable)
    {
        std::cout.rdbuf(basicStream);
        Settings::NO_PRINTOUT = false;
    }
    else
    {
        basicStream = std::cout.rdbuf(nullptr);
        Settings::NO_PRINTOUT = true;
    }
}

void Logger::collectStatistics()
{
    Computer::evaluateMaximalDensity();
    Computer::evaluateMaximalVelocity();
    Computer::evaluateMaximalPressure();
    Computer::evaluateMaximalViscosity();
    Computer::evaluateMaximalBuoyancy();
    Computer::evaluateMaximalTension();

    averageNeighbourCount = 0.;
    if (Settings::PARALLEL_CUDA)
    {
        #ifdef COMPILER_MSVC
        if (Particle::neighbours_host != nullptr)
        {
            for (int i = 0; i < Settings::PARTICLE_COUNT; ++i)
            {
                for (int j = 0; j < Settings::PARTICLE_COUNT; ++j)
                {
                    if (Particle::neighbours_host[i * Settings::PARTICLE_COUNT + j])
                    {
                        averageNeighbourCount += 1;
                    }
                }
            }
            averageNeighbourCount /= Settings::PARTICLE_COUNT;
        }
        #endif
    }
    else
    {
        std::vector<Particle*> *neighbours;
        for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
        {
            neighbours = Particle::flows[0][i]->neighbours;
            if (neighbours != nullptr)
            {
                averageNeighbourCount += neighbours->size();
            }
        }
        averageNeighbourCount /= Particle::flows[0].size();
    }

    std::cout
        << Particle::rho_avg << "\t"
        << Particle::v_avg_norm << "\t"
        << Particle::F_P_avg_norm << "\t"
        << Particle::F_vsc_avg_norm << "\t"
        << Particle::F_b_avg_norm << "\t"
        << Particle::F_tns_avg_norm << "\t"
        << averageNeighbourCount << std::endl
        << "rho_avg " << "\t"
        << "vel_avg " << "\t"
        << "F_P_avg" << "\t"
        << "F_vsc_~" << "\t"
        << "F_b_avg" << "\t"
        << "F_tns_~" << "\t"
        << "~nghbrs" << std::endl << std::flush;
}

void Logger::fileDump(std::string logKey, qint64 logValue)
{
    dumps.push_back(logKey);
    dumps.push_back(QString::number(logValue).toStdString());
}

void Logger::fileDump(bool overwrite)
{
    QString baseName = "sphCalls";
    QString extension = ".txt";
    QDir logsDirectory(DIR_LOGS);

    QFile *logDumpFile;
    if (! overwrite)
    {
        int max = -1;
        QStringList nameFilter(baseName + "*" + extension);
        QStringList logDumpFiles = logsDirectory.entryList(nameFilter);
        QRegularExpression re = QRegularExpression("\\D");
        for (QString file : logDumpFiles)
        {
            max = std::max(max, file.replace(re, "").toInt());
        }
        max += 1;

        logDumpFile = new QFile(DIR_LOGS + baseName + QString::number(max) + extension);
    }
    else
    {
        logDumpFile = new QFile(DIR_LOGS + baseName + extension);
    }

    if (logDumpFile->open(QIODevice::WriteOnly))
    {
        QString logText = QString();
        logText.append("id,time\r\n");
        for (auto it = dumps.begin(); it != dumps.end(); ) {
            logText.append(QString::fromStdString(*it));
            ++it;
            logText.append(",");
            logText.append(QString::fromStdString(*it));
            ++it;
            logText.append("\r\n");
        }

        QTextStream stream(logDumpFile);
        stream << logText;
        logDumpFile->close();
    }
}

void Logger::showLogs()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(DIR_LOGS));
}

QString Logger::framePath(unsigned i)
{
    return DIR_FRAMES + QString("frame_%1").arg(i) + IMG_FILE_TYPE;
}
