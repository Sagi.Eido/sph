#include "object/particle.h"

#include "graphics/matrices.h"
#include "physics/kernelizer.h"
#include "physics/partitioning.h"
#include "physics/vector.h"
#include "shape/dot.h"
#include "shape/sphere.h"
#include "util/debugger.h"
#include "util/constants.h"
#include "util/map.h"
#include "util/settings.h"

#include <iostream>
#include <random>

#include <omp.h>
#include "mpi.h"

#include "util/macros.h"

using namespace std;

int Particle::count = 0;
std::vector<std::vector<Particle*>> Particle::flows;
double Particle::rho_max = 0;
double Particle::v_max_norm = 0;
double Particle::F_P_max_norm = 0;
double Particle::F_vsc_max_norm = 0;
double Particle::F_b_max_norm = 0;
double Particle::F_tns_max_norm = 0;
double Particle::rho_avg = 0;
double Particle::v_avg_norm = 0;
double Particle::F_P_avg_norm = 0;
double Particle::F_vsc_avg_norm = 0;
double Particle::F_b_avg_norm = 0;
double Particle::F_tns_avg_norm = 0;

// for CUDA // pinned host memory
double *Particle::rx_host = nullptr;
double *Particle::ry_host = nullptr;
double *Particle::rz_host = nullptr;
double *Particle::vx_host = nullptr;
double *Particle::vy_host = nullptr;
double *Particle::vz_host = nullptr;
double *Particle::m_host = nullptr;
bool *Particle::is_stationary_host = nullptr;

double *Particle::rx_device = nullptr;
double *Particle::ry_device = nullptr;
double *Particle::rz_device = nullptr;
double *Particle::vx_device = nullptr;
double *Particle::vy_device = nullptr;
double *Particle::vz_device = nullptr;
double *Particle::m_device = nullptr;
bool *Particle::is_stationary_device = nullptr;

bool *Particle::neighbours_host = nullptr;
bool *Particle::neighbours_device = nullptr;
// for CUDA

Particle::~Particle()
{
    delete r;
    delete v;
    delete r_former;
    delete v_former;
    delete F_former;
    delete dr;
    delete dv;
    delete inwardSurfaceNormal;
    delete F;
    delete F_P;
    delete F_vsc;
    delete F_b;
    delete F_tns;
    delete displacement;
    delete tilt;
    //delete cube;
    delete neighbours;
}

Particle::Particle(int parentFlow)
    : colorDefault {0.5, 0, 0},
      colorBoundary {0.2f, 0.2f, 0.2f},
      colorOverpressured {0.7f, 0, 0},
      colorGreen {0, 0.7f, 0}
{
    this->parentFlow = parentFlow;

    id = ++count;
    neighbours = nullptr;
    cell = std::vector<int>{ std::numeric_limits<int>::max(),
                             std::numeric_limits<int>::max(),
                             std::numeric_limits<int>::max() };
    cube = nullptr;
    isStationary = false;
    isGhost = false;
    isPhantom = false;
    isSolid = false;
    boundary = false;
    overpressured = false;
    m = Settings::PARTICLE_MASS;
    rho = Settings::DESIRED_REST_DENSITY;
    charge = Settings::FORCE_COULOMB;
    viscosity = Settings::VISCOSITY;
    radius = Settings::PARTICLE_RADIUS;
    P = 0.;
    smoothingLength = Settings::SMOOTHING_LENGTH;
    dt_left = new double(1. / Settings::REQUESTED_FRAMERATE); // TODO
    dr = new Vector();
    dv = new Vector();
    F = new Vector();
    r = new Vector();
    v = new Vector();
    inwardSurfaceNormal = new Vector();
    F_P = new Vector();
    F_vsc = new Vector();
    F_b = new Vector();
    F_tns = new Vector();
    F_ext = new Vector(); // external forces

    r_former = new Vector(*r);
    v_former = new Vector(*v);
    F_former = new Vector(*F);
    displacement = new Vector();
    tilt = new Vector();

    linkView(SPHERE_YELLOW);
    formDefault = currentForm;
    linkView(SPHERE_BLUE);
    formBoundary = currentForm;
    linkView(SPHERE_RED);
    formOverpressured = currentForm;
    linkView(SPHERE_GREEN);
    formGreen = currentForm;
}

Particle::Particle(const Particle &p)
{
    Q_UNUSED(p)
    Debugger::throwWithMessage("Particle copy constructor called");
}

Particle& Particle::operator=(const Particle &p)
{
    Q_UNUSED(p)
    Debugger::throwWithMessage("Particle copy assignment operator used");
}

//bool Particle::operator==(const Particle &p) const {
//    return this == &p;
//}
//bool Particle::operator!=(const Particle &p) const {
//    return ! (*this == p);
//}

std::vector<Particle*>& Particle::getParentFlow()
{
    return Particle::flows[parentFlow];
}
Particle* Particle::getParentFlowParticle(int i) const
{
    return Particle::flows[parentFlow][i];
}

void Particle::createView()
{
    if (Settings::DOT_OR_SPHERE) {
        formDefault       = Sphere(1.0, colorDefault, SPHERE_YELLOW).form;
        formBoundary      = Sphere(1.0, colorBoundary, SPHERE_BLUE).form;
        formOverpressured = Sphere(1.0, colorOverpressured, SPHERE_RED).form;
        formGreen         = Sphere(1.0, colorGreen, SPHERE_GREEN).form;
        if      (boundary)      currentForm = formBoundary;
        else if (overpressured) currentForm = formOverpressured;
        else                    currentForm = formDefault;
    }
    else
    {
        Dot dot(colorDefault);
        this->currentForm = dot.form;
    }
}

void Particle::setModelMatrix()
{
    Matrices::modelMatrix.setToIdentity();
    Matrices::modelMatrix.translate(QVector3D(
        static_cast<float>(r->x),
        static_cast<float>(r->y),
        static_cast<float>(r->z)));
    Matrices::modelMatrix.scale(QVector3D(
        static_cast<float>(radius),
        static_cast<float>(radius),
        static_cast<float>(radius)));
    //Matrices::modelMatrix.rotate(100.0f * SimulationWindow::frame / SimulationWindow::screenRefreshRate, 0, 1, 0);

    // x y z // r theta phi // radius inclination azimuth // yaw roll pitch
    Vector dir = v->normalized();
    static Vector up = Vector(0, 1, 0);
    double angle = - acos(dir.dot(up)) * radToDeg;
    if (fabs(angle) > 0.5)
    {
        Vector axis = (dir * up).normalized();
        // rotate towards velocity direction vector
        Matrices::modelMatrix.rotate(
            static_cast<float>(angle),
            QVector3D(
                static_cast<float>(axis.x),
                static_cast<float>(axis.y),
                static_cast<float>(axis.z)));
    }
}

void Particle::paint()
{
    setModelMatrix();

    switch (Settings::COLOR_BY) {
        case LAYERS             : paintLayers();      break;
        case BOUNDARIES         : paintBoundaries();  break;
        case DENSITY            : paintDensities();   break;
        case PRESSURE           : paintPressures();   break;
        case VELOCITY           : paintVelocities();  break;
        case ColorBy::VISCOSITY : paintViscosities(); break;
        case TENSION            : paintTensions();    break;
        default                 : Debugger::throwWithMessage("Switch failure at paint()!");
    }

    Paintable::paint();

    v->paint();
}

void Particle::paintLayers()
{
    float which = float(id) / float(Settings::PARTICLE_COUNT);
    if      (which <= 0.25f) linkView(SPHERE_BLUE);
    else if (which <= 0.50f) linkView(SPHERE_GREEN);
    else if (which <= 0.75f) linkView(SPHERE_YELLOW);
    else                     linkView(SPHERE_RED);
}
void Particle::paintBoundaries()
{
    if (boundary) linkView(SPHERE_BLUE); // depends on density, check isBoundary()
    else          linkView(SPHERE_YELLOW);
}
void Particle::paintDensities()
{
    if      (boundary)      linkView(SPHERE_BLUE);
    else if (overpressured) linkView(SPHERE_RED);
    else                    linkView(SPHERE_YELLOW);
}
void Particle::paintVelocities()
{
    if      (v->norm() <= v_max_norm * 0.25) linkView(SPHERE_BLUE);
    else if (v->norm() <= v_max_norm * 0.50) linkView(SPHERE_GREEN);
    else if (v->norm() <= v_max_norm * 0.75) linkView(SPHERE_YELLOW);
    else                                     linkView(SPHERE_RED);
}
void Particle::paintPressures()
{
    if      (F_P->norm() <= F_P_max_norm * 0.25) linkView(SPHERE_BLUE);
    else if (F_P->norm() <= F_P_max_norm * 0.50) linkView(SPHERE_GREEN);
    else if (F_P->norm() <= F_P_max_norm * 0.75) linkView(SPHERE_YELLOW);
    else                                         linkView(SPHERE_RED);
}
void Particle::paintViscosities()
{
    if      (F_vsc->norm() <= F_vsc_max_norm * 0.25) linkView(SPHERE_BLUE);
    else if (F_vsc->norm() <= F_vsc_max_norm * 0.50) linkView(SPHERE_GREEN);
    else if (F_vsc->norm() <= F_vsc_max_norm * 0.75) linkView(SPHERE_YELLOW);
    else                                             linkView(SPHERE_RED);
}
void Particle::paintBuyoances()
{

}
void Particle::paintTensions()
{
    if      (F_tns->norm() <= F_tns_max_norm * 0.25) linkView(SPHERE_BLUE);
    else if (F_tns->norm() <= F_tns_max_norm * 0.50) linkView(SPHERE_GREEN);
    else if (F_tns->norm() <= F_tns_max_norm * 0.75) linkView(SPHERE_YELLOW);
    else                                             linkView(SPHERE_RED);
}
