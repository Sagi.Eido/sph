#include "object/segment.h"

#include "graphics/matrices.h"
#include "object/particle.h"
#include "physics/collider.h"
#include "physics/partitioning.h"
#include "shape/line.h"
#include "util/constants.h"
#include "util/settings.h"

#include <QOpenGLFunctions>

using namespace std;

int Segment::wallCount = 0;

Segment::Segment(double x1, double y1, double z1,
           double x2, double y2, double z2)
{
    vector<double> l = vector<double> {x1, y1, z1, x2, y2, z2};
    lines.push_back(l);
    Vector v1 = Vector(l[0], l[1], l[2]);
    Vector v2 = Vector(l[3], l[4], l[5]);
    lineVectors.push_back(vector<Vector> {v1, v2});
    geometry::Line lp = geometry::Line(v1, v2);
    linePaths.push_back(lp);
    geometry::Line lpr = geometry::Line(v2, v1);
    linePathsReversed.push_back(lpr);
    linePathNormals.push_back(lp.normal());
    linePathNormalsReversed.push_back(linePathNormals.back() * -1);

    wallNumber = wallCount++;
    // TODO // dirty quick hack
    linkView(ShapeType(100 + wallNumber));
}

void Segment::createView()
{
    std::vector<float> color = {0.f, 0.1f, 0.f};
    std::vector<float> floatLine;
    for (double x : lines[0])
    {
        floatLine.push_back(static_cast<float>(x));
    }
    Line line(floatLine, color);
    // TODO // dirty quick hack
    line.makeForm(ShapeType(100 + wallNumber));
    this->currentForm = line.form;
}

void Segment::setModelMatrix()
{
    Matrices::modelMatrix.setToIdentity();
}

void Segment::paint()
{
    setModelMatrix();
    Paintable::paint();
}

std::map<std::string, std::variant<double, Vector>>
Segment::collide(Particle *p, bool execute) {
    return Collider::collideParticleWithSegment(p, this, execute);
}
