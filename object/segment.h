#pragma once
//#ifndef SEGMENT_H
//#define SEGMENT_H

class Particle;

#include "graphics/paintable.h"
#include "object/obstacle.h"
#include "object/obstacle.h"
#include "physics/geometry.h"
#include "physics/vector.h"

class Segment : public Obstacle {

    friend class Collider;

private:
    std::vector<std::vector<Vector>> lineVectors = std::vector<std::vector<Vector>>();
    std::vector<geometry::Line> linePaths = std::vector<geometry::Line>();
    std::vector<geometry::Line> linePathsReversed = std::vector<geometry::Line>();
    std::vector<Vector> linePathNormals = std::vector<Vector>();
    std::vector<Vector> linePathNormalsReversed = std::vector<Vector>();

public:
    static int wallCount;

    int wallNumber;
    std::vector<std::vector<double>> lines = std::vector<std::vector<double>>();

    Segment(double x1, double y1, double z1, double x2, double y2, double z2);

    std::map<std::string, std::variant<double, Vector>>
        collide(Particle *p, bool execute) override;

    void createView() override;
    void setModelMatrix() override;
    void paint() override;

};

//#endif // SEGMENT_H
