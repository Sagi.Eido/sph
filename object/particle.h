#pragma once
//#ifndef PARTICLE_H
//#define PARTICLE_H

#include <vector>

class Partitioning;
class Vector;

#include "graphics/paintable.h"

class Particle : public Paintable
{
public:
    static int count;
    static std::vector<std::vector<Particle*>> flows;
    static double v_max_norm, F_P_max_norm, F_vsc_max_norm, F_b_max_norm, F_tns_max_norm;
    static double v_avg_norm, F_P_avg_norm, F_vsc_avg_norm, F_b_avg_norm, F_tns_avg_norm;
    static double rho_avg, rho_max;

    // for CUDA
    static double *rx_host, *ry_host, *rz_host;
    static double *vx_host, *vy_host, *vz_host;
    static double *m_host;
    static bool *is_stationary_host;
    static double *rx_device, *ry_device, *rz_device;
    static double *vx_device, *vy_device, *vz_device;
    static double *m_device;
    static bool *is_stationary_device;

    static bool *neighbours_host;
    static bool *neighbours_device;
    // for CUDA

    int parentFlow;
    int id;
    std::vector<Particle*> *neighbours;
    std::vector<int> cell;
    Partitioning *cube;
    bool isStationary, isGhost, isPhantom, isSolid;
    bool boundary, overpressured;
    double m, radius, rho, charge, viscosity, P;
    double smoothingLength;
    double *dt_left;
    Vector *r, *v, *dr, *dv, *F;
    Vector *r_former, *v_former, *F_former;
    Vector *displacement, *tilt;
    Vector *inwardSurfaceNormal;
    Vector *F_P, *F_vsc, *F_b, *F_tns, *F_ext;
    Form *formDefault = nullptr;
    Form *formBoundary = nullptr;
    Form *formOverpressured = nullptr;
    Form *formGreen = nullptr;
    float colorDefault[3];
    float colorBoundary[3];
    float colorOverpressured[3];
    float colorGreen[3];

    Particle(int parentFlow);
    Particle(const Particle &p); // copy constructor
    ~Particle() override;

    Particle& operator=(const Particle &p); // copy assignment operator

//    virtual bool operator==(const Particle &p) const;
//    virtual bool operator!=(const Particle &p) const;

    std::vector<Particle*>& getParentFlow();
    Particle* getParentFlowParticle(int i) const;
    void paintLayers();
    void paintBoundaries();
    void paintDensities();
    void paintVelocities();
    void paintPressures();
    void paintViscosities();
    void paintBuyoances();
    void paintTensions();

    virtual void createView() override;
    virtual void setModelMatrix() override;
    virtual void paint() override;
};

//#endif // PARTICLE_H
