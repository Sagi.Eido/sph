#pragma once
//#ifndef OBSTACLE_H
//#define OBSTACLE_H

#include "graphics/paintable.h"

#include <map>
#include <iostream>
#include <variant>
#include <vector>

class Particle;
class Vector;

using namespace std;

class Obstacle : public Paintable
{
public:
    static std::vector<Obstacle*> obstacles;

    Obstacle();
    virtual ~Obstacle();

    virtual map<string, variant<double, Vector>>
        collide(Particle *p2, bool execute) = 0;
};

//#endif // OBSTACLE_H
