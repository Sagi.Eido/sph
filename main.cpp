#include <QApplication>
#include <QThread>
#include <QTimer>

#include "graphics/gui.h"
#include "util/logger.h"
#include "util/settings.h"
#include "window/main_window.h"
#include "window/simulation_window.h"

#include <omp.h>

#include <iostream>

// Won't help catching exceptions in spawned threads!
class Application final : public QApplication
{
public:
    Application(int &argc, char **argv) : QApplication(argc, argv) {}
    virtual bool notify(QObject *receiver, QEvent *event) override {
        try
        {
            return QApplication::notify(receiver, event);
        }
        catch (const std::exception &exception)
        {
            qFatal(
                "Error %s sending event %s to object %s (%s)",
                exception.what(),
                typeid(*event).name(),
                qPrintable(receiver->objectName()),
                typeid(*receiver).name());
        }
        catch (...)
        {
            qFatal(
                "Error <unknown> sending event %s to object %s (%s)",
                typeid(*event).name(),
                qPrintable(receiver->objectName()),
                typeid(*receiver).name());
        }
        return false;
    }
};

int main(int argc, char *argv[]) try
{
    Application *app = new Application(argc, argv);

    Logger::init();
    Settings::enableFloatExceptions();
    Settings::initializeOMP();
    Settings::initializeCUDA();
    Settings::initializeMPI();

    SimulationWindow::simWindow = new SimulationWindow();
    GUI::prepareGUI();
    SimulationWindow::mainElapsedTimer->start();

    // QTimer::singleShot(20, SimulationWindow::simWindow, SLOT(renderLater()));
    SimulationWindow::simulationTimer->setTimerType(Qt::PreciseTimer);
    SimulationWindow::simWindow->connect(
        SimulationWindow::simulationTimer,
        SIGNAL(timeout()),
        SimulationWindow::simWindow,
        SLOT(gameStepLater()));

    return app->exec();
}
catch (const std::exception& e)
{
    std::cout << e.what() << std::endl << std::flush;
    throw e;
}
catch (...)
{
    std::cout << "Unknown exception occured." << std::endl << std::flush;
    throw;
}
// Won't help catching exceptions in spawned threads!
