dim = 2
h = 4.8 # smoothing length

################################################################################

cubic <- function(q) {
    if      (dim == 1) C_h <-  1 / (6*h)
    else if (dim == 2) C_h <- 15 / (14*pi*h^2)
    else if (dim == 3) C_h <-  1 / (4*pi*h^3)
	if      (0 <= q && q < 1) W <- C_h * ((2-q)^3 - 4*(1-q)^3)
	else if (1 <= q && q < 2) W <- C_h *  (2-q)^3
	else if (2 <= q)          W <- 0
    W
}
cat("cubic_kernel ")
integrate(cubic, 0, 1)$val + integrate(cubic, 1, 2)$val
png(filename='cubic_kernel_plot.png')
    plot(cubic, 0, 1, col=c('blue'), xlab='q', main='Cubic Spline Kernel Function',
         ylab='W(q)', xlim=c(0, 2), ylim=c(0, cubic(0)))
    plot(cubic, 1, 2, col=c('blue'), add=TRUE)
    legend("bottomleft", legend=c(bquote('dim' ~ '=' ~ .(dim)),
                                  bquote(h == .(h)),
                                  expression('q = distance / h')))
bin <- dev.off()

# cubic_gradient <- function(q) {
#     if      (dim == 1) C_h =  -1 / (6*h^2)
#     else if (dim == 2) C_h = -15 / (7*pi*h^3)
#     else if (dim == 3) C_h =  -3 / (4*pi*h^4)
#     if      (0 <= q && q < 1) C_h * q*(9*q-4) / q
#     else if (1 <= q && q < 2) C_h * (q*(4-3*q)-4) / q
#     else if (2 <= q)          0
# }
# print("cubic_kernel_gradient")
# integrate(cubic_gradient, lower=0, upper=2)
# curve(cubic_gradient, col=c('blue'))
# legend("bottomleft", legend=c(bquote(dim == .(dim)), bquote(h == .(h)), 'q = distance / h'))

################################################################################

quintic <- function(q) {
    if      (dim == 1) C_h <- 1 / (120*h)
    else if (dim == 2) C_h <- 7 / (478*pi*h^2)
    else if (dim == 3) C_h <- 3 / (359*pi*h^3)
	if      (0 <= q && q < 1) W <- C_h * ((3-q)^5 - 6*(2-q)^5 + 15*(1-q)^5)
	else if (1 <= q && q < 2) W <- C_h * ((3-q)^5 - 6*(2-q)^5)
	else if (2 <= q && q < 3) W <- C_h *  (3-q)^5
	else if (3 <= q)          W <- 0
    W
}
cat("quintic_kernel ")
integrate(quintic, 0, 1)$val + integrate(quintic, 1, 2)$val + integrate(quintic, 2, 3)$val
png(filename='quintic_kernel_plot.png')
    plot(quintic, 0, 1, col=c('blue'), main='Quntic Spline Kernel Function',
         xlab='q', ylab='W(q)', xlim=c(0, 3), ylim=c(0, quintic(0)))
    plot(quintic, 1, 2, col=c('blue'), add=TRUE)
    plot(quintic, 2, 3, col=c('blue'), add=TRUE)
    legend("bottomleft", legend=c(bquote('dim' ~ '=' ~ .(dim)),
                                  bquote(h == .(h)),
                                  expression('q = distance / h')))
bin <- dev.off()

# quintic_gradient <- function(q) {
#     if      (dim == 1) C_h = -1 / (120*h^2)
#     else if (dim == 2) C_h = -7 / (239*pi*h^3)
#     else if (dim == 3) C_h = -9 / (359*pi*h^4)
#     if      (0 <= q && q < 1) C_h * (-10*q*(5*q^3 - 12*q^2 + 1))
#     else if (1 <= q && q < 2) C_h * (30*(2-q)^4 - 5*(3-q)^4)
#     else if (2 <= q && q < 3) C_h * (-5 * (3-q)^4)
#     else if (3 <= q)          0
# }
# print("quintic_kernel_gradient")
# integrate(quintic_gradient, lower=0, upper=3)
# curve(q, col=c('blue'))
# legend("bottomleft", legend=c(bquote(dim == .(dim)), bquote(h == .(h)), 'q = distance / h'))

################################################################################

# system('explorer .')
# system('chmod 761 Rplots.pdf')
# system('cmd /c start E:/Dropbox/well/WFAIS/LIC/R/Rplots.pdf')
