#pragma once
//#ifndef SIMULATION_WINDOW_H
//#define SIMULATION_WINDOW_H

#include <QElapsedTimer>
#include <QLabel>
#include <QScreen>

#include "control/interaction.h"
#include "shape/mesh.h"
#include "util/timer.h"
#include "window/gl_window.h"

class SimulationWindow : public GLWindow
{
public:
    static SimulationWindow *simWindow;
    static QWidget *simWidget;
    static QElapsedTimer *mainElapsedTimer;
    static QElapsedTimer *simWindowElapsedTimer;
    static QTimer *simulationTimer;
    static unsigned frameNumber;
    static qint64 latestFrameTime;
    static double longestRenderingTime;
    static double longestComputingTime;

    static shapeSpace::Mesh *mesh;

    double latestFPS;

    SimulationWindow();
    ~SimulationWindow() override;

    void initializeGL() Q_DECL_OVERRIDE;
    void prepareScene();
    void render() Q_DECL_OVERRIDE;
    void renderFrame();
    void calculate(double dt) Q_DECL_OVERRIDE;

    void measureAndUpdateLatestFramerate();

    [[deprecated]] void addObstacles();

private:
    Timer timer;

    void keyPressEvent(QKeyEvent *e) Q_DECL_OVERRIDE { Interaction::keyPress(e); }
    void keyReleaseEvent(QKeyEvent *e) override { Interaction::keyRelease(e); }
    void mouseMoveEvent(QMouseEvent *e) override { Interaction::mouseMove(e); }
    void mousePressEvent(QMouseEvent *e) override { Interaction::mousePress(e); }
    void mouseReleaseEvent(QMouseEvent *e) override { Interaction::mouseRelease(e); }
};

//#endif // SIMULATION_WINDOW_H
