#pragma once
//#ifndef GL_WINDOW_H
//#define GL_WINDOW_H

#include <QOpenGLFunctions>
#include <QOpenGLPaintDevice>
#include <QMainWindow>
#include <QWindow>

class GLWindow :
    public QWindow,
    protected QOpenGLFunctions
{
    Q_OBJECT

private:
    bool updatePending;
    QOpenGLContext *glContext;
    QOpenGLPaintDevice *paintDevice;

protected:
    bool event(QEvent *event) Q_DECL_OVERRIDE;
    void exposeEvent(QExposeEvent *event) Q_DECL_OVERRIDE;
    void render(const std::function <void()>& render);

public slots:
    void gameStepLater();
    void gameStepNow();

public:
    explicit GLWindow(QWindow *parent = nullptr);
    ~GLWindow() override;

    virtual void render(QPainter *painter);
    virtual void render() = 0;
    virtual void initializeGL() = 0;
    virtual void calculate(double dt) = 0;
};

//#endif // GL_WINDOW_H
