#include "window/simulation_window.h"

#include "graphics/form.h"
#include "graphics/gui.h"
#include "graphics/handles.h"
#include "graphics/matrices.h"
#include "graphics/shader/shader.h"
#include "graphics/vertex_array.h"
#include "object/particle.h"
#include "object/segment.h"
#include "physics/collider.h"
#include "physics/computer.h"
#include "physics/grid.h"
#include "physics/octree.h"
#include "util/logger.h"
#include "util/debugger.h"
#include "util/macros.h"
#include "util/map.h"
#include "util/settings.h"
#include "util/timer.h"

#include <QApplication>
#include <QElapsedTimer>
#include <QKeyEvent>
#include <QLayout>
#include <QMouseEvent>
#include <QScreen>
#include <QString>
#include <QThread>
#include <QTimer>

#include <iostream>

#include <omp.h>

SimulationWindow *SimulationWindow::simWindow = nullptr;
QWidget *SimulationWindow::simWidget = nullptr;
unsigned SimulationWindow::frameNumber = 0;
qint64 SimulationWindow::latestFrameTime = 0;
double SimulationWindow::longestRenderingTime = 0.001;
double SimulationWindow::longestComputingTime = 0.005;
QElapsedTimer *SimulationWindow::mainElapsedTimer = new QElapsedTimer();
QElapsedTimer *SimulationWindow::simWindowElapsedTimer = new QElapsedTimer();
QTimer *SimulationWindow::simulationTimer = new QTimer(SimulationWindow::simWindow);
shapeSpace::Mesh *SimulationWindow::mesh = nullptr;

SimulationWindow::SimulationWindow()
{
    timer = Timer();
}

SimulationWindow::~SimulationWindow()
{
    std::cout << "Exiting the program." << std::flush;
}

void SimulationWindow::initializeGL()
{
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    const qreal retinaScale = devicePixelRatio();
    glViewport(
        GLint(0), GLint(0),
        width() * static_cast<GLint>(retinaScale),
        height() * static_cast<GLint>(retinaScale));

    Shader::currentShader = new Shader();
}

void SimulationWindow::prepareScene()
{
    static bool run = false;
    if (!run)
    {
        Map::generate();
        Map::importUserDefinedObstaclesFromFile();
        SimulationWindow::mesh = new shapeSpace::Mesh();

        switch (Settings::NEIGHBOUR_CHOICE)
        {
            case ALL    :                                   break;
            case GRID   : Partitioning::net = new Grid();   break;
            case OCTREE : Partitioning::net = new Octree(); break;
            default     : Debugger::throwWithMessage("No case provided.");
        }

        std::cout
            << "ARENA_DIAMETER:     " << Settings::ARENA_DIAMETER << " = " << Partitioning::cellCountX << "cells * " << Settings::MESH_CELL_DIAMETER << std::endl
            << "ARENA_DIAMETER_Z:   " << Settings::ARENA_DIAMETER_Z << " = " << Partitioning::cellCountZ << "cells * " << Settings::MESH_CELL_DIAMETER << std::endl
            << "NEIGH_RNG_CELLS:    " << Settings::NEIGHBOUR_RANGE_CELLS << " = " << Settings::MESH_CELL_DIAMETER * Settings::NEIGHBOUR_RANGE_CELLS << " + [0, " << Settings::MESH_CELL_DIAMETER / 2 << "]" << std::endl
            << "NEIGHBOUR_RANGE:    " << Settings::NEIGHBOUR_RANGE << std::endl
            << "PARTICLE_INIT_DIST: " << Settings::PARTICLE_INIT_DIST << std::endl << std::flush;

        Collider::initializeCollisions();

        addObstacles();

        Computer::currentComputer->evaluateSPHForces();

        std::cout << Settings::PARTICLE_MASS << " <- Particle mass." << std::endl << std::flush;

        keybd_event(0x52, 0, 0, 0);
        keybd_event(0x52, 0, KEYEVENTF_KEYUP, 0);

        run = true;
    }
}

void SimulationWindow::render()
{
    Logger::fileDump("R1", SimulationWindow::mainElapsedTimer->elapsed());
    GLWindow::render( [&]() {
        renderFrame();
        GUI::updateLabels();

        Logger::takeScreenshot(this);

        IF_SIMULATION_ONGOING {
            std::cout << longestComputingTime << "ms <- Longest computing time." << std::endl;
            std::cout << longestRenderingTime << "ms <- Longest rendering time." << std::endl;
            ++frameNumber;
        }
    });
    Logger::fileDump("R2", SimulationWindow::mainElapsedTimer->elapsed());
}

void SimulationWindow::renderFrame()
{
    if (! Settings::NO_GRAPHICS)
    {
        longestRenderingTime = std::max(
            longestRenderingTime,
            timer.measureTime("Frame rendering", [this]() -> void {
                glClear(GL_COLOR_BUFFER_BIT
                      | GL_DEPTH_BUFFER_BIT
                      | GL_STENCIL_BUFFER_BIT);

                Shader::currentShader->program->bind();
                {
                    Matrices::viewMatrix.setToIdentity();
                    Matrices::viewProjectionMatrix.setToIdentity();
                    Matrices::viewProjectionInverted.setToIdentity();
                    Matrices::setViewMatrix();
                    Matrices::viewProjectionMatrix
                        = Matrices::projectionMatrix * Matrices::viewMatrix;
                    Matrices::viewProjectionInverted
                        = Matrices::viewProjectionMatrix.inverted();

                    mesh->paint();
            //#pragma omp parallel for if(Settings::PARALLEL_OMP)
                    for (unsigned i = 0; i < Particle::flows[0].size(); ++i)
                    {
                        Particle::flows[0][i]->paint();
                    }
            //#pragma omp parallel for if(Settings::PARALLEL_OMP)
                    for (unsigned i = 0; i < Obstacle::obstacles.size(); ++i)
                    {
                        Obstacle::obstacles[i]->paint();
                    }
                }
                Shader::currentShader->program->release();
            })
        );
    }
}

void SimulationWindow::calculate(double dt)
{
    Logger::fileDump("U1", SimulationWindow::mainElapsedTimer->elapsed());
    measureAndUpdateLatestFramerate();
    longestComputingTime = std::max(
        longestComputingTime,
        timer.measureTime(
            "Whole physics computation",
            [&]() -> void {
                Debugger::verifyInvariants();

                #pragma omp parallel if(Settings::PARALLEL_OMP)
                {
                    #pragma omp master
                    IF_SIMULATION_ONGOING
                    std::cout << omp_get_num_threads() << " threads." << std::endl;
                }

                Interaction::interact();
                IF_SIMULATION_ONGOING {
                    // for (unsigned i = 0; i < Settings::ITERATIONS_PER_FRAME; ++i)
                    Computer::currentComputer->computeVectors(dt);
                }
            }
        )
    );
    Logger::fileDump("U2", SimulationWindow::mainElapsedTimer->elapsed());
}

void SimulationWindow::measureAndUpdateLatestFramerate()
{
    latestFrameTime = simWindowElapsedTimer->elapsed();
    latestFPS = round(1000. / latestFrameTime);
    simWindowElapsedTimer->restart();

    IF_SIMULATION_ONGOING
    std::cout
        << latestFrameTime << "ms <- Frame time from QElapsedTimer." << std::endl
        << "~" << latestFPS << " FPS from QElapsedTimer" << std::endl
        << "-- Frame " << frameNumber-1 << " end ---------------------------" << std::endl << std::endl
        << "-- Frame " << frameNumber << " start --------------------------" << std::endl << std::flush;
}

void SimulationWindow::addObstacles() {}
