#include "window/gl_window.h"

#include "graphics/matrices.h"
#include "util/settings.h"
#include "window/simulation_window.h"

#include <QCoreApplication>
#include <QPainter>
#include <QThread>

#include <iostream>

GLWindow::~GLWindow()
{
    delete glContext;
    delete paintDevice;
}

GLWindow::GLWindow(QWindow *parent)
    : QWindow(parent),
      updatePending(false),
      glContext(nullptr),
      paintDevice(nullptr)
{
    //ui->setupUi(this);
    setSurfaceType(QWindow::OpenGLSurface);
}

bool GLWindow::event(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::UpdateRequest:
            updatePending = false;
            gameStepNow();
            return true;
        default:
            return QWindow::event(event);
    }
}

void GLWindow::gameStepLater()
{
    if (! updatePending)
    {
        updatePending = true;
        QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
    }
}

void GLWindow::gameStepNow()
{
    if (! isExposed())
        return;

    if (Settings::VARIABLE_TIME_STEP)
    {
        calculate(Settings::TIME_ARROW * SimulationWindow::latestFrameTime / 1000.);
    }
    else
    {
        calculate(Settings::TIME_ARROW * 1. / Settings::REQUESTED_FRAMERATE);
    }

    render();
}

void GLWindow::exposeEvent(QExposeEvent *event)
{
    Q_UNUSED(event);

//    if (isExposed()) renderNow();
}

void GLWindow::render(QPainter *painter)
{
    Q_UNUSED(painter);

//    if (! paintDevice)
//        paintDevice = new QOpenGLPaintDevice;

//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

//    paintDevice->setSize(size());

//    QPainter painter(paintDevice);
//    render(&painter);
}

void GLWindow::render(const std::function <void()>& render)
{
    bool needsInitialize = false;

    if (! glContext)
    {
        glContext = new QOpenGLContext(this);
        glContext->setFormat(requestedFormat());
        glContext->create();

        needsInitialize = true;
    }

    glContext->makeCurrent(this);

    if (needsInitialize)
    {
        initializeOpenGLFunctions();
        initializeGL();
        SimulationWindow::simWindow->prepareScene();
        Matrices::resetCamera();
    }

    render();

    glContext->swapBuffers(this);
}
