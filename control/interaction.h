#pragma once
//#ifndef INTERACTION_H
//#define INTERACTION_H

#include <QKeyEvent>
#include <QtMultimedia/QMediaPlayer>

class Particle;
class Vector;

class Interaction {
private:
    static Particle *pressedParticle;
    static Vector *lockedPressedParticlePosition;
    static Vector *pressedPoint;
    static QPoint mousePoint;
    static QPoint lastPressedMousePoint;

    static std::string previousAction;
    static int dx, dy;
    static float t;
    static float normalizedX, normalizedY;
    static float previousX, previousY;
    static float previousDistance;

public:
    static std::map<int, bool> key;

    static QMediaPlayer *blooper;

    static bool pause, rewind;
    static float moveSpeed, mouseSpeed;

    static void handleTouchPress(float normalizedX, float normalizedY);
    static void handleTouchDrag(float normalizedX, float normalizedY);
    static void handleTouchDrop();
    static void holdPressedParticle();

    static void keyPress(QKeyEvent *e);
    static void keyRelease(QKeyEvent *e);
    static void mouseMove(QMouseEvent *e);
    static void mousePress(QMouseEvent *e);
    static void mouseRelease(QMouseEvent *e);

    static void interact();
    static void moveUp();
    static void moveDown();
    static void moveLeft();
    static void moveRight();

    static void centerCursor();

    static void playBloop();
    static void playMusic();
};

//#endif // INTERACTION_H
