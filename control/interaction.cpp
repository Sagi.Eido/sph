#include "control/interaction.h"

#include <QApplication>
#include <QKeyEvent>
//#include <QSound>
//#include <QSoundEffect>

#include <typeinfo>
#include <algorithm> // for std::remove()

#include <Windows.h> // for keybd_event on Windows

#include "graphics/gui.h"
#include "graphics/matrices.h"
#include "object/particle.h"
#include "object/segment.h"
#include "physics/computer.h"
#include "physics/geometry.h"
#include "physics/neighbourhood.h"
#include "physics/partitioning.h"
#include "util/files.h"
#include "util/logger.h"
#include "util/macros.h"
#include "util/settings.h"
#include "util/state.h"
#include "window/simulation_window.h"

Particle *Interaction::pressedParticle = nullptr;
Vector *Interaction::lockedPressedParticlePosition = nullptr;
Vector *Interaction::pressedPoint = nullptr;
QPoint Interaction::mousePoint = QPoint();
QPoint Interaction::lastPressedMousePoint;
std::map<int, bool> Interaction::key = std::map<int, bool>();
std::string Interaction::previousAction;
bool Interaction::pause = true;
bool Interaction::rewind = false;
int Interaction::dx = 0;
int Interaction::dy = 0;
float Interaction::t = 1.0f;
float Interaction::normalizedX = 0.;
float Interaction::normalizedY = 0.;
float Interaction::moveSpeed = Settings::MOVE_SPEED;
float Interaction::mouseSpeed = Settings::MOUSE_SPEED;
float Interaction::previousX = 0.;
float Interaction::previousY = 0.;
float Interaction::previousDistance = 0.;
QMediaPlayer *Interaction::blooper = nullptr;
#pragma omp threadprivate(Interaction::blooper)

extern void GPUFrees();
extern void GPUUpdateNeighbour(int i);

void Interaction::handleTouchPress(float normalizedX, float normalizedY)
{
    if (! State::USER_INSIDE_MENU && ! State::USER_INSIDE_GAME_BUILDING_OBSTACLES)
    {
        geometry::Ray ray = normalized2DPointToRay(normalizedX, normalizedY);
        double minProximity = std::numeric_limits<double>::infinity();
        Particle *closest(nullptr);
        Computer::forEachParticleParallel([&](Particle *p) -> void {
            geometry::Sphere particleBoundingSphere
                = geometry::Sphere(*p->r, p->radius);
            double distance
                = pointRayDist(particleBoundingSphere.center, ray);
            if (distance < minProximity)
            {
                minProximity = distance;
                closest = p;
            }
        });
        pressedParticle = closest;
    }
    else if (! State::USER_INSIDE_MENU && State::USER_INSIDE_GAME_BUILDING_OBSTACLES)
    {
        Segment *segment;
        static double x, y, z;
        const int VERTICES_PER_OBSTACLE = 2;

        geometry::Ray ray = normalized2DPointToRay(normalizedX, normalizedY);

        // Define plane parallel to default view plane and crossing center of 2D arena
        static const Vector pointOnPlane(
            0,
            0,
            0);
        static const Vector straightVector(0, 0, 1);
        geometry::Plane plane = geometry::Plane(pointOnPlane, straightVector);

        Vector *pressedPointOnPlane = new Vector(rayPlaneIntersection(ray, plane));

        if (State::OBSTACLE_VERTICES_LEFT_TO_BUILD % VERTICES_PER_OBSTACLE == 0)
        {
            x = pressedPointOnPlane->x;
            y = pressedPointOnPlane->y + Settings::ARENA_DIAMETER + Settings::PARTICLE_INIT_DIST * 11;
            z = pressedPointOnPlane->z - Settings::PARTICLE_INIT_DIST/2;
        }
        else
        {
            segment = new Segment(
                x, y, z,
                pressedPointOnPlane->x,
                pressedPointOnPlane->y + Settings::ARENA_DIAMETER + Settings::PARTICLE_INIT_DIST * 11,
                pressedPointOnPlane->z - Settings::PARTICLE_INIT_DIST/2);
            Obstacle::obstacles.push_back(segment);
            SimulationWindow::simWindow->render();

            std::cout <<
                segment->lines.at(0).at(0) << " " <<
                segment->lines.at(0).at(1) << " " <<
                segment->lines.at(0).at(2) << " " <<
                segment->lines.at(0).at(3) << " " <<
                segment->lines.at(0).at(4) << " " <<
                segment->lines.at(0).at(5) << " " << std::endl;
        }
        State::OBSTACLE_VERTICES_LEFT_TO_BUILD -= 1;

        if (State::OBSTACLE_VERTICES_LEFT_TO_BUILD == 0)
        {
            State::USER_INSIDE_GAME_BUILDING_OBSTACLES = false;
//            State::CAMERA_POSITION_LOCKED = false;
//            State::CAMERA_ROTATION_LOCKED = false;
            GUI::showHideObstacleLabel();
        }
    }
}

void Interaction::handleTouchDrag(float normalizedX, float normalizedY)
{
    if (! State::USER_INSIDE_MENU && ! State::USER_INSIDE_GAME_BUILDING_OBSTACLES)
    {
        geometry::Ray ray = normalized2DPointToRay(normalizedX, normalizedY);
        if (pressedParticle != nullptr)
        {
            // Define plane parallel to default view plane and crossing Particle
            Vector pressedParticlePosition(*pressedParticle->r);
            static Vector straightVector(0, 0, 1);
            geometry::Plane plane
                   = geometry::Plane(pressedParticlePosition, straightVector);

            // Find out where the touched point intersects the
            // aforementioned plane. We'll move the particle along it.
            pressedPoint = new Vector(rayPlaneIntersection(ray, plane));

            if (Settings::CONTROL_MODE == ONE_DRAG
             || Settings::CONTROL_MODE == BALL_DRAG) {
                lockedPressedParticlePosition = new Vector(
                    pressedPoint->x,
                    pressedPoint->y,
                    pressedParticle->r->z);
                holdPressedParticle();
            }
            else if (Settings::CONTROL_MODE == BALL_DRAG) {

            }
            else if (Settings::CONTROL_MODE == FORCE_DRAG) {
                // TODO, in particle, neigh vector can't ever be null, but rather empty
                if (pressedPoint != nullptr)
                {
                    Computer::forEachParticleNonParallel([](Particle *p) -> void {
                        Vector dF_external
                            = pressedPoint->to(*p->r).normalized()
                              * Settings::FORCE_DRAG_COEFFICIENT
                              / pressedPoint->dist(*p->r);
                        //*p->F_external += dF_external;
                        // TODO
                        //    1. walls should work anyway
                        //    2. in how many methods throughout the whole
                        //       program we're not safe against building up
                        //       floating errors!?
                        *p->F_ext += dF_external.correct();
                    });
                }
            }
            else if (Settings::CONTROL_MODE == LOCAL_DRAG) {
                lockedPressedParticlePosition = new Vector(
                    pressedPoint->x,
                    pressedPoint->y,
                    pressedParticle->r->z);
                Particle *p(nullptr);
                #pragma omp parallel for if(Settings::PARALLEL_OMP)
                for (LOOP_TYPE i = 0; i < Particle::flows[0].size(); ++i)
                {
                    p = Particle::flows[0][i];
                    // TODO
                    if (p->cell[0] == pressedParticle->cell[0]
                     && p->cell[1] == pressedParticle->cell[1]
                     && p->cell[2] == pressedParticle->cell[2])
                    {
                        *p->r = *lockedPressedParticlePosition - *p->r;
                    }
                }
            }
        }
    }
}

void Interaction::handleTouchDrop()
{
    if (! State::USER_INSIDE_MENU && ! State::USER_INSIDE_GAME_BUILDING_OBSTACLES)
    {
        centerCursor();

        if (pressedParticle != nullptr)
        {
            *pressedParticle->r_former = *pressedParticle->r;

            // TODO
            Partitioning::net != nullptr ? Partitioning::net->remove(pressedParticle) : void();
            if (! Settings::PARALLEL_CUDA)
            {
                //TODO
                neighbourhood::updateNeighbours(pressedParticle);
                neighbourhood::unNeighbour(pressedParticle);
            }
            #ifdef COMPILER_MSVC
            else
            {
                GPUUpdateNeighbour(pressedParticle->id - 1);
            }
            std::cout << *pressedParticle->r << std::endl << std::flush;
            #endif

            pressedPoint = nullptr;
        }
        pressedParticle = nullptr;
        lockedPressedParticlePosition = nullptr;
    }
}

void Interaction::holdPressedParticle()
{
    if (! State::USER_INSIDE_MENU)
    {
        if (pressedParticle != nullptr && lockedPressedParticlePosition != nullptr)
        {
            *pressedParticle->r = *lockedPressedParticlePosition;
            *pressedParticle->v = Vector();
        }
    }
}

void Interaction::keyPress(QKeyEvent *e)
{
    if (! State::USER_INSIDE_MENU)
    {
        switch (e->key())
        {
            case Qt::Key_R         : break;
            case Qt::Key_Shift     :
                key[Qt::Key_Shift] = true;
                Interaction::moveSpeed /= 10;
            break;
            case Qt::Key_Control :
                key[Qt::Key_Control] = true;
                State::CAMERA_ROTATION_LOCKED = true;
            break;

            default:
                key[e->key()] = true;
            break;
        }
    }
}

void Interaction::keyRelease(QKeyEvent *e)
{
    if (! State::USER_INSIDE_MENU)
    {
        if (! e->isAutoRepeat())
        {
            key[e->key()] = false;
            switch (e->key())
            {
                case Qt::Key_Control :
                    State::CAMERA_ROTATION_LOCKED = false;
                    Interaction::handleTouchDrop();
                    break;
                case Qt::Key_Shift   : Interaction::moveSpeed *= 10;   break;
                case Qt::Key_Space   : Interaction::pause ^= true;     break;
                case Qt::Key_Backspace :
//                    Settings::TIME_ARROW *= -1;
//                    Interaction::rewind ^= true;
                break;
                case Qt::Key_Escape :
                    if (Settings::cudaInitialized) {
                        GPUFrees();
                    }
                    QApplication::quit();
                    if (! Settings::NO_SCREENS_NO_VIDEO) {
                        Logger::saveVideo();
                        Logger::showLogs();
                    }
                    Logger::fileDump(true);
                break;
                case Qt::Key_F :
//                    Settings::FULLSCREEN ^= true;
//                    GUI::setFullScreen(Settings::FULLSCREEN);
                break;
                case Qt::Key_P :
                    Settings::PARALLEL_OMP ^= true;
                    GUI::updateLabels();
                break;
                case Qt::Key_R :
                    key[Qt::Key_R] = true;
                    if (Interaction::pause)
                    {
                        SimulationWindow::simWindow->gameStepNow();
                    }
                    key[Qt::Key_R] = false;
                break;
                case Qt::Key_G :
                    Settings::WORLD_ROTATION += 10.;
                    Settings::WORLD_ROTATION = fmod(Settings::WORLD_ROTATION, 360.);
                break;
                case Qt::Key_H :
                    GUI::showHideHelpLabel();
                break;
                case Qt::Key_I :
//                    switch (Settings::CONTROL_MODE) {
//                        case ONE_DRAG   : Settings::CONTROL_MODE = LOCAL_DRAG; break;
//                        case LOCAL_DRAG : Settings::CONTROL_MODE = FORCE_DRAG; break;
//                        case FORCE_DRAG : Settings::CONTROL_MODE = ONE_DRAG;   break;
//                    }
                break;
                case Qt::Key_N :
                    keybd_event(0x12, 0, 0, 0);
                    keybd_event(0x7B, 0, 0, 0);
                    keybd_event(0x12, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(0x7B, 0, KEYEVENTF_KEYUP, 0);
                break;
                case Qt::Key_T : Logger::enableOutput(Settings::NO_PRINTOUT); break;
                case Qt::Key_V : Matrices::resetCamera(); break;
                default: break;
            }
        }
    }
}

void Interaction::mouseMove(QMouseEvent *event)
{
    if (! State::USER_INSIDE_MENU)
    {
        mousePoint = event->pos();
        normalizedX = mousePoint.x()
            / static_cast<float>(SimulationWindow::simWindow->width()) * 2 - 1;
        normalizedY = - mousePoint.y()
            / static_cast<float>(SimulationWindow::simWindow->height()) * 2 + 1;
        dy = (SimulationWindow::simWindow->width()  / 2) - mousePoint.x();
        dx = (SimulationWindow::simWindow->height() / 2) - mousePoint.y();

        if (! State::CAMERA_ROTATION_LOCKED)
        {
            Matrices::camRX -= dx * mouseSpeed;
            Matrices::camRY -= dy * mouseSpeed;
            if (Matrices::camRX >  90) Matrices::camRX =  90;
            if (Matrices::camRX < -90) Matrices::camRX = -90;
            centerCursor();
        }
        else if (key[Qt::LeftButton])
        {
            handleTouchDrag(normalizedX, normalizedY);
        }
    }
}

void Interaction::mousePress(QMouseEvent *event)
{
    if (! State::USER_INSIDE_MENU)
    {
        mousePoint = event->pos();
        normalizedX = mousePoint.x()
            / static_cast<float>(SimulationWindow::simWindow->width()) * 2 - 1;
        normalizedY = - mousePoint.y()
            / static_cast<float>(SimulationWindow::simWindow->height()) * 2 - 1;

        if (State::CAMERA_ROTATION_LOCKED)
        {
            switch (event->button())
            {
                case Qt::LeftButton :
                    key[Qt::LeftButton] = true;
                    lastPressedMousePoint = mousePoint;
                    Interaction::handleTouchPress(normalizedX, normalizedY);
                break;
                case Qt::RightButton :
                    key[Qt::RightButton] = true;
                break;
                default : break;
            }
        }
    }
}
void Interaction::mouseRelease(QMouseEvent *event)
{
    if (! State::USER_INSIDE_MENU)
    {
        if (State::CAMERA_ROTATION_LOCKED)
        {
            switch (event->button())
            {
                case Qt::LeftButton :
                    key[Qt::LeftButton] = false;
                    Interaction::handleTouchDrop();
                break;
                case Qt::RightButton :
                    key[Qt::RightButton] = false;
                break;
                default : break;
            }
        }
    }
}

void Interaction::interact()
{
    if (! State::USER_INSIDE_MENU)
    {
        if (! State::CAMERA_POSITION_LOCKED)
        {
            if (key[Qt::Key_Up]    || key[Qt::Key_W]) moveUp();
            if (key[Qt::Key_Down]  || key[Qt::Key_S]) moveDown();
            if (key[Qt::Key_Left]  || key[Qt::Key_A]) moveLeft();
            if (key[Qt::Key_Right] || key[Qt::Key_D]) moveRight();
        }
        holdPressedParticle();
    }
}

void Interaction::moveUp()
{
    Matrices::camTX += moveSpeed * sinf(Matrices::camRY * degToRadF)
                                 * cosf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTY -= moveSpeed * sinf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTZ -= moveSpeed * cosf(Matrices::camRY * degToRadF)
                                 * cosf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
}
void Interaction::moveDown()
{
    Matrices::camTX -= moveSpeed * sinf(Matrices::camRY * degToRadF)
                                 * cosf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTY += moveSpeed * sinf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTZ += moveSpeed * cosf(Matrices::camRY * degToRadF)
                                 * cosf(Matrices::camRX * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
}
void Interaction::moveLeft()
{
    Matrices::camTX -= moveSpeed * cosf(Matrices::camRY * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTZ -= moveSpeed * sinf(Matrices::camRY * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
}
void Interaction::moveRight()
{
    Matrices::camTX += moveSpeed * cosf(Matrices::camRY * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
    Matrices::camTZ += moveSpeed * sinf(Matrices::camRY * degToRadF)
                                 * abs(SimulationWindow::latestFrameTime);
}

void Interaction::centerCursor()
{
    static QWidget *widget = SimulationWindow::simWidget;
    int x = widget->geometry().x() + widget->width()/2;
    int y = widget->geometry().y() + widget->height()/2;
    QCursor::setPos(x, y);
}

void Interaction::playBloop()
{
    #pragma omp critical
    {
        if (! blooper)
        {
            blooper = new QMediaPlayer;
            QUrl bloopURL = QUrl::fromLocalFile(QString("E:/Dropbox/well/WFAIS/prog/Qt/SPH/resources/sounds/bloop.mp3"));
            std::cout << bloopURL.toString().toStdString() << std::endl << std::endl;
            blooper->setMedia(bloopURL);
            blooper->setVolume(100);
        }
        blooper->play();
        //if (blooper->isAvailable())
    }
}

void Interaction::playMusic()
{
    static QMediaPlayer *musician = nullptr;
    if (! musician)
    {
        QUrl musicURL = QUrl::fromLocalFile(QString("E:/Dropbox/well/WFAIS/prog/Qt/SPH/resources/sounds/music.mp3"));
        musician->setMedia(musicURL);
        blooper->setVolume(20);
    }
    blooper->play();
}
